# Before Run this script you have to give permission
# chmod +x populate.sh

# Define nginx configuration file path
nginx_conf_folder="/etc/nginx/sites-enabled"

# Paths to nginx configuration files
frontend_nginx="./frontend.nginx"
backend_nginx="./backend.nginx"

# Check if nginx configuration folder exists
if [ ! -d $nginx_conf_folder ]; then
    echo "Nginx configuration folder '$nginx_conf_folder' not found. Exiting..."
    exit 1
fi

# Copy nginx configuration files
sudo cp $frontend_nginx $nginx_conf_folder
sudo cp $backend_nginx $nginx_conf_folder
echo "Copied the configuration files"

# Restart Nginx
echo "Restarting Nginx..."
sudo systemctl restart nginx

sudo ufw allow 8000

exit 0