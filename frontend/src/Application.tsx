import React, { useEffect } from "react";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import i18n from "./helper/i18n";
import { store } from "./data/redux/store";
import { Navigation } from "./Routes";
import { ConfirmBoxWidget } from "./widgets/confirm-box/ConfirmBoxWidget";
import { useColorMemo } from "./hook/useColorMemo";
import "./styles/bootstrap-theme.scss";
import "./index.scss";
import "./styles/components/bootstrap/ListGroup.scss";
import "./styles/themes.scss";

import "react-windows-ui/config/app-config.css";
import "react-windows-ui/dist/react-windows-ui.min.css";
import "react-windows-ui/icons/winui-icons.min.css";
import { electronStatic } from "./constant/configEndpoints";

export const App = () => {
  return (
    <Provider store={store}>
      <AppProvidedByRedux />
    </Provider>
  );
};

const AppProvidedByRedux = () => {
  const colorMode = useColorMemo();

  return (
    <I18nextProvider i18n={i18n}>
      <div className={`theme--${colorMode}`}>
       
        <ConfirmBoxWidget />
        <Navigation />
      </div>
    </I18nextProvider>
  );
};
