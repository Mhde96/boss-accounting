let domain = () => {
    console.log("NODE_ENV : ", process.env.NODE_ENV)

    if (process.env.NODE_ENV == "development") {
        return "http://localhost:5000"
    }

    return "https://api.boss-accounting.com/";
};
const api = "/";

export const baseURL = domain() + api;


export const electronStatic  = "static://"
////