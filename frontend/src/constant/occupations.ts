import { en } from "../helper/languages/en";

export const occupations = [
    {
        id: "5",
        name: en.teacher
    },
    {
        id: "1",
        name: en.student,
    },
    {
        id: "2",
        name: en.accountant,
    },
    {
        id: "3",
        name: en.developer
    }, {
        id: "4",
        name: en.other
    },
]