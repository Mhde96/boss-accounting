export const endpoints = {
  register: "register",
  login: "login",

  // accounts
  fetch_accounts: "fetch_accounts",
  add_account: "add_account",
  update_account: "update_account",
  delete_account: "delete_account",

  fetch_journals: "fetch_journals",
  add_journal: "add_journal",
  update_journal: "update_journal",
  delete_journal: "delete_journal",

  refreshToken: "refreshToken",
  forgot_password: "forgotPassword",
  backup_download: "backup_download",
  backup_upload: "backup_upload",
  getTimeFromServer: "getTimeFromServer",

  fetch_maps: "/fetch_maps",
  create_map: "/create_map",
  update_map: "/update_map",
  delete_map: "/delete_map",
  change_current_map: "/change_current_map",
  fetch_finincialCycles: "/fetch_financialcycles",
  add_finincialCycles: "/add_financialcycle",
  delete_finacialcycle: "/delete_finacialcycle",
  admin_users: "users",

  // admin
  get_admin_users: ({ serviceName,
    currentPage,
    pageSize,
    debounceSearchTerm,
    sortOrder,
    sortedColumn, }: {
      serviceName: string;

      currentPage: string | number;
      pageSize: string | number;
      debounceSearchTerm: string | number;
      sortOrder: string | number;
      sortedColumn: string | number | null;
    }) => `/admin/${serviceName}?page=${currentPage}&limit=${pageSize}&searchTerm=${debounceSearchTerm}&sortOrder=${sortOrder}&sortField=${sortedColumn}`
};
