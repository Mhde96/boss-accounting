import { getLanguage } from "../utils/language-utils";
import { AccountType } from "../types/accountType";
import { en } from "../helper/languages/en";
import i18n from '../helper/i18n'
const tradomgAccountName = i18n.t(en.trading)
const ProfitAndLossAccountName = i18n.t(en.profit_and_loss)

export const financialAccounts: AccountType[] = [
  {
    id: -2,
    name: tradomgAccountName,
    // @ts-ignore
    financial_statement: -1,
    map_id: undefined,
  },
  {
    id: -1,
    name: ProfitAndLossAccountName,
    // @ts-ignore
    financial_statement: 0,
    map_id: undefined,
  },
];
