import i18next from "i18next";
import { en } from "../helper/languages/en";

export const financial_statement_obg = {
  balance_sheet: {
    value: "0",
    label: i18next.t(en.balance_sheet),
  },
  profit_loss: {
    value: "-1",
    label: i18next.t(en.profit_and_loss),
    financial_statement: 0,
  },
  trading: {
    value: "-2",
    label: i18next.t(en.trading),
    financial_statement: -1,
  },
};

export const financial_statement_array = [
  {
    value: financial_statement_obg.balance_sheet.value,
    label: financial_statement_obg.balance_sheet.label,
  },
  {
    value: financial_statement_obg.profit_loss.value,
    label: financial_statement_obg.profit_loss.label,
  },
  {
    value: financial_statement_obg.trading.value,
    label: financial_statement_obg.trading.label,
  },
];
