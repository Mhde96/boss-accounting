export const countryCode = [
    {
        name: "Ascension Island",
        code: "AC",
        countryCode: "+247",
        emoji: "🇦🇨",
        unicode: "U+1F1E6 U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AC.svg"
    },
    {
        name: "Andorra",
        code: "AD",
        countryCode: "+376",
        emoji: "🇦🇩",
        unicode: "U+1F1E6 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AD.svg"
    },
    {
        name: "United Arab Emirates",
        code: "AE",
        countryCode: "+971",
        emoji: "🇦🇪",
        unicode: "U+1F1E6 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AE.svg"
    },
    {
        name: "Afghanistan",
        code: "AF",
        countryCode: "+93",
        emoji: "🇦🇫",
        unicode: "U+1F1E6 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AF.svg"
    },
    // {
    //     name: "Antigua & Barbuda",
    //     code: "AG",
    //     countryCode: "+1",
    //     emoji: "🇦🇬",
    //     unicode: "U+1F1E6 U+1F1EC",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AG.svg"
    // },
    // {
    //     name: "Anguilla",
    //     code: "AI",
    //     countryCode: "+1",
    //     emoji: "🇦🇮",
    //     unicode: "U+1F1E6 U+1F1EE",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AI.svg"
    // },
    {
        name: "Albania",
        code: "AL",
        countryCode: "+355",
        emoji: "🇦🇱",
        unicode: "U+1F1E6 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AL.svg"
    },
    {
        name: "Armenia",
        code: "AM",
        countryCode: "+374",
        emoji: "🇦🇲",
        unicode: "U+1F1E6 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AM.svg"
    },
    {
        name: "Angola",
        code: "AO",
        countryCode: "+244",
        emoji: "🇦🇴",
        unicode: "U+1F1E6 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AO.svg"
    },
    {
        name: "Antarctica",
        code: "AQ",
        countryCode: "+672", // Country code for Antarctica
        emoji: "🇦🇶",
        unicode: "U+1F1E6 U+1F1F6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AQ.svg"
    },
    {
        name: "Argentina",
        code: "AR",
        countryCode: "+54",
        emoji: "🇦🇷",
        unicode: "U+1F1E6 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AR.svg"
    },
    // {
    //     name: "American Samoa",
    //     code: "AS",
    //     countryCode: "+1",
    //     emoji: "🇦🇸",
    //     unicode: "U+1F1E6 U+1F1F8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AS.svg"
    // },
    {
        name: "Austria",
        code: "AT",
        countryCode: "+43",
        emoji: "🇦🇹",
        unicode: "U+1F1E6 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AT.svg"
    },
    {
        name: "Australia",
        code: "AU",
        countryCode: "+61",
        emoji: "🇦🇺",
        unicode: "U+1F1E6 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AU.svg"
    },
    {
        name: "Aruba",
        code: "AW",
        countryCode: "+297",
        emoji: "🇦🇼",
        unicode: "U+1F1E6 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AW.svg"
    },
    {
        name: "Åland Islands",
        code: "AX",
        countryCode: "+358",
        emoji: "🇦🇽",
        unicode: "U+1F1E6 U+1F1FD",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AX.svg"
    },
    {
        name: "Azerbaijan",
        code: "AZ",
        countryCode: "+994",
        emoji: "🇦🇿",
        unicode: "U+1F1E6 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AZ.svg"
    },
    {
        name: "Bosnia & Herzegovina",
        code: "BA",
        countryCode: "+387",
        emoji: "🇧🇦",
        unicode: "U+1F1E7 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BA.svg"
    },
    // {
    //     name: "Barbados",
    //     code: "BB",
    //     countryCode: "1-246",
    //     emoji: "🇧🇧",
    //     unicode: "U+1F1E7 U+1F1E7",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BB.svg"
    // },
    {
        name: "Bangladesh",
        code: "BD",
        countryCode: "+880",
        emoji: "🇧🇩",
        unicode: "U+1F1E7 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BD.svg"
    },
    {
        name: "Belgium",
        code: "BE",
        countryCode: "+32",
        emoji: "🇧🇪",
        unicode: "U+1F1E7 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BE.svg"
    },
    {
        name: "Burkina Faso",
        code: "BF",
        countryCode: "+226",
        emoji: "🇧🇫",
        unicode: "U+1F1E7 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BF.svg"
    },
    {
        name: "Bulgaria",
        code: "BG",
        countryCode: "+359",
        emoji: "🇧🇬",
        unicode: "U+1F1E7 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BG.svg"
    },
    {
        name: "Bahrain",
        code: "BH",
        countryCode: "+973",
        emoji: "🇧🇭",
        unicode: "U+1F1E7 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BH.svg"
    },
    {
        name: "Burundi",
        code: "BI",
        countryCode: "+257",
        emoji: "🇧🇮",
        unicode: "U+1F1E7 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BI.svg"
    },
    {
        name: "Benin",
        code: "BJ",
        countryCode: "+229",
        emoji: "🇧🇯",
        unicode: "U+1F1E7 U+1F1EF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BJ.svg"
    },
    {
        name: "St. Barthélemy",
        code: "BL",
        countryCode: "+590",
        emoji: "🇧🇱",
        unicode: "U+1F1E7 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BL.svg"
    },
    // {
    //     name: "Bermuda",
    //     code: "BM",
    //     countryCode: "1-441",
    //     emoji: "🇧🇲",
    //     unicode: "U+1F1E7 U+1F1F2",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BM.svg"
    // },
    {
        name: "Brunei",
        code: "BN",
        countryCode: "+673",
        emoji: "🇧🇳",
        unicode: "U+1F1E7 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BN.svg"
    },
    {
        name: "Bolivia",
        code: "BO",
        countryCode: "+591",
        emoji: "🇧🇴",
        unicode: "U+1F1E7 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BO.svg"
    },
    {
        name: "Caribbean Netherlands",
        code: "BQ",
        countryCode: "+599",
        emoji: "🇧🇶",
        unicode: "U+1F1E7 U+1F1F6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BQ.svg"
    },
    {
        name: "Brazil",
        code: "BR",
        countryCode: "+55",
        emoji: "🇧🇷",
        unicode: "U+1F1E7 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BR.svg"
    },
    // {
    //     name: "Bahamas",
    //     code: "BS",
    //     countryCode: "1-242",
    //     emoji: "🇧🇸",
    //     unicode: "U+1F1E7 U+1F1F8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BS.svg"
    // },
    {
        name: "Bhutan",
        code: "BT",
        countryCode: "+975",
        emoji: "🇧🇹",
        unicode: "U+1F1E7 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BT.svg"
    },
    // {
    //     name: "Bouvet Island",
    //     code: "BV",
    //     countryCode: null,
    //     emoji: "🇧🇻",
    //     unicode: "U+1F1E7 U+1F1FB",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BV.svg"
    // },
    {
        name: "Botswana",
        code: "BW",
        countryCode: "+267",
        emoji: "🇧🇼",
        unicode: "U+1F1E7 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BW.svg"
    },
    {
        name: "Belarus",
        code: "BY",
        countryCode: "+375",
        emoji: "🇧🇾",
        unicode: "U+1F1E7 U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BY.svg"
    },
    {
        name: "Belize",
        code: "BZ",
        countryCode: "+501",
        emoji: "🇧🇿",
        unicode: "U+1F1E7 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BZ.svg"
    },
    {
        name: "Canada",
        code: "CA",
        countryCode: "+1",
        emoji: "🇨🇦",
        unicode: "U+1F1E8 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CA.svg"
    },
    {
        name: "Cocos (Keeling) Islands",
        code: "CC",
        countryCode: "+61",
        emoji: "🇨🇨",
        unicode: "U+1F1E8 U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CC.svg"
    },
    {
        name: "Congo - Kinshasa",
        code: "CD",
        countryCode: "+243",
        emoji: "🇨🇩",
        unicode: "U+1F1E8 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CD.svg"
    },
    {
        name: "Central African Republic",
        code: "CF",
        countryCode: "+236",
        emoji: "🇨🇫",
        unicode: "U+1F1E8 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CF.svg"
    },
    {
        name: "Congo - Brazzaville",
        code: "CG",
        countryCode: "+242",
        emoji: "🇨🇬",
        unicode: "U+1F1E8 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CG.svg"
    },
    {
        name: "Switzerland",
        code: "CH",
        countryCode: "+41",
        emoji: "🇨🇭",
        unicode: "U+1F1E8 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CH.svg"
    },
    {
        name: "Côte d’Ivoire",
        code: "CI",
        countryCode: "+225",
        emoji: "🇨🇮",
        unicode: "U+1F1E8 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CI.svg"
    },
    {
        name: "Cook Islands",
        code: "CK",
        countryCode: "+682",
        emoji: "🇨🇰",
        unicode: "U+1F1E8 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CK.svg"
    },
    {
        name: "Chile",
        code: "CL",
        countryCode: "+56",
        emoji: "🇨🇱",
        unicode: "U+1F1E8 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CL.svg"
    },
    {
        name: "Cameroon",
        code: "CM",
        countryCode: "+237",
        emoji: "🇨🇲",
        unicode: "U+1F1E8 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CM.svg"
    },
    {
        name: "China",
        code: "CN",
        countryCode: "+86",
        emoji: "🇨🇳",
        unicode: "U+1F1E8 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CN.svg"
    },
    {
        name: "Colombia",
        code: "CO",
        countryCode: "+57",
        emoji: "🇨🇴",
        unicode: "U+1F1E8 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CO.svg"
    },
    {
        name: "Clipperton Island",
        code: "CP",
        countryCode: "+262",
        emoji: "🇨🇵",
        unicode: "U+1F1E8 U+1F1F5",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CP.svg"
    },
    {
        name: "Costa Rica",
        code: "CR",
        countryCode: "+506",
        emoji: "🇨🇷",
        unicode: "U+1F1E8 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CR.svg"
    },
    {
        name: "Cuba",
        code: "CU",
        countryCode: "+53",
        emoji: "🇨🇺",
        unicode: "U+1F1E8 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CU.svg"
    },
    {
        name: "Cape Verde",
        code: "CV",
        countryCode: "+238",
        emoji: "🇨🇻",
        unicode: "U+1F1E8 U+1F1FB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CV.svg"
    },
    {
        name: "Curaçao",
        code: "CW",
        countryCode: "+599",
        emoji: "🇨🇼",
        unicode: "U+1F1E8 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CW.svg"
    },
    {
        name: "Christmas Island",
        code: "CX",
        countryCode: "+61",
        emoji: "🇨🇽",
        unicode: "U+1F1E8 U+1F1FD",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CX.svg"
    },
    {
        name: "Cyprus",
        code: "CY",
        countryCode: "+357",
        emoji: "🇨🇾",
        unicode: "U+1F1E8 U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CY.svg"
    },
    {
        name: "Czechia",
        code: "CZ",
        countryCode: "+420",
        emoji: "🇨🇿",
        unicode: "U+1F1E8 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CZ.svg"
    },
    {
        name: "Germany",
        code: "DE",
        countryCode: "+49",
        emoji: "🇩🇪",
        unicode: "U+1F1E9 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DE.svg"
    },
    {
        name: "Diego Garcia",
        code: "DG",
        countryCode: "+246",
        emoji: "🇩🇬",
        unicode: "U+1F1E9 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DG.svg"
    },
    {
        name: "Djibouti",
        code: "DJ",
        countryCode: "+253",
        emoji: "🇩🇯",
        unicode: "U+1F1E9 U+1F1EF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DJ.svg"
    },
    {
        name: "Denmark",
        code: "DK",
        countryCode: "+45",
        emoji: "🇩🇰",
        unicode: "U+1F1E9 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DK.svg"
    },
    // {
    //     name: "Dominica",
    //     code: "DM",
    //     countryCode: "+1-767",
    //     emoji: "🇩🇲",
    //     unicode: "U+1F1E9 U+1F1F2",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DM.svg"
    // },
    // {
    //     name: "Dominican Republic",
    //     code: "DO",
    //     countryCode: "+1-809, +1-829, +1-849",
    //     emoji: "🇩🇴",
    //     unicode: "U+1F1E9 U+1F1F4",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DO.svg"
    // },
    {
        name: "Algeria",
        code: "DZ",
        countryCode: "+213",
        emoji: "🇩🇿",
        unicode: "U+1F1E9 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DZ.svg"
    },
    // {
    //     name: "Ceuta & Melilla",
    //     code: "EA",
    //     countryCode: "",
    //     emoji: "🇪🇦",
    //     unicode: "U+1F1EA U+1F1E6",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EA.svg"
    // },
    {
        name: "Ecuador",
        code: "EC",
        countryCode: "+593",
        emoji: "🇪🇨",
        unicode: "U+1F1EA U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EC.svg"
    },
    {
        name: "Estonia",
        code: "EE",
        countryCode: "+372",
        emoji: "🇪🇪",
        unicode: "U+1F1EA U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EE.svg"
    },
    {
        name: "Egypt",
        code: "EG",
        countryCode: "+20",
        emoji: "🇪🇬",
        unicode: "U+1F1EA U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EG.svg"
    },
    {
        name: "Western Sahara",
        code: "EH",
        countryCode: "+212",
        emoji: "🇪🇭",
        unicode: "U+1F1EA U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EH.svg"
    },
    {
        name: "Eritrea",
        code: "ER",
        countryCode: "+291",
        emoji: "🇪🇷",
        unicode: "U+1F1EA U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ER.svg"
    },
    {
        name: "Spain",
        code: "ES",
        countryCode: "+34",
        emoji: "🇪🇸",
        unicode: "U+1F1EA U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ES.svg"
    },
    {
        name: "Ethiopia",
        code: "ET",
        countryCode: "+251",
        emoji: "🇪🇹",
        unicode: "U+1F1EA U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ET.svg"
    },
    // {
    //     name: "European Union",
    //     code: "EU",
    //     countryCode: "",
    //     emoji: "🇪🇺",
    //     unicode: "U+1F1EA U+1F1FA",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EU.svg"
    // },
    {
        name: "Finland",
        code: "FI",
        countryCode: "+358",
        emoji: "🇫🇮",
        unicode: "U+1F1EB U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FI.svg"
    },
    {
        name: "Fiji",
        code: "FJ",
        countryCode: "+679",
        emoji: "🇫🇯",
        unicode: "U+1F1EB U+1F1EF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FJ.svg"
    },
    {
        name: "Falkland Islands",
        code: "FK",
        countryCode: "+500",
        emoji: "🇫🇰",
        unicode: "U+1F1EB U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FK.svg"
    },
    {
        name: "Micronesia",
        code: "FM",
        countryCode: "+691",
        emoji: "🇫🇲",
        unicode: "U+1F1EB U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FM.svg"
    },
    {
        name: "Faroe Islands",
        code: "FO",
        countryCode: "+298",
        emoji: "🇫🇴",
        unicode: "U+1F1EB U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FO.svg"
    },
    {
        name: "France",
        code: "FR",
        countryCode: "+33",
        emoji: "🇫🇷",
        unicode: "U+1F1EB U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FR.svg"
    },
    {
        name: "Gabon",
        code: "GA",
        countryCode: "+241",
        emoji: "🇬🇦",
        unicode: "U+1F1EC U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GA.svg"
    },
    {
        name: "United Kingdom",
        code: "GB",
        countryCode: "+44",
        emoji: "🇬🇧",
        unicode: "U+1F1EC U+1F1E7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GB.svg"
    },
    // {
    //     name: "Grenada",
    //     code: "GD",
    //     countryCode: "+1 473",
    //     emoji: "🇬🇩",
    //     unicode: "U+1F1EC U+1F1E9",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GD.svg"
    // },
    {
        name: "Georgia",
        code: "GE",
        countryCode: "+995",
        emoji: "🇬🇪",
        unicode: "U+1F1EC U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GE.svg"
    },
    {
        name: "French Guiana",
        code: "GF",
        countryCode: "+594",
        emoji: "🇬🇫",
        unicode: "U+1F1EC U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GF.svg"
    },
    {
        name: "Guernsey",
        code: "GG",
        countryCode: "+44",
        emoji: "🇬🇬",
        unicode: "U+1F1EC U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GG.svg"
    },
    {
        name: "Ghana",
        code: "GH",
        countryCode: "+233",
        emoji: "🇬🇭",
        unicode: "U+1F1EC U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GH.svg"
    },
    {
        name: "Gibraltar",
        code: "GI",
        countryCode: "+350",
        emoji: "🇬🇮",
        unicode: "U+1F1EC U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GI.svg"
    },
    {
        name: "Greenland",
        code: "GL",
        countryCode: "+299",
        emoji: "🇬🇱",
        unicode: "U+1F1EC U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GL.svg"
    },
    {
        name: "Gambia",
        code: "GM",
        countryCode: "+220",
        emoji: "🇬🇲",
        unicode: "U+1F1EC U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GM.svg"
    },
    {
        name: "Guinea",
        code: "GN",
        countryCode: "+224",
        emoji: "🇬🇳",
        unicode: "U+1F1EC U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GN.svg"
    },
    {
        name: "Guadeloupe",
        code: "GP",
        countryCode: "+590",
        emoji: "🇬🇵",
        unicode: "U+1F1EC U+1F1F5",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GP.svg"
    },
    {
        name: "Equatorial Guinea",
        code: "GQ",
        countryCode: "+240",
        emoji: "🇬🇶",
        unicode: "U+1F1EC U+1F1F6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GQ.svg"
    },
    {
        name: "Greece",
        code: "GR",
        countryCode: "+30",
        emoji: "🇬🇷",
        unicode: "U+1F1EC U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GR.svg"
    },
    {
        name: "South Georgia & South Sandwich Islands",
        code: "GS",
        countryCode: "+500",
        emoji: "🇬🇸",
        unicode: "U+1F1EC U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GS.svg"
    },
    {
        name: "Guatemala",
        code: "GT",
        countryCode: "+502",
        emoji: "🇬🇹",
        unicode: "U+1F1EC U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GT.svg"
    },
    // {
    //     name: "Guam",
    //     code: "GU",
    //     countryCode: "+1 671",
    //     emoji: "🇬🇺",
    //     unicode: "U+1F1EC U+1F1FA",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GU.svg"
    // },
    {
        name: "Guinea-Bissau",
        code: "GW",
        countryCode: "+245",
        emoji: "🇬🇼",
        unicode: "U+1F1EC U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GW.svg"
    },
    {
        name: "Guyana",
        code: "GY",
        countryCode: "+592",
        emoji: "🇬🇾",
        unicode: "U+1F1EC U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GY.svg"
    },
    {
        name: "Hong Kong SAR China",
        code: "HK",
        countryCode: "+852",
        emoji: "🇭🇰",
        unicode: "U+1F1ED U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HK.svg"
    },
    // {
    //     name: "Heard & McDonald Islands",
    //     code: "HM",
    //     countryCode: null,
    //     emoji: "🇭🇲",
    //     unicode: "U+1F1ED U+1F1F2",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HM.svg"
    // },
    {
        name: "Honduras",
        code: "HN",
        countryCode: "+504",
        emoji: "🇭🇳",
        unicode: "U+1F1ED U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HN.svg"
    },
    {
        name: "Croatia",
        code: "HR",
        countryCode: "+385",
        emoji: "🇭🇷",
        unicode: "U+1F1ED U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HR.svg"
    },
    {
        name: "Haiti",
        code: "HT",
        countryCode: "+509",
        emoji: "🇭🇹",
        unicode: "U+1F1ED U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HT.svg"
    },
    {
        name: "Hungary",
        code: "HU",
        countryCode: "+36",
        emoji: "🇭🇺",
        unicode: "U+1F1ED U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HU.svg"
    },
    // {
    //     name: "Canary Islands",
    //     code: "IC",
    //     countryCode: null,
    //     emoji: "🇮🇨",
    //     unicode: "U+1F1EE U+1F1E8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IC.svg"
    // },
    {
        name: "Indonesia",
        code: "ID",
        countryCode: "+62",
        emoji: "🇮🇩",
        unicode: "U+1F1EE U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg"
    },
    {
        name: "Ireland",
        code: "IE",
        countryCode: "+353",
        emoji: "🇮🇪",
        unicode: "U+1F1EE U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IE.svg"
    },
    {
        name: "Israel",
        code: "IL",
        countryCode: "+972",
        emoji: "🇮🇱",
        unicode: "U+1F1EE U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IL.svg"
    },
    {
        name: "Isle of Man",
        code: "IM",
        countryCode: "+44",
        emoji: "🇮🇲",
        unicode: "U+1F1EE U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IM.svg"
    },
    {
        name: "India",
        code: "IN",
        countryCode: "+91",
        emoji: "🇮🇳",
        unicode: "U+1F1EE U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IN.svg"
    },
    {
        name: "British Indian Ocean Territory",
        code: "IO",
        countryCode: "+246",
        emoji: "🇮🇴",
        unicode: "U+1F1EE U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IO.svg"
    },
    {
        name: "Iraq",
        code: "IQ",
        countryCode: "+964",
        emoji: "🇮🇶",
        unicode: "U+1F1EE U+1F1F6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IQ.svg"
    },
    {
        name: "Iran",
        code: "IR",
        countryCode: "+98",
        emoji: "🇮🇷",
        unicode: "U+1F1EE U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IR.svg"
    },
    {
        name: "Iceland",
        code: "IS",
        countryCode: "+354",
        emoji: "🇮🇸",
        unicode: "U+1F1EE U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IS.svg"
    },
    {
        name: "Italy",
        code: "IT",
        countryCode: "+39",
        emoji: "🇮🇹",
        unicode: "U+1F1EE U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IT.svg"
    },
    {
        name: "Jersey",
        code: "JE",
        countryCode: "+44",
        emoji: "🇯🇪",
        unicode: "U+1F1EF U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JE.svg"
    },
    // {
    //     name: "Jamaica",
    //     code: "JM",
    //     countryCode: "+1",
    //     emoji: "🇯🇲",
    //     unicode: "U+1F1EF U+1F1F2",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JM.svg"
    // },
    {
        name: "Jordan",
        code: "JO",
        countryCode: "+962",
        emoji: "🇯🇴",
        unicode: "U+1F1EF U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JO.svg"
    },
    {
        name: "Japan",
        code: "JP",
        countryCode: "+81",
        emoji: "🇯🇵",
        unicode: "U+1F1EF U+1F1F5",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JP.svg"
    },
    {
        name: "Kenya",
        code: "KE",
        countryCode: "+254",
        emoji: "🇰🇪",
        unicode: "U+1F1F0 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KE.svg"
    },
    {
        name: "Kyrgyzstan",
        code: "KG",
        countryCode: "+996",
        emoji: "🇰🇬",
        unicode: "U+1F1F0 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KG.svg"
    },
    {
        name: "Cambodia",
        code: "KH",
        countryCode: "+855",
        emoji: "🇰🇭",
        unicode: "U+1F1F0 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KH.svg"
    },
    {
        name: "Kiribati",
        code: "KI",
        countryCode: "+686",
        emoji: "🇰🇮",
        unicode: "U+1F1F0 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KI.svg"
    },
    {
        name: "Comoros",
        code: "KM",
        countryCode: "+269",
        emoji: "🇰🇲",
        unicode: "U+1F1F0 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KM.svg"
    },
    // {
    //     name: "St. Kitts & Nevis",
    //     code: "KN",
    //     countryCode: "+1",
    //     emoji: "🇰🇳",
    //     unicode: "U+1F1F0 U+1F1F3",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KN.svg"
    // },
    {
        name: "North Korea",
        code: "KP",
        countryCode: "+850",
        emoji: "🇰🇵",
        unicode: "U+1F1F0 U+1F1F5",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KP.svg"
    },
    {
        name: "South Korea",
        code: "KR",
        countryCode: "+82",
        emoji: "🇰🇷",
        unicode: "U+1F1F0 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KR.svg"
    },
    {
        name: "Kuwait",
        code: "KW",
        countryCode: "+965",
        emoji: "🇰🇼",
        unicode: "U+1F1F0 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KW.svg"
    },
    // {
    //     name: "Cayman Islands",
    //     code: "KY",
    //     countryCode: "+1",
    //     emoji: "🇰🇾",
    //     unicode: "U+1F1F0 U+1F1FE",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KY.svg"
    // },
    {
        name: "Kazakhstan",
        code: "KZ",
        countryCode: "+7",
        emoji: "🇰🇿",
        unicode: "U+1F1F0 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KZ.svg"
    },
    {
        name: "Laos",
        code: "LA",
        countryCode: "+856",
        emoji: "🇱🇦",
        unicode: "U+1F1F1 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LA.svg"
    },
    {
        name: "Lebanon",
        code: "LB",
        countryCode: "+961",
        emoji: "🇱🇧",
        unicode: "U+1F1F1 U+1F1E7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LB.svg"
    },
    // {
    //     name: "St. Lucia",
    //     code: "LC",
    //     countryCode: "+1",
    //     emoji: "🇱🇨",
    //     unicode: "U+1F1F1 U+1F1E8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LC.svg"
    // },
    {
        name: "Liechtenstein",
        code: "LI",
        countryCode: "+423",
        emoji: "🇱🇮",
        unicode: "U+1F1F1 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LI.svg"
    },
    {
        name: "Sri Lanka",
        code: "LK",
        countryCode: "+94",
        emoji: "🇱🇰",
        unicode: "U+1F1F1 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LK.svg"
    },
    {
        name: "Liberia",
        code: "LR",
        countryCode: "+231",
        emoji: "🇱🇷",
        unicode: "U+1F1F1 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LR.svg"
    },
    {
        name: "Lesotho",
        code: "LS",
        countryCode: "+266",
        emoji: "🇱🇸",
        unicode: "U+1F1F1 U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LS.svg"
    },
    {
        name: "Lithuania",
        code: "LT",
        countryCode: "+370",
        emoji: "🇱🇹",
        unicode: "U+1F1F1 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LT.svg"
    },
    {
        name: "Luxembourg",
        code: "LU",
        countryCode: "+352",
        emoji: "🇱🇺",
        unicode: "U+1F1F1 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LU.svg"
    },
    {
        name: "Latvia",
        code: "LV",
        countryCode: "+371",
        emoji: "🇱🇻",
        unicode: "U+1F1F1 U+1F1FB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LV.svg"
    },
    {
        name: "Libya",
        code: "LY",
        emoji: "🇱🇾",
        unicode: "U+1F1F1 U+1F1FE",
        countryCode: "+218",

        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LY.svg"
    },
    {
        name: "Morocco",
        code: "MA",
        countryCode: "+212",
        emoji: "🇲🇦",
        unicode: "U+1F1F2 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MA.svg"
    },
    {
        name: "Monaco",
        code: "MC",
        countryCode: "+377",
        emoji: "🇲🇨",
        unicode: "U+1F1F2 U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MC.svg"
    },
    {
        name: "Moldova",
        code: "MD",
        countryCode: "+373",
        emoji: "🇲🇩",
        unicode: "U+1F1F2 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MD.svg"
    },
    {
        name: "Montenegro",
        code: "ME",
        countryCode: "+382",
        emoji: "🇲🇪",
        unicode: "U+1F1F2 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ME.svg"
    },
    {
        name: "St. Martin",
        code: "MF",
        countryCode: "+590",
        emoji: "🇲🇫",
        unicode: "U+1F1F2 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MF.svg"
    },
    {
        name: "Madagascar",
        code: "MG",
        countryCode: "+261",
        emoji: "🇲🇬",
        unicode: "U+1F1F2 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MG.svg"
    },
    {
        name: "Marshall Islands",
        code: "MH",
        countryCode: "+692",
        emoji: "🇲🇭",
        unicode: "U+1F1F2 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MH.svg"
    },
    {
        name: "North Macedonia",
        code: "MK",
        countryCode: "+389",
        emoji: "🇲🇰",
        unicode: "U+1F1F2 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MK.svg"
    },
    // {
    //     name: "Mali",
    //     code: "ML",
    //     emoji: "🇲🇱",
    //     unicode: "U+1F1F2 U+1F1F1",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ML.svg"
    // },
    {
        name: "Myanmar (Burma)",
        code: "MM",
        countryCode: "+95",
        emoji: "🇲🇲",
        unicode: "U+1F1F2 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MM.svg"
    },
    {
        name: "Mongolia",
        code: "MN",
        countryCode: "+976",
        emoji: "🇲🇳",
        unicode: "U+1F1F2 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MN.svg"
    },
    {
        name: "Macao SAR China",
        code: "MO",
        countryCode: "+853",
        emoji: "🇲🇴",
        unicode: "U+1F1F2 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MO.svg"
    },
    // {
    //     name: "Northern Mariana Islands",
    //     code: "MP",
    //     countryCode: "+1",
    //     emoji: "🇲🇵",
    //     unicode: "U+1F1F2 U+1F1F5",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MP.svg"
    // },
    {
        name: "Martinique",
        code: "MQ",
        emoji: "🇲🇶",
        unicode: "U+1F1F2 U+1F1F6",
        countryCode: "+596",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MQ.svg"
    },
    {
        name: "Mauritania",
        code: "MR",
        countryCode: "+222",
        emoji: "🇲🇷",
        unicode: "U+1F1F2 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MR.svg"
    },
    // {
    //     name: "Montserrat",
    //     code: "MS",
    //     countryCode: "+1",
    //     emoji: "🇲🇸",
    //     unicode: "U+1F1F2 U+1F1F8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MS.svg"
    // },
    {
        name: "Malta",
        code: "MT",
        countryCode: "+356",
        emoji: "🇲🇹",
        unicode: "U+1F1F2 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MT.svg"
    },
    {
        name: "Mauritius",
        code: "MU",
        emoji: "🇲🇺",
        unicode: "U+1F1F2 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MU.svg",
        countryCode: "+230"
    },
    {
        name: "Maldives",
        code: "MV",
        emoji: "🇲🇻",
        unicode: "U+1F1F2 U+1F1FB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MV.svg",
        countryCode: "+960"
    },
    {
        name: "Malawi",
        code: "MW",
        emoji: "🇲🇼",
        unicode: "U+1F1F2 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MW.svg",
        countryCode: "+265"
    },
    {
        name: "Mexico",
        code: "MX",
        emoji: "🇲🇽",
        unicode: "U+1F1F2 U+1F1FD",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MX.svg",
        countryCode: "+52"
    },
    {
        name: "Malaysia",
        code: "MY",
        emoji: "🇲🇾",
        unicode: "U+1F1F2 U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MY.svg",
        countryCode: "+60"
    },
    {
        name: "Mozambique",
        code: "MZ",
        emoji: "🇲🇿",
        unicode: "U+1F1F2 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MZ.svg",
        countryCode: "+258"
    },
    {
        name: "Namibia",
        code: "NA",
        emoji: "🇳🇦",
        unicode: "U+1F1F3 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NA.svg",
        countryCode: "+264"
    },
    {
        name: "New Caledonia",
        code: "NC",
        emoji: "🇳🇨",
        unicode: "U+1F1F3 U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NC.svg",
        countryCode: "+687"
    },
    {
        name: "Niger",
        code: "NE",
        emoji: "🇳🇪",
        unicode: "U+1F1F3 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NE.svg",
        countryCode: "+227"
    },
    {
        name: "Norfolk Island",
        code: "NF",
        emoji: "🇳🇫",
        unicode: "U+1F1F3 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NF.svg",
        countryCode: "+672"
    },
    {
        name: "Nigeria",
        code: "NG",
        emoji: "🇳🇬",
        unicode: "U+1F1F3 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NG.svg",
        countryCode: "+234"
    },
    {
        name: "Nicaragua",
        code: "NI",
        emoji: "🇳🇮",
        unicode: "U+1F1F3 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NI.svg",
        countryCode: "+505"
    },
    {
        name: "Netherlands",
        code: "NL",
        emoji: "🇳🇱",
        unicode: "U+1F1F3 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NL.svg",
        countryCode: "+31"
    },
    {
        name: "Norway",
        code: "NO",
        emoji: "🇳🇴",
        unicode: "U+1F1F3 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NO.svg",
        countryCode: "+47"
    },
    {
        name: "Nepal",
        code: "NP",
        emoji: "🇳🇵",
        unicode: "U+1F1F3 U+1F1F5",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NP.svg",
        countryCode: "+977"
    },
    {
        name: "Nauru",
        code: "NR",
        emoji: "🇳🇷",
        unicode: "U+1F1F3 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NR.svg",
        countryCode: "+674"
    },
    {
        name: "Niue",
        code: "NU",
        emoji: "🇳🇺",
        unicode: "U+1F1F3 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NU.svg",
        countryCode: "+683"
    },
    {
        name: "New Zealand",
        code: "NZ",
        emoji: "🇳🇿",
        unicode: "U+1F1F3 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NZ.svg",
        countryCode: "+64"
    },
    {
        name: "Oman",
        code: "OM",
        emoji: "🇴🇲",
        unicode: "U+1F1F4 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/OM.svg",
        countryCode: "+968"
    },
    {
        name: "Panama",
        code: "PA",
        emoji: "🇵🇦",
        unicode: "U+1F1F5 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PA.svg",
        countryCode: "+507"
    },
    {
        name: "Peru",
        code: "PE",
        emoji: "🇵🇪",
        unicode: "U+1F1F5 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PE.svg",
        countryCode: "+51"
    },
    {
        name: "French Polynesia",
        code: "PF",
        emoji: "🇵🇫",
        unicode: "U+1F1F5 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PF.svg",
        countryCode: "+689"
    },
    {
        name: "Papua New Guinea",
        code: "PG",
        emoji: "🇵🇬",
        unicode: "U+1F1F5 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PG.svg",
        countryCode: "+675"
    },
    {
        name: "Philippines",
        code: "PH",
        emoji: "🇵🇭",
        unicode: "U+1F1F5 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PH.svg",
        countryCode: "+63"
    },
    {
        name: "Pakistan",
        code: "PK",
        emoji: "🇵🇰",
        unicode: "U+1F1F5 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PK.svg",
        countryCode: "+92"
    },
    {
        name: "Poland",
        code: "PL",
        emoji: "🇵🇱",
        unicode: "U+1F1F5 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PL.svg",
        countryCode: "+48"
    },
    {
        name: "St. Pierre & Miquelon",
        code: "PM",
        emoji: "🇵🇲",
        unicode: "U+1F1F5 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PM.svg",
        countryCode: "+508"
    },
    {
        name: "Pitcairn Islands",
        code: "PN",
        emoji: "🇵🇳",
        unicode: "U+1F1F5 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PN.svg",
        countryCode: "+870"
    },
    // {
    //     name: "Puerto Rico",
    //     code: "PR",
    //     emoji: "🇵🇷",
    //     unicode: "U+1F1F5 U+1F1F7",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PR.svg",
    //     countryCode: "+1"
    // },
    {
        name: "Palestinian Territories",
        code: "PS",
        emoji: "🇵🇸",
        unicode: "U+1F1F5 U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PS.svg",
        countryCode: "+970"
    },
    {
        name: "Portugal",
        code: "PT",
        emoji: "🇵🇹",
        unicode: "U+1F1F5 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PT.svg",
        countryCode: "+351"
    },
    {
        name: "Palau",
        code: "PW",
        emoji: "🇵🇼",
        unicode: "U+1F1F5 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PW.svg",
        countryCode: "+680"
    },
    {
        name: "Paraguay",
        code: "PY",
        emoji: "🇵🇾",
        unicode: "U+1F1F5 U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PY.svg",
        countryCode: "+595"
    },
    {
        name: "Qatar",
        code: "QA",
        emoji: "🇶🇦",
        unicode: "U+1F1F6 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/QA.svg",
        countryCode: "+974"
    },
    {
        name: "Réunion",
        code: "RE",
        emoji: "🇷🇪",
        unicode: "U+1F1F7 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RE.svg",
        countryCode: "+262"
    },
    {
        name: "Romania",
        code: "RO",
        emoji: "🇷🇴",
        unicode: "U+1F1F7 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RO.svg",
        countryCode: "+40"
    },
    {
        name: "Serbia",
        code: "RS",
        emoji: "🇷🇸",
        unicode: "U+1F1F7 U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RS.svg",
        countryCode: "+381"
    },
    {
        name: "Russia",
        code: "RU",
        emoji: "🇷🇺",
        unicode: "U+1F1F7 U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RU.svg",
        countryCode: "+7"
    },
    {
        name: "Rwanda",
        code: "RW",
        emoji: "🇷🇼",
        unicode: "U+1F1F7 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RW.svg",
        countryCode: "+250"
    },
    {
        name: "Saudi Arabia",
        code: "SA",
        emoji: "🇸🇦",
        unicode: "U+1F1F8 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SA.svg",
        countryCode: "+966"
    },
    {
        name: "Solomon Islands",
        code: "SB",
        emoji: "🇸🇧",
        unicode: "U+1F1F8 U+1F1E7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SB.svg",
        countryCode: "+677"
    },
    {
        name: "Seychelles",
        code: "SC",
        emoji: "🇸🇨",
        unicode: "U+1F1F8 U+1F1E8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SC.svg",
        countryCode: "+248"
    },
    {
        name: "Sudan",
        code: "SD",
        emoji: "🇸🇩",
        unicode: "U+1F1F8 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SD.svg",
        countryCode: "+249"
    },
    {
        name: "Sweden",
        code: "SE",
        emoji: "🇸🇪",
        unicode: "U+1F1F8 U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SE.svg",
        countryCode: "+46"
    },
    {
        name: "Singapore",
        code: "SG",
        emoji: "🇸🇬",
        unicode: "U+1F1F8 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SG.svg",
        countryCode: "+65"
    },
    {
        name: "St. Helena",
        code: "SH",
        emoji: "🇸🇭",
        unicode: "U+1F1F8 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SH.svg",
        countryCode: "+290"
    },
    {
        name: "Slovenia",
        code: "SI",
        emoji: "🇸🇮",
        unicode: "U+1F1F8 U+1F1EE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SI.svg",
        countryCode: "+386"
    },
    {
        name: "Svalbard & Jan Mayen",
        code: "SJ",
        emoji: "🇸🇯",
        unicode: "U+1F1F8 U+1F1EF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SJ.svg",
        countryCode: "+47"
    },
    {
        name: "Slovakia",
        code: "SK",
        emoji: "🇸🇰",
        unicode: "U+1F1F8 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SK.svg",
        countryCode: "+421"
    },
    {
        name: "Sierra Leone",
        code: "SL",
        emoji: "🇸🇱",
        unicode: "U+1F1F8 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SL.svg",
        countryCode: '+232'
    },
    {
        name: "San Marino",
        code: "SM",
        emoji: "🇸🇲",
        unicode: "U+1F1F8 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SM.svg",
        countryCode: '+378'
    },
    {
        name: "Senegal",
        code: "SN",
        emoji: "🇸🇳",
        unicode: "U+1F1F8 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SN.svg",
        countryCode: '+221'
    },
    {
        name: "Somalia",
        code: "SO",
        emoji: "🇸🇴",
        unicode: "U+1F1F8 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SO.svg",
        countryCode: '+252'
    },
    {
        name: "Suriname",
        code: "SR",
        emoji: "🇸🇷",
        unicode: "U+1F1F8 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SR.svg",
        countryCode: '+597'
    },
    {
        name: "South Sudan",
        code: "SS",
        emoji: "🇸🇸",
        unicode: "U+1F1F8 U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SS.svg",
        countryCode: '+211'
    },
    {
        name: "São Tomé & Príncipe",
        code: "ST",
        emoji: "🇸🇹",
        unicode: "U+1F1F8 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ST.svg",
        countryCode: '+239'
    },
    {
        name: "El Salvador",
        code: "SV",
        emoji: "🇸🇻",
        unicode: "U+1F1F8 U+1F1FB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SV.svg",
        countryCode: '+503'
    },
    // {
    //     name: "Sint Maarten",
    //     code: "SX",
    //     emoji: "🇸🇽",
    //     unicode: "U+1F1F8 U+1F1FD",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SX.svg",
    //     countryCode: '+1'
    // },
    {
        name: "Syria",
        code: "SY",
        emoji: "🇸🇾",
        unicode: "U+1F1F8 U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SY.svg",
        countryCode: "+963"
    },
    {
        name: "Eswatini",
        code: "SZ",
        emoji: "🇸🇿",
        unicode: "U+1F1F8 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SZ.svg",
        countryCode: "+268"
    },
    {
        name: "Tristan da Cunha",
        code: "TA",
        emoji: "🇹🇦",
        unicode: "U+1F1F9 U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TA.svg",
        countryCode: "+290"
    },
    // {
    //     name: "Turks & Caicos Islands",
    //     code: "TC",
    //     emoji: "🇹🇨",
    //     unicode: "U+1F1F9 U+1F1E8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TC.svg",
    //     countryCode: "+1"
    // },
    {
        name: "Chad",
        code: "TD",
        emoji: "🇹🇩",
        unicode: "U+1F1F9 U+1F1E9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TD.svg",
        countryCode: "+235"
    },
    {
        name: "French Southern Territories",
        code: "TF",
        emoji: "🇹🇫",
        unicode: "U+1F1F9 U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TF.svg",
        countryCode: "+262"
    },
    {
        name: "Togo",
        code: "TG",
        emoji: "🇹🇬",
        unicode: "U+1F1F9 U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TG.svg",
        countryCode: "+228"
    },
    {
        name: "Thailand",
        code: "TH",
        emoji: "🇹🇭",
        unicode: "U+1F1F9 U+1F1ED",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TH.svg",
        countryCode: "+66"
    },
    {
        name: "Tajikistan",
        code: "TJ",
        emoji: "🇹🇯",
        unicode: "U+1F1F9 U+1F1EF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TJ.svg",
        countryCode: "+992"
    },
    {
        name: "Tokelau",
        code: "TK",
        emoji: "🇹🇰",
        unicode: "U+1F1F9 U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TK.svg",
        countryCode: "+690"
    },
    {
        name: "Timor-Leste",
        code: "TL",
        emoji: "🇹🇱",
        unicode: "U+1F1F9 U+1F1F1",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TL.svg",
        countryCode: "+670"
    },
    {
        name: "Turkmenistan",
        code: "TM",
        emoji: "🇹🇲",
        unicode: "U+1F1F9 U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TM.svg",
        countryCode: "+993"
    },
    {
        name: "Tunisia",
        code: "TN",
        emoji: "🇹🇳",
        unicode: "U+1F1F9 U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TN.svg",
        countryCode: "+216"
    },
    {
        name: "Tonga",
        code: "TO",
        emoji: "🇹🇴",
        unicode: "U+1F1F9 U+1F1F4",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TO.svg",
        countryCode: "+676"
    },
    {
        name: "Turkey",
        code: "TR",
        emoji: "🇹🇷",
        unicode: "U+1F1F9 U+1F1F7",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TR.svg",
        countryCode: "+90"
    },
    {
        name: "Trinidad & Tobago",
        code: "TT",
        emoji: "🇹🇹",
        unicode: "U+1F1F9 U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TT.svg",
        countryCode: "+1868"
    },
    {
        name: "Tuvalu",
        code: "TV",
        emoji: "🇹🇻",
        unicode: "U+1F1F9 U+1F1FB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TV.svg",
        countryCode: "+688"
    },
    {
        name: "Taiwan",
        code: "TW",
        emoji: "🇹🇼",
        unicode: "U+1F1F9 U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TW.svg",
        countryCode: "+886"
    },
    {
        name: "Tanzania",
        code: "TZ",
        emoji: "🇹🇿",
        unicode: "U+1F1F9 U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TZ.svg",
        countryCode: "+255"
    },
    {
        name: "Ukraine",
        code: "UA",
        emoji: "🇺🇦",
        unicode: "U+1F1FA U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UA.svg",
        countryCode: "+380"
    },
    {
        name: "Uganda",
        code: "UG",
        emoji: "🇺🇬",
        unicode: "U+1F1FA U+1F1EC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UG.svg",
        countryCode: "+256"
    },
    // {
    //     name: "U.S. Outlying Islands",
    //     code: "UM",
    //     emoji: "🇺🇲",
    //     unicode: "U+1F1FA U+1F1F2",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UM.svg",
    //     countryCode: null
    // },
    // {
    //     name: "United Nations",
    //     code: "UN",
    //     emoji: "🇺🇳",
    //     unicode: "U+1F1FA U+1F1F3",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UN.svg",
    //     countryCode: null
    // },
    {
        name: "United States",
        code: "US",
        emoji: "🇺🇸",
        unicode: "U+1F1FA U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/US.svg",
        countryCode: "+1"
    },
    {
        name: "Uruguay",
        code: "UY",
        emoji: "🇺🇾",
        unicode: "U+1F1FA U+1F1FE",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UY.svg",
        countryCode: "+598"
    },
    {
        name: "Uzbekistan",
        code: "UZ",
        emoji: "🇺🇿",
        unicode: "U+1F1FA U+1F1FF",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UZ.svg",
        countryCode: "+998"
    },
    {
        name: "Vatican City",
        code: "VA",
        emoji: "🇻🇦",
        unicode: "U+1F1FB U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VA.svg",
        countryCode: "+379"
    },
    // {
    //     name: "St. Vincent & Grenadines",
    //     code: "VC",
    //     emoji: "🇻🇨",
    //     unicode: "U+1F1FB U+1F1E8",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VC.svg",
    //     countryCode: "+1"
    // },
    {
        name: "Venezuela",
        code: "VE",
        emoji: "🇻🇪",
        unicode: "U+1F1FB U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VE.svg",
        countryCode: "+58"
    },
    // {
    //     name: "British Virgin Islands",
    //     code: "VG",
    //     emoji: "🇻🇬",
    //     unicode: "U+1F1FB U+1F1EC",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VG.svg",
    //     countryCode: "+1"
    // },
    // {
    //     name: "U.S. Virgin Islands",
    //     code: "VI",
    //     emoji: "🇻🇮",
    //     unicode: "U+1F1FB U+1F1EE",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VI.svg",
    //     countryCode: "+1"
    // },
    {
        name: "Vietnam",
        code: "VN",
        emoji: "🇻🇳",
        unicode: "U+1F1FB U+1F1F3",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VN.svg",
        countryCode: "+84"
    },
    {
        name: "Vanuatu",
        code: "VU",
        emoji: "🇻🇺",
        unicode: "U+1F1FB U+1F1FA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VU.svg",
        countryCode: "+678"
    },
    {
        name: "Wallis & Futuna",
        code: "WF",
        emoji: "🇼🇫",
        unicode: "U+1F1FC U+1F1EB",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WF.svg",
        countryCode: "+681"
    },
    {
        name: "Samoa",
        code: "WS",
        emoji: "🇼🇸",
        unicode: "U+1F1FC U+1F1F8",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WS.svg",
        countryCode: "+685"
    },
    {
        name: "Kosovo",
        code: "XK",
        emoji: "🇽🇰",
        unicode: "U+1F1FD U+1F1F0",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/XK.svg",
        countryCode: "+383"
    },
    {
        name: "Yemen",
        code: "YE",
        emoji: "🇾🇪",
        unicode: "U+1F1FE U+1F1EA",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YE.svg",
        countryCode: "+967"
    },
    {
        name: "Mayotte",
        code: "YT",
        emoji: "🇾🇹",
        unicode: "U+1F1FE U+1F1F9",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YT.svg",
        countryCode: "+262"
    },
    {
        name: "South Africa",
        code: "ZA",
        emoji: "🇿🇦",
        unicode: "U+1F1FF U+1F1E6",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZA.svg",
        countryCode: "+27"
    },
    {
        name: "Zambia",
        code: "ZM",
        emoji: "🇿🇲",
        unicode: "U+1F1FF U+1F1F2",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZM.svg",
        countryCode: "+260"
    },
    {
        name: "Zimbabwe",
        code: "ZW",
        emoji: "🇿🇼",
        unicode: "U+1F1FF U+1F1FC",
        image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZW.svg",
        countryCode: "+263"
    },
    // {
    //     name: "England",
    //     code: "ENGLAND",
    //     emoji: "🏴",
    //     unicode: "U+1F3F4 U+E0067 U+E0062 U+E0065 U+E006E U+E0067 U+E007F",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ENGLAND.svg",
    //     countryCode: null
    // },
    // {
    //     name: "Scotland",
    //     code: "SCOTLAND",
    //     emoji: "🏴",
    //     unicode: "U+1F3F4 U+E0067 U+E0062 U+E0073 U+E0063 U+E0074 U+E007F",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SCOTLAND.svg",
    //     countryCode: null
    // },
    // {
    //     name: "Wales",
    //     code: "WALES",
    //     emoji: "🏴",
    //     unicode: "U+1F3F4 U+E0067 U+E0062 U+E0077 U+E006C U+E0073 U+E007F",
    //     image: "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WALES.svg",
    //     countryCode: null
    // }
]


