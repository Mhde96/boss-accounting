import React from "react";
import { NavBar as WindowNavBar } from "react-windows-ui";

import { isElectron } from "../../utils/isElectron";

interface NavBarType {
  children: React.ReactNode;
}
export const NavBar = ({ children }: NavBarType) => {
  if (isElectron)
    return (
      <div id="nav-bar">
        
        <WindowNavBar>{children}</WindowNavBar>
      </div>
    );
  else return <div className="sidebar-container">{children}</div>;
};
