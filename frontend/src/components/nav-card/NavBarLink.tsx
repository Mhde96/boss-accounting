import React, { useEffect } from "react";
import { isElectron } from "../../utils/isElectron";
import { NavBarLink as WindowNavBarLink } from "react-windows-ui";
import { useAnimationControls, motion } from "framer-motion";
import { useColors } from "../../styles/variables-styles";
import { ReactSVG } from "react-svg";
import "./nav.scss";
import { electronStatic } from "../../constant/configEndpoints";
import classNames from "classnames";

type interfaceType = {
  icon: string;
  text: string;
  href: string;
  handleNavigate: () => void;
  active: boolean;
  disabled?: boolean | number;
};

export const NavBarLink = ({
  icon,
  text,
  handleNavigate,
  active,
  disabled,
}: interfaceType) => {
  const controls = useAnimationControls();
  const colors = useColors();

  useEffect(() => {
    if (active)
      controls.start({
        background: colors.background,
        color: colors.onSurface,
      });
    else
      controls.start({
        background: colors.surface,
      });
  }, [active]);

  const handleClick = () => {
    if (disabled) {
      return;
    }

    handleNavigate();
  };

  const classes = classNames({
    ["disabled"]: disabled,
  });

  if (isElectron)
    return (
      <div id="nav-bar-link" className={classes}>
          <WindowNavBarLink
            onClick={handleClick}
            active={active}
            text={text}
            // imgSrc={"static://___electron_copy_assets/" + icon}
            icon={
              <ReactSVG className="windows-icon" src={electronStatic + icon} />
            }
          />
      </div>
    );

  return (
    <div id="nav-bar-link" className={classes}>
      <motion.div
        animate={controls}
        className={ " nav-card"}
        onClick={handleClick}
      >
        <ReactSVG
          className="img"
          style={{ minWidth: 24, maxWidth: 24 }}
          src={icon}
        />
        <div className="content">{text}</div>
      </motion.div>
    </div>
  );
};
