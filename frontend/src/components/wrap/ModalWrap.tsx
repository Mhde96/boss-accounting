import React, { ReactNode } from "react";
import { Modal } from "react-bootstrap";
import { useColors } from "../../styles/variables-styles";
import { useColorMemo } from "../../hook/useColorMemo";
import { isElectron } from "../../utils/isElectron";
// import { Dialog } from "react-windows-ui";
import "./modal-warp.scss";
interface ModalWrapType {
  children: ReactNode;
  show: boolean;
  onHide: () => void;
  size?: "lg";
  backdrop?: "static" | boolean;
}
export const ModalWrap = ({
  children,
  show,
  onHide,
  ...props
}: ModalWrapType) => {
  const colors = useColors();
  const colorMode = useColorMemo();

  const backgroundStyle = {
    backgroundColor: colors.surface,
    color: colors.text,
  };

  // if (isElectron)
  //   return (
  //     <Dialog isVisible={show} onBackdropPress={onHide}>
  //       <div
  //         className={`theme--${colorMode}`}
  //         style={{ ...backgroundStyle, padding: 15 }}
  //       >
  //         {children}
  //       </div>
  //     </Dialog>
  //   );
  if (isElectron)
    return (
      <Modal show={show} onHide={onHide} {...props} centered >
        <div className={`theme--${colorMode} `} style={{ ...backgroundStyle }}>
          <div className="modal-warp-windows">{children}</div>
        </div>
      </Modal>
    );

  return (
    <Modal show={show} onHide={onHide} {...props}>
      <div className={`theme--${colorMode}`} style={backgroundStyle}>
        {children}
      </div>
    </Modal>
  );
};
