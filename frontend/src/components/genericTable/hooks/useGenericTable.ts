import { useEffect, useState } from "react";
import { api } from "../../../helper/api";
import { CustomTableType } from "../container/CustomTable";
import { getAdminUsersApi } from "../../../apis/admin/getAdminUsersApi";
import { endpoints } from "../../../constant/endpoints";

export const useGenericTable = ({ serviceName, mapper }: CustomTableType) => {
  const [columns, setColumns] = useState<any>();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [searchTerm, setSearchTerm] = useState("");
  const [debounceSearchTerm, setDebounceSearchTerm] = useState("");
  const [sortedColumn, setSortedColumn] = useState<string | null>("id");
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");
  const [totalPages, setTotalPages] = useState(1);

  const handlePageChange = (page: number) => {
    setCurrentPage(+page);
  };

  const handlePageSizeChange = (e: any) => {
    setPageSize(parseInt(e.target.value, 10));
    setCurrentPage(1); // Reset to the first page when changing page size
  };

  const handleSortTable = (e: any) => {
    setSortedColumn(e.target.value.toLowerCase());
  };

  const handleSort = (column: string) => {
    if (sortedColumn === column) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortedColumn(column);
      setSortOrder("asc");
    }
  };

  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);

        const response = await api.get(
          endpoints.get_admin_users({
            serviceName,
            currentPage,
            pageSize,
            debounceSearchTerm,
            sortOrder,
            sortedColumn,
          })
        );

        const mapperData = response.data?.data.map((item: any) =>
          mapper.reduce((acc: any, key: any) => {
            if (item.hasOwnProperty(key)) {
              acc[key] = item[key];
            }
            return acc;
          }, {})
        );

        setData(mapperData);
        const userColumns = Object.keys(mapperData?.[0]).map((key) => ({
          key: key,
          label: key.charAt(0).toUpperCase() + key.slice(1),
        }));
        setColumns(userColumns);
        setCurrentPage(response.data.currentPage);
        setTotalPages(response.data.totalPages);

        setLoading(false);
      } catch (error: any) {
        if (error?.message) setError(error?.message);
        else setError("Some thing went wrong");
        setLoading(false);
      }
    };

    fetchData();
  }, [sortedColumn, sortOrder, debounceSearchTerm, pageSize, currentPage]);

  return {
    data,
    columns,
    sortOrder,
    sortedColumn,
    searchTerm,
    pageSize,
    currentPage,
    handleSort,
    handlePageSizeChange,
    handlePageChange,
    setSearchTerm,
    totalPages,
    setDebounceSearchTerm,
    loading,
    error,
    handleSortTable,
  };
};
