// TableComponent.tsx
import React from "react";
import { Table, Pagination, Form } from "react-bootstrap";
import "../style/GenericTable.scss";
import { colors } from "../../../styles/variables-styles";

interface Column {
  key: string;
  label: string;
}

interface DataItem {
  [key: string]: any;
}
type SetStateProp = React.Dispatch<React.SetStateAction<any>>;

interface GenericTableProps {
  data: DataItem[];
  columns: Column[];
  currentPage: any;
  pageSize: any;
  sortedColumn: any;
  searchTerm: any;
  sortOrder: any;
  handlePageChange: (page: number) => void;
  handlePageSizeChange: (e: any) => void;
  handleSortTable: (e: any) => void;
  handleSort: (column: string) => void;
  setSearchTerm: SetStateProp;
  setDebounceSearchTerm: SetStateProp;
  totalPages: number;
  error?: string | null;
}

const pageSizeOptions = [1, 3, 5, 10, 20];

const GenericTable: React.FC<GenericTableProps> = ({
  data,
  columns,
  handleSort,
  handlePageSizeChange,
  handlePageChange,
  sortOrder,
  sortedColumn,
  searchTerm,
  pageSize,
  currentPage,
  setSearchTerm,
  totalPages,
  setDebounceSearchTerm,
  error,
  handleSortTable,
}) => {
  return (
    <div className="generic-table-container">
      <Form>
        <Form.Group controlId="searchTerm" style={{ width: "40%" }}>
          <Form.Control
            type="text"
            placeholder="Search..."
            value={searchTerm}
            onChange={(e) => {
              setSearchTerm(e.target.value);
              setTimeout(() => {
                setDebounceSearchTerm(e.target.value);
              }, 2000);
            }}
          />
        </Form.Group>
      </Form>
      <Table className="generic-table" striped bordered hover>
        <thead>
          <tr>
            {columns.map((column) => (
              <th
                key={column.key}
                style={{ color: colors.primary }}
                onClick={() => handleSort(column.key)}
              >
                {column.label}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={index}>
              {columns.map((column) => (
                <td key={column.key}>{item[column.key]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
      <div className="pagination-container">
        <Pagination>
          <Pagination.Prev
            disabled={currentPage == 1}
            onClick={() => handlePageChange(+currentPage - 1)}
          />
          {Array.from({ length: totalPages }).map((_, index) => (
            <Pagination.Item
              key={index + 1}
              active={index + 1 == currentPage}
              style={{ backgroundColor: colors.primary }}
              onClick={() => handlePageChange(index + 1)}
            >
              {index + 1}
            </Pagination.Item>
          ))}
          <Pagination.Next
            disabled={currentPage == totalPages}
            onClick={() => handlePageChange(+currentPage + 1)}
          />
        </Pagination>
      </div>
      <Form.Group controlId="pageSize" className="page-size-select">
        <Form.Label>Page Size</Form.Label>
        <Form.Control
          style={{ width: "40%" }}
          as="select"
          onChange={handlePageSizeChange}
          value={pageSize}
        >
          {pageSizeOptions.map((size) => (
            <option key={size} value={size}>
              {size}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
      <Form.Group controlId="columns" className="page-size-select">
        <Form.Label>Sort By</Form.Label>
        <Form.Control
          style={{ width: "40%" }}
          as="select"
          onChange={handleSortTable}
        >
          {columns.map((column) => (
            <option
              key={column.key}
              onClick={() => handleSort(column.key.toLowerCase())}
            >
              {column.label}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
    </div>
  );
};

export default GenericTable;
