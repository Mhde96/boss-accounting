import React from 'react'
import { Spinner } from "react-bootstrap";
import { useGenericTable } from "../hooks/useGenericTable";
import GenericTable from "../components/GenericTable";

export type CustomTableType = {
  serviceName: string;
  mapper: string[];
};

const CustomTable = ({ serviceName, mapper }: CustomTableType) => {
  const {
    data,
    columns,
    sortOrder,
    sortedColumn,
    searchTerm,
    pageSize,
    currentPage,
    handleSort,
    handlePageSizeChange,
    handlePageChange,
    setSearchTerm,
    totalPages,
    setDebounceSearchTerm,
    loading,
    error,
    handleSortTable,
  } = useGenericTable({ serviceName, mapper });

  if (loading) {
    return <Spinner animation="grow" variant="warning" />;
  }

  //   if (error) {
  //     return <div>Error: {error}</div>;
  //   }

  return (
    <>
      {error ? (
        <div className="d-flex justify-content-center align-items-center">
          Error: {error}
        </div>
      ) : (
        <GenericTable
          data={data}
          columns={columns}
          sortOrder={sortOrder}
          sortedColumn={sortedColumn}
          searchTerm={searchTerm}
          pageSize={pageSize}
          currentPage={currentPage}
          handleSort={handleSort}
          handlePageSizeChange={handlePageSizeChange}
          handlePageChange={handlePageChange}
          setSearchTerm={setSearchTerm}
          totalPages={totalPages}
          setDebounceSearchTerm={setDebounceSearchTerm}
          error={error}
          handleSortTable={handleSortTable}
        />
      )}
    </>
  );
};
export default CustomTable;
