type ButtonProps = {
  children: any;
  onClick?: any;
  loading?: boolean;
  variant?: 'primary' | 'danger';
};
export type { ButtonProps };
