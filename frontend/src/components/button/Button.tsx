import React from "react";
import { Button as BootstrapButton } from "react-bootstrap";
import { ButtonProps } from "./button-type";
import "./button-style.scss";
import { Button as WindowsButton } from "react-windows-ui";

export const Button = ({
  children,
  onClick,
  loading,
  variant,
}: ButtonProps) => {
  const isElectron = navigator.userAgent.includes("Electron");

  if (isElectron) {
    return (
      <WindowsButton
        style={{ margin: "0px 5px" }}
        type={variant}
        onClick={onClick}
        value={children}
        isLoading={loading}
      ></WindowsButton>
    );
  }

  return (
    <BootstrapButton
      // className="button-style"
      onClick={onClick}
      disabled={loading}
      variant={variant}
    >
      {children}
    </BootstrapButton>
  );
};
