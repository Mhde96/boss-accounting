export type AccountCardTypeProps = {
  id?: number,
  account_key?: string | number;
  name: string;
  isHeader?: boolean;
  index?: number;
  financial_statement?: number;
  operations?: {
    open?: any;
    update?: any;
    delete?: any;
  };
};
