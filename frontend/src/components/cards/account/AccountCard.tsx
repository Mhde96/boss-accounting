import React from "react";
import "./account-card-styles.scss";
import { motion } from "framer-motion";
import { AccountCardTypeProps } from "./account-card-type";
import { useColors } from "../../../styles/variables-styles";

import { TrashIcon } from "../../../assets/icons/TrashIcon";
import { EditIcon } from "../../../assets/icons/EditIcon";
import { CardAnimationProps } from "../CardAnimation";
import { financial_statement_array } from "../../../constant/account_constant";

export const AccountCard = (props: AccountCardTypeProps) => {
  const colors = useColors();
  const { name, operations, index, financial_statement, id } = props;

  const AnimationProps = { ...CardAnimationProps({ index }) };
  const financialStatementName = financial_statement_array.find(
    // @ts-ignore
    (item) => item.value == financial_statement
  )?.label;
  return (
    <motion.div {...AnimationProps} id="account-card-styles">
      <div className="name" onClick={() => operations?.open()}>
        {name}
      </div>

      <div
        className="operations"
        style={{
          gap: "30px",
          display: "flex",

          alignItems: "center",
        }}
      >
        {operations?.update && (
          <>
            <div className="tag f5">{financialStatementName}</div>
            {/* <div
              style={{ color: colors.text, cursor: "pointer" }}
              className="name"
              onClick={() => operations?.open()}
            >
              {t(en.statement)}
            </div> */}

            {id > 0 && (
              <EditIcon
                fill={colors.text}
                onClick={operations.update}
                className="icons"
                // color={colors.surface}
              />
            )} 
          </>
        )}
        {operations?.delete && id > 0 && (
          <TrashIcon fill={colors.text} onClick={operations.delete} />
        )}
      </div>
    </motion.div>
  );
};
