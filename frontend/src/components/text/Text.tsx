import React from 'react'

import { TextPropsType } from "./text-type";
import "../../index.scss";
import "./text-style.scss";
import classNames from "classnames";
import { useColors } from "../../styles/variables-styles";
import { isArabic } from "../../utils/isArabic";
export const Text = ({
  children,
  fs,
  bold,
  center,
  breakSpaces,
  color,
  border,
  maxWidth,
  html,
  textAlign,
  onClick,
}: TextPropsType) => {
  const colors = useColors();
  let _color = color ? color : colors.text;

  const classes = classNames(fs, {
    bold,
    center,
    ["break-spaces"]: breakSpaces,
    ["border-text"]: border,
    onClick,
  });

  if (html)
    return (
      <div id="text-style" style={{ color:_color, textAlign }}>
        <div
          dangerouslySetInnerHTML={{ __html: html }}
          dir={isArabic(html) ? "rtl" : undefined}
        />
      </div>
    );

  return (
    <div id="text-style" onClick={onClick}>
      <div className={classes} style={{ color:_color, maxWidth, textAlign }}>
        {children}
      </div>
    </div>
  );
};
