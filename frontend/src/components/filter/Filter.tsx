import React from "react";

import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import "./filter-styles.scss";
import { useColors } from "../../styles/variables-styles";
import {
  JOURNAL_SORT_OPTIONS,
  JOURNAL_SORT_OPTIONS_TYPES,
} from "../../types/JournalType";
import { SortIcon } from "../../assets/icons/SortIcon";
import { ArrowUpNumberSort } from "../../assets/icons/ArrowUpNumberSort";
import { ArrowDownNumberSort } from "../../assets/icons/ArrowDownNumberSort";

// @ts-ignore
export const Filter = ({ onChange }) => {
  const colors = useColors();
  const [state, setState] = useState<JOURNAL_SORT_OPTIONS_TYPES>(
    JOURNAL_SORT_OPTIONS.BY_NUMBER_REVERSE
  );
  const handleChange = (state: string) => {
    onChange(state);
  };
  useEffect(() => {
    // @ts-ignore
    handleChange(state);
  }, [state]);

  return (
    <div id="filter-styles" className="d-flex">
      {/* <img src="assets/icons/sort-solid.svg" /> */}
      <SortIcon width={30} height={30} />
      <span>Sort</span>

      <div className="d-flex box">
        <SelectedBox
          active={state == "byNumberReverse"}
          handleClick={() => setState("byNumberReverse")}
        >
          <ArrowUpNumberSort
            width={30}
            height={30}
            fill={state == "byNumberReverse" ? colors.onPrimary :colors.text }

          />
        </SelectedBox>
        <SelectedBox
          active={state == "byNumber"}
          handleClick={() => setState("byNumber")}
        >
          <ArrowDownNumberSort
            width={30}
            height={30}
            fill={state == "byNumber" ? colors.onPrimary :colors.text }
          />
        </SelectedBox>

        {/* <SelectedBox
          active={state == "News"}
          handleClick={() => setState("News")}
        >
          <span>News</span>
        </SelectedBox>
        <SelectedBox
          active={state == "Older"}
          handleClick={() => setState("Older")}
        >
          <span>Older</span>
        </SelectedBox>
        <img src="assets/icons/calendar-days-solid.svg" /> */}
      </div>
    </div>
  );
};

// @ts-ignore
const SelectedBox = ({ children, active, handleClick }) => {
  const colors = useColors();
  return (
    <motion.div
      className="selected-box"
      animate={{ background: active ? colors.primary : colors.surface }}
      onClick={handleClick}
    >
      {children}
    </motion.div>
  );
};
