import { Modal } from "react-bootstrap";
import React from "react";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { useColors } from "../../styles/variables-styles";
import { isElectron } from "../../utils/isElectron";
import { Dialog } from "react-windows-ui/dist";
import { Button } from "../button/Button";
interface CustomModalProps {
  show: boolean;
  onHide: () => void;
  title: string;
  onSubmit: () => void;
  children: any;
}

export const PopUp: React.FC<CustomModalProps> = ({
  show,
  onHide,
  title,
  children,
  onSubmit,
}) => {
  const colors = useColors();
  const backgroundStyle = {
    backgroundColor: colors.surface,
    color: colors.text,
  };

  const { t } = useTranslation();

  if (isElectron)
    return (
      <Dialog isVisible={show} onBackdropPress={onHide}>
        <div style={{ ...backgroundStyle, padding: 15 }}>
          <h2>{title}</h2>
          <div className="py-5"> {children}</div>

          <div className="d-flex flex-row justify-content-end">
            <Button  onClick={onSubmit}>
              {t(en.add)}
            </Button>
            &nbsp;
            <Button   onClick={onHide}>
              {t(en.cancel)}
            </Button>
          </div>
        </div>
      </Dialog>
    );

  return (
    <Modal show={show} onHide={onHide} style={{ padding: "30px" }} className="">
      <div style={backgroundStyle}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={onHide}
          >
            {t(en.cancel)}
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              onSubmit();
            }}
          >
            {t(en.add)}
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};
