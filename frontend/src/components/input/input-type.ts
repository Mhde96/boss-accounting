export type InputPropsType = {
  placeholder: string;
  onChange?: (e: any) => void;
  value?: any;
  error?: string;
  disabled?: boolean;
  type?:  "text" | "date" | "time" | "datetime-local" | "month" | "password";
  maxWidth?: number;
  textArea?: boolean;
  inlineDatePicker?: boolean
  typeOfInput?: 'password' | undefined | "countryCode" | "SelectOccupations";
};
