import React from "react";
import Select from "react-select";
import { occupations } from "../../constant/occupations";
import { useTranslation } from "react-i18next";

// @ts-ignore
export const SelectOccupations = ({ error, onChange, placeholder }) => {
  const { t } = useTranslation();

  const options = occupations.map((item) => ({
    ...item,
    value: item.id,
    label: t(item.name),
  }));

  return (
    <div>
      <Select
        placeholder={placeholder}
        options={options}
        onChange={(value) => {
          onChange(value?.id);
        }}
        styles={{
          control: (baseStyles) => ({
            ...baseStyles,
            border: error ? "1px solid red" : "",
          }),
          menuList: (baseStyles, state) => ({
            ...baseStyles,
            fontSize: 14,
          }),
        }}
      />
      <div className="error" style={{ position: "absolute" }}>
        {error}
      </div>
    </div>
  );
};
