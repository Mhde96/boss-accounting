import React from "react";
import { Form, InputGroup } from "react-bootstrap";
import "./input-style.scss";
import { InputPropsType } from "./input-type";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { datePickerFormat } from "../../utils/date-format";
import JoditEditor from "jodit-react";
import { isArabic } from "../../utils/isArabic";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { CountryCodeInput } from "./CountryCodeInput";
import { SelectOccupations } from "./SelectOccupations";
import { isElectron } from "../../utils/isElectron";
import WindowsInputText from "react-windows-ui/dist/components/InputText";
export const Input = ({
  type,
  onChange,
  value,
  placeholder,
  error,
  disabled,
  maxWidth,
  textArea,
  typeOfInput,
  inlineDatePicker,
  ...props
}: InputPropsType) => {
  const [showPassword, setShowPassword] = React.useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const typeOfinputPassword = showPassword ? "text" : "password";

  if (textArea) {
    // return (
    //   <div id="input-style">
    //     <JoditEditor onChange={onChange} value={value}  className="JoditEditor"/>
    //     {error && <div className="error">{error} </div>}
    //   </div>
    // );

    return (
      <div id="input-style">
        <Form.Control
          dir={isArabic(value) ? "rtl" : undefined}
          as="textarea"
          rows={10}
          onBlur={onChange}
          defaultValue={value}
        />
        {error && <div className="error">{error} </div>}
      </div>
    );
  }

  if (type == "date")
    return (
      <div id="input-style">
        <DatePicker
          className="date"
          onChange={onChange}
          selected={new Date(value)}
          dateFormat={datePickerFormat}
          inline={inlineDatePicker}
          disabled={disabled ? true : false}
          {...props}
        />
      </div>
    );

  if (typeOfInput === "password") {
    return (
      <div id="input-style">
        <Form.Group controlId="formPassword">
          <InputGroup>
            <Form.Control
              type={typeOfinputPassword}
              disabled={disabled}
              defaultValue={value}
              // onChange={onChange}
              onBlur={onChange}
              placeholder={placeholder}
              style={{
                maxWidth,
              }}
            />
            <InputGroup.Text onClick={togglePasswordVisibility}>
              {showPassword ? (
                <FontAwesomeIcon icon={faEyeSlash} />
              ) : (
                <FontAwesomeIcon icon={faEye} />
              )}
            </InputGroup.Text>
          </InputGroup>
        </Form.Group>
        {error && <div className="error">{error} </div>}
      </div>
    );
  }
  if (typeOfInput === "countryCode")
    return <CountryCodeInput error={error} onChange={onChange} />;

  if (typeOfInput === "SelectOccupations")
    return (
      <SelectOccupations
        error={error}
        onChange={onChange}
        placeholder={placeholder}
      />
    );

  

  if (isElectron) {
    return (
      <div id="input-style">
        <WindowsInputText
          type={type}
          disabled={disabled}
          defaultValue={value}
          clearButton={true}
          width={300}
          // onChange={onChange}
          onChange={onChange}
          placeholder={placeholder}

          // style={{
          //   maxWidth,
          // }}
        />
        {error && <div className="error">{error} </div>}
      </div>
    );
  }

  return (
    <div id="input-style">
      <Form.Control
        type={type}
        disabled={disabled}
        defaultValue={value}
        // onChange={onChange}
        onBlur={onChange}
        placeholder={placeholder}
        style={{
          maxWidth,
        }}
      />
      {error && <div className="error">{error} </div>}
    </div>
  );
};

export const ErrorMessage = ({ children }: any) => (
  <div id="input-style">
    {children && <div className="error">{children} </div>}
  </div>
);
