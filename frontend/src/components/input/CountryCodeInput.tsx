import React from "react";
import Select from "react-select";
import { countryCode } from "../../constant/countryCode";
export const CountryCodeInput = ({ error, onChange }: any) => {
  const options = countryCode.map((item) => ({
    ...item,
    value: item.countryCode,
    label: item.emoji + " " + item.countryCode,
  }));
  return (
    <div style={{ width: 130 }}>
      <Select
        options={options}
        onChange={(value) => {
          onChange(value?.countryCode)
        }}
        components={{ DropdownIndicator: null }}
        styles={{
          control: (baseStyles) => ({
            ...baseStyles,
            border: error ? "1px solid red" : "",
          }),
          menuList: (baseStyles, state) => ({
            ...baseStyles,
            fontSize: 14,
          }),
        }}
      />
    </div>
  );
};
