import React from "react";

import { Button, Modal } from "react-bootstrap";
import { useColorMemo } from "../../hook/useColorMemo";
import { useColors } from "../../styles/variables-styles";
import { useTranslation } from "react-i18next/";
import { en } from "../../helper/languages/en";

export const ConfirmationDeleteDialog = ({ data, setData, handleSubmit }) => {
  const colors = useColors();
  const colorMode = useColorMemo();
  const handleClose = () => {
    setData({ ...data, show: false });
  };
  const { t } = useTranslation();
  const backgroundStyle = {
    backgroundColor: colors.surface,
    color: colors.text,
  };

  return (
    <Modal onHide={handleClose} show={data.show}>
      <div className={`theme--${colorMode}`} style={backgroundStyle}>
        <Modal.Header>{data.title}</Modal.Header>
        <Modal.Body>{data.body}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            {t(en.cancel)}
          </Button>
          <Button
            type="submit"
            variant="primary"
            onClick={() => {
              handleClose();
              handleSubmit();
            }}
          >
            {t(en.delete)}
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};
