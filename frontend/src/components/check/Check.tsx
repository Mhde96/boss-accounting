import React from "react";
import { isElectron } from "../../utils/isElectron";
import { Checkbox as WindowsCheckbox } from "react-windows-ui";
import { Form } from "react-bootstrap";
import "./check.scss"
interface CheckType {
  label: string;
  onChange: (e: any) => void;
  defaultChecked: boolean;
}
export const Check = ({ label, onChange, defaultChecked }: CheckType) => {
  if (isElectron) {
    return (
      <div id="check">
        <WindowsCheckbox
          label={label}
          onChange={onChange}
          defaultChecked={defaultChecked}
          value={defaultChecked}
        />
      </div>
    );
  } else
    return (
      <Form.Check
        type="checkbox"
        label={label}
        onChange={onChange}
        checked={defaultChecked}
      />
    );
};
