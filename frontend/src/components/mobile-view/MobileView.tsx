import React from "react";
import { Text } from "../text/Text";
import "./mobile-view-styles.scss";
export const MobileView = () => {
  return (
    <div id="mobile-view-styles">
      <div className="mobile-container">
        <Text bold center fs="f1" textAlign="center">
          Boss Accounting
        </Text>
        <img src="assets/icons/desktop.svg" />
        <Text bold center fs="f1" textAlign="center">
          Only works on desktop
        </Text>
      </div>
    </div>
  );
};
