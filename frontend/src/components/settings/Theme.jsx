import React, { memo } from "react";

import appSlice, { selectColorMode } from "../../data/redux/app/appSlice";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { en } from "../../helper/languages/en";
import { useColors } from "../../styles/variables-styles";
import { SettingsCardAnimation } from "../animations/SettingsCardAnimation";
import { useTranslation } from "react-i18next";
import { motion } from "framer-motion";
import { DarkIcon } from "../../assets/icons/DarkIcon";
import { LightIcon } from "../../assets/icons/LightIcon";
import { AutoIcon } from "../../assets/icons/AutoIcon";
import { Text } from "../text/Text";
import "./theme.scss";
import Appearance from "react-windows-ui/dist/api/Appearance";

export const Theme = memo(() => {
  const dispatch = useAppDispatch();
  const colorMode = useAppSelector(selectColorMode);
  const colorScheme = Appearance.getColorScheme();

  const colors = useColors();
  const { t } = useTranslation();

  const handleDarkMode = () => dispatch(appSlice.actions.colorMode("dark"));
  const handleLightMode = () => dispatch(appSlice.actions.colorMode("light"));
  const handleAutoMode = () => dispatch(appSlice.actions.colorMode("auto"));
  return (
    <div id="theme-styles">
      <Text fs="f2">{t(en.theme)}</Text>
      <br />

      <div className="bright-container">
        <motion.div
          {...SettingsCardAnimation(colorMode === "dark")}
          className="bright-button"
          onClick={handleDarkMode}
        >
          <DarkIcon />
          <div className="space" />
          <Text
            fs="f4"
            color={colorMode === "dark" ? colors.onPrimary : colors.text}
          >
            {t(en.dark)}
          </Text>
        </motion.div>
        <motion.div
          {...SettingsCardAnimation(colorMode === "light")}
          className="bright-button"
          onClick={handleLightMode}
        >
          <LightIcon />
          <Text
            fs="f4"
            color={colorMode === "light" ? colors.onPrimary : colors.text}
          >
            {t(en.light)}
          </Text>
        </motion.div>
        <motion.div
          {...SettingsCardAnimation(colorMode === "auto")}
          className="bright-button"
          onClick={handleAutoMode}
        >
          <AutoIcon />
          <Text
            fs="f4"
            color={colorMode === "auto" ? colors.onPrimary : colors.text}
          >
            {t(en.auto)}
          </Text>
        </motion.div>
      </div>
    </div>
  );
});
