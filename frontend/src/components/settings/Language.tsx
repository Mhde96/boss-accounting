import React from "react";

import { LanguageIcon } from "../../assets/icons/LanguageIcon";
import { en } from "../../helper/languages/en";
import { SettingsCardAnimation } from "../animations/SettingsCardAnimation";
import { Text } from "../text/Text";
import { useTranslation } from "react-i18next";
import { useColors } from "../../styles/variables-styles";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import appSlice, {
  selectColorMode,
  selectLanguage,
} from "../../data/redux/app/appSlice";
import { motion } from "framer-motion";

export const Language = () => {
  const dispatch = useAppDispatch();
  const language = useAppSelector(selectLanguage);

  const colors = useColors();
  const { t } = useTranslation();

  const handleLanguage = (language: string) =>
    dispatch(appSlice.actions.language(language));

  const cardStyle = {
    backgroundColor: colors.background,
    fill: colors.text,
  };

  return (
    <>
      <div style={{ display: "flex", alignItems: "center", fill: colors.text }}>
        <Text fs="f2">{t(en.language)}</Text>
        <div className="p-1" />
        <LanguageIcon />
      </div>
      <br />
      <div className="bright-container">
        <motion.div
          {...SettingsCardAnimation(language == "en")}
          style={cardStyle}
          className="bright-button"
          onClick={() => handleLanguage("en")}
        >
          <Text
            fs="f4"
            color={language == "en" ? colors.onPrimary : colors.text}
          >
            English
          </Text>
        </motion.div>

        <motion.div
          {...SettingsCardAnimation(language == "ar")}
          style={cardStyle}
          className="bright-button"
          onClick={() => handleLanguage("ar")}
        >
          <Text
            fs="f4"
            color={language == "ar" ? colors.onPrimary : colors.text}
          >
            عربي
          </Text>
        </motion.div>
      </div>
    </>
  );
};
