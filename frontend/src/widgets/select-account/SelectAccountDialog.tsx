import React, { useMemo } from "react";

import { Button, Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import { selectAccounts } from "../../data/redux/data/dataSlice";
import { SelectAccountDialogPropsType } from "./select-account-dialog-type";

import "./select-account-styles.scss";
import {
  AccountDialog,
  OpenAccountDialog,
} from "../../containers/accounts/AccountDialog";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";
import { en } from "../../helper/languages/en";

export const SelectAccountDialog = (props: SelectAccountDialogPropsType) => {
  const { open, setOpen, onSubmit } = props;
  const accounts = useSelector(selectAccounts);
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();

  const getAccounts = useMemo(() => {
    return accounts;
  }, [accounts]);
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <AccountDialog />

      <ModalWrap show={open} onHide={handleClose}>
        <div id="select-account-styles">
          <Modal.Header closeButton>
            <Modal.Title>Find Account</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="accounts-contaienr">
              {getAccounts.map((item: any, index: number) => (
                <div
                  key={index}
                  className="account-item"
                  onClick={() => onSubmit(item)}
                >
                  {item.name}
                </div>
              ))}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={() => {
                OpenAccountDialog(location, navigate, undefined);
              }}
              style={{ width: 100 }}
            >
              {t(en.add)}
            </Button>

            {/* <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button> */}
          </Modal.Footer>
        </div>
      </ModalWrap>
    </>
  );
};
