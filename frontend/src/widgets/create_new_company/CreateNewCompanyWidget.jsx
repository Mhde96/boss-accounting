import React, { useEffect, useMemo } from "react";
import { useFormik } from "formik";
import { useState } from "react";
import { Input } from "../../components/input/Input";
import { Theme } from "../../components/settings/Theme";
import { Language } from "../../components/settings/Language";
import { AnimatePresence, motion } from "framer-motion";
import "./create-new-company.scss";
import { Button } from "../../components/button/Button";
import { Card } from "react-bootstrap";
import { useColors } from "../../styles/variables-styles";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import {
  selectAppDirection,
  selectColorMode,
  selectLanguage,
} from "../../data/redux/app/appSlice";
import {
  saveCompanyAsync,
  addFinicialAsync,
  fetchCompaniesAsync,
} from "../../data/redux/data/dataAsync";
import * as yup from "yup";
import { SelectCompany } from "../../data/redux/data/dataSlice";
// import { useNavigate } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import { en } from "../../helper/languages/en";
import { useTranslation } from "react-i18next";
import { Text } from "../../components/text/Text";
import { dateFormatUi } from "../../utils/date-format";
import { navigateTo } from "../../helper/navigationService";

export const CreateNewCompanyWidget = () => {
  const dispatch = useAppDispatch();
  const language = useAppSelector(selectLanguage);
  const colorMode = useAppSelector(selectColorMode);
  const company = useAppSelector(SelectCompany);
  const colors = useColors();
  const [step, setStep] = useState(0);
  const { t } = useTranslation();
  const appDirection = useAppSelector(selectAppDirection);

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        name: yup.string().required(t(en.name_is_required)),
        language: yup.string().required(t(en.field_is_required)),
        color_mode: yup.string().required(t(en.field_is_required)),
        date: yup.string().required(t(en.field_is_required)),
      }),
    [language]
  );

  const { values, setValues, handleSubmit, handleChange, errors } = useFormik({
    initialValues: {
      name: "",
      language: "",
      color_mode: "",
      date: new Date(),
    },
    validationSchema,
    validateOnChange: false,
    validateOnBlur: false,
    validateOnMount: false,

    onSubmit: (values) => {
      dispatch(saveCompanyAsync(values));
      dispatch(addFinicialAsync({ start_at: values.date.toISOString() }));
    },
  });

  useEffect(() => {
    dispatch(fetchCompaniesAsync());
  }, []);

  useEffect(() => {
    if (company?.id) {
      setStep(3);
    }
  }, [company?.id]);

  const handleNext = () => {
    if (step == 0) {
      setValues({ ...values, language: language });
      setStep(1);
    } else if (step == 1) {
      setValues({ ...values, color_mode: colorMode });
      setStep(2);
    } else if (step == 2) {
      handleSubmit();
    } else if (step == 3) navigateTo(endroutes.home.path);
  };
  const handleBack = () => setStep(step - 1);

  const NextButtonProperty = () => {
    let title = "";
    if (step == 0 || step == 1) title = t(en.next);
    else if (step == 2) title = t(en.create);
    else title = t(en.continue);

    return { title };
  };

  const BackButtonProperty = () => {
    let title = t(en.back);
    let isShow = false;
    if (step == 0) isShow = false;
    else if (step == 1 || step == 2) isShow = true;
    else isShow = false;

    return { title, isShow };
  };

  const backgroundStyle = {
    background: colors.surface,
    color: colors.text,
    width: 700,
    height: 700,
  };

  const animation = useMemo(() => ({
    initial: { opacity: 0, display: "none" },
    animate: {
      opacity: 1,
      transition: { duration: 0.5, delay: 1.5 },
      display: "block",
    },
    exit: { opacity: 0 },
  }));

  const onClickStep = (value) => {
    setStep(value);
  };

  return (
    <div id="create-new-company" dir={appDirection} lang={language}>
      <motion.div
        className="card interior"
        style={backgroundStyle}
        animate={{
          height: step > 1 ? backgroundStyle.height : step == 1 ? 450 : 350,
          transition: {
            duration: 2,
            bounce: false,
          },
        }}
      >
        <Card.Header>
          <div className="progress-container">
            <motion.div
              className="step"
              onClick={() => onClickStep(0)}
              animate={{
                background: step > 0 ? colors.primary : colors.surface,
                color: step > 0 ? colors.onPrimary : colors.text,
                borderColor: step >= 0 ? colors.primary : colors.border,
              }}
            >
              1
            </motion.div>
            <motion.div
              className="line"
              animate={{
                background: step >= 1 ? colors.primary : colors.border,
                transition: { duration: 1 },
              }}
            ></motion.div>

            <motion.div
              className="step"
              onClick={() => onClickStep(1)}
              animate={{
                background: step > 1 ? colors.primary : colors.surface,
                color: step > 1 ? colors.onPrimary : colors.text,
                borderColor: step >= 1 ? colors.primary : colors.border,
                transition: { delay: 0.5, duration: 1 },
              }}
            >
              2
            </motion.div>
            <motion.div
              className="line"
              animate={{
                background: step >= 2 ? colors.primary : colors.border,
                transition: { duration: 1 },
              }}
            ></motion.div>
            <motion.div
              className="step"
              onClick={() => onClickStep(2)}
              animate={{
                background: step > 2 ? colors.primary : colors.surface,
                color: step > 2 ? colors.onPrimary : colors.text,
                borderColor: step >= 2 ? colors.primary : colors.border,
                transition: { delay: 0.5, duration: 1 },
              }}
            >
              3
            </motion.div>
          </div>
        </Card.Header>
        <Card.Body>
          <AnimatePresence>
            {step === 0 && (
              <motion.div
                initial={animation.initial}
                animate={animation.animate}
                exit={animation.exit}
              >
                <Language />
              </motion.div>
            )}
          </AnimatePresence>

          <AnimatePresence>
            {step === 1 && (
              <motion.div
                initial={animation.initial}
                animate={animation.animate}
                exit={animation.exit}
              >
                <Theme />
              </motion.div>
            )}
          </AnimatePresence>

          <AnimatePresence>
            {step === 2 && (
              <motion.div
                initial={animation.initial}
                animate={animation.animate}
                exit={animation.exit}
              >
                <section className="company-section">
                  <Text fs="f2">{t(en.enter_your_company_name)}</Text>
                  <Input
                    onChange={handleChange("name")}
                    placeholder={t(en.name)}
                    value={values.name}
                    error={errors?.name}
                  />
                </section>

                <section className="date-section">
                  <Text fs="f2">{t(en.enter_your_start_date)}</Text>
                  <div className="date-picker-container">
                    <Input
                      type="date"
                      value={values.date}
                      inlineDatePicker={true}
                      showMonthYearPicker
                      onChange={(date) => {
                        setValues({ ...values, date: dateFormatUi({ date }) });
                      }}
                    />

                    <Input
                      type="date"
                      value={values.date}
                      inlineDatePicker={true}
                      onChange={(date) => {
                        setValues({ ...values, date: dateFormatUi({ date }) });
                      }}
                      renderCustomHeader={() => <></>}
                    />
                  </div>
                  <div className="selected-date">
                    <Text fs="f1">
                      {" "}
                      {dateFormatUi({ date: values.date })} 📅
                    </Text>
                  </div>
                  <div className="errpr">{errors.date}</div>
                </section>
              </motion.div>
            )}
          </AnimatePresence>

          <AnimatePresence>
            {step === 3 && (
              <motion.div
                initial={animation.initial}
                animate={animation.animate}
                exit={animation.exit}
              >
                <div className="congratulation">
                  <p> {t(en.congratulation)} </p>
                  <h1>🎉</h1>
                  <Text fs="f2">{t(en.first_step)}</Text>
                  <motion.div
                    style={{ paddingTop: 30 }}
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1, transition: { delay: 3 } }}
                  >
                    <Text center fs="f3" bold>
                      {t(en.start_using_app)}
                    </Text>
                  </motion.div>
                </div>
              </motion.div>
            )}
          </AnimatePresence>
        </Card.Body>
        <Card.Footer className="d-flex gap-2">
          {step !== 3 ? (
            <>
              <div className="w-100" />
              <div className="w-100" />
              {BackButtonProperty().isShow ? (
                <Button onClick={handleBack}>
                  {BackButtonProperty().title}
                </Button>
              ) : (
                <div className="w-100" />
              )}
              <Button onClick={handleNext}>{NextButtonProperty().title}</Button>
            </>
          ) : (
            <Button onClick={handleNext}>{NextButtonProperty().title}</Button>
          )}
        </Card.Footer>
      </motion.div>
    </div>
  );
};
