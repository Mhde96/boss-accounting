import React from "react";

import { useMemo } from "react";
import { Button, Modal } from "react-bootstrap";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import { Text } from "../../components/text/Text";
import "./settings-styles.scss";


import { useColors } from "../../styles/variables-styles";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { Theme } from "../../components/settings/Theme";
import { Language } from "../../components/settings/Language";

const link = "settings";

export const SettingsDialogWidget = () => {
  const { t } = useTranslation();
  const colors = useColors();

  // navigation
  const [searchParams] = useSearchParams();
  const { search } = useLocation();
  const navigate = useNavigate();
  const isOpen: any = useMemo(() => searchParams.get(link), [search]);
  const handleClose = () => navigate(-1);

 
  const backgroundStyle = {
    backgroundColor: colors.surface,
    color: colors.text,
  };

 
  return (
    <Modal show={isOpen} onHide={handleClose} placement="end">
      <div id="settings-styles" style={backgroundStyle}>
        <Modal.Header closeButton>
          <Text bold fs="f2">
            {t(en.settings)}
          </Text>
        </Modal.Header>

        <Modal.Body>
      

          <Theme />
          <hr />

          <Language />
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={handleClose}>{t(en.accept)}</Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export const openSettingsDialog = (location: any, navigate: any) => {
  if (location.search)
    navigate(location.pathname + location.search + `&${link}=` + 1);
  else navigate(location.pathname + `?${link}=` + 1);
};
