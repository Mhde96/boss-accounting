import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../data/redux/hooks";
import { HeaderLinkCardType } from "./header-widget-type";
import { useColors } from "../../styles/variables-styles";
import { useSelector } from "react-redux";
import appSlice, {
  selectCurrentPathNameLayout,
} from "../../data/redux/app/appSlice";
import { motion } from "framer-motion";
import { ReactSVG } from "react-svg";

export const HeaderLinkCard = ({
  iconPath,
  href,
  title,
  onClick,
}: HeaderLinkCardType) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const colors = useColors();
  const location = useLocation();

  const isActive = location.pathname === href; // Check if the link is active

  const handleClick = () => {
    dispatch(appSlice.actions.changeCurrentPathNameLayout(href));
    if (onClick) {
      onClick();
    } else {
      navigate(href);
    }
  };

  return (
    <motion.div
      className="header-link-card"
      onClick={handleClick}
      animate={{
        background: isActive ? colors.primary : colors.surface,
      }}
    >
      <ReactSVG className="img" src={iconPath} />
      <motion.div
      layout
        animate={{
          opacity: isActive ? 1 : 0,
          width: "max-content",
        }}
        className="active"
      >
        {isActive ? title : null}
      </motion.div>
    </motion.div>
  );
};
