import React from "react";

import { Container, Nav, Navbar, OverlayTrigger } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { SettingsIcon } from "../../assets/icons/SettingsIcon";
import { UserIcon } from "../../assets/icons/UserIcon";
import { Text } from "../../components/text/Text";
import { selectUser } from "../../data/redux/app/appSlice";
import { useColors } from "../../styles/variables-styles";
// import {
//   CalculatorWidget,
//   openCalculatorDialog,
// } from "../../features/calculator/Calculator";
import {
  openSettingsDialog,
  SettingsDialogWidget,
} from "../settings/SettingsWidget";
import "./header-style.scss";
import { OpenProfileDialog, ProfileDialogWidget } from "./ProfileDialogWidget";

import { endroutes } from "../../constant/endroutes";

import { useBreakpoints } from "../../hook/useBreakPoint";
import { HeaderLinkCard } from "./HeaderLinkCard";

export const HeaderWidget = () => {
  const colors = useColors();
  const user = useSelector(selectUser);

  const navigate = useNavigate();
  const location = useLocation();

  const { isPhone } = useBreakpoints();

  return (
    <div id="header-style">
      <ProfileDialogWidget />
      <SettingsDialogWidget />
      {/* <CalculatorWidget /> */}
      <Container fluid>
        <div className="header-container">
          <Navbar.Brand className="logo">Boss Accounting</Navbar.Brand>

          <div className="d-flex">
            {/* <Nav
              onClick={() => openCalculatorDialog(location, navigate)}
              className="user pointer"
              style={{ padding: "0 10px" }}
            >
              <img src="assets/images/calculator.png" width={24} height={24} />
            </Nav> */}

            <Nav
              onClick={() => openSettingsDialog(location, navigate)}
              className="user pointer"
              style={{ padding: "0 10px" }}
            >
              <SettingsIcon />
            </Nav>
          </div>
        </div>
      </Container>
    </div>
  );
};
