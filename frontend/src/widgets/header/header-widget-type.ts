export type HeaderLinkCardType = {
    iconPath: string;
    href: string;
    title: string;
    onClick?: any;
}