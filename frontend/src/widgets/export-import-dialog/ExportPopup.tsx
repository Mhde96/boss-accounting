import React, { useEffect } from "react";
import { useFormik } from "formik/dist";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import { en } from "../../helper/languages/en";
import { Text } from "../../components/text/Text";
import { Input } from "../../components/input/Input";
import { Button } from "../../components/button/Button";
import * as yup from "yup";
import { Modal } from "react-bootstrap";
import { exportDatabase } from "../../data/indexed-db/importData";
import { useAppSelector } from "../../data/redux/hooks";
import { SelectCompany } from "../../data/redux/data/dataSlice";
import { timeFormat } from "../../utils/timeFormat";
import { dateFormatUi } from "../../utils/date-format";
import { useTranslation } from "react-i18next";

interface ExportPopupType {
  show: boolean;
  onHide: () => void;
}
export const ExportPopup = ({ show, onHide }: ExportPopupType) => {
  const { t } = useTranslation();
  const company = useAppSelector(SelectCompany);

  const validationSchema = yup.object().shape({
    filename: yup.string().required(t(en.name_is_required)),
  });

  const date = new Date();
  const BackupName = `boss-accounting-${company?.name}-${dateFormatUi({
    date,
    formatType: "-",
  })}-${timeFormat(date)}`;

  const { values, handleSubmit, handleChange, setValues } = useFormik({
    initialValues: {
      filename: BackupName,
    },
    validationSchema,
    onSubmit: (values) => {
      exportDatabase(values);
    },
  });

  return (
    <ModalWrap show={show} onHide={onHide}>
      <Modal.Header>
        <Modal.Title>{t(en.backup)}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="d-flex  gap-2 mt-4 align-items-center">
          <div className="w-25">
            <Text>{t(en.filename)}</Text>
          </div>
          <Input
            onChange={handleChange("filename")}
            placeholder="Enter Name"
            value={values.filename}
          />
        </div>
      </Modal.Body>
      <Modal.Footer style={{ marginTop: 10 }}>
        <Button onClick={onHide}>{t(en.cancel)}</Button>
        <Button onClick={handleSubmit}>{t(en.export)}</Button>
      </Modal.Footer>
    </ModalWrap>
  );
};
