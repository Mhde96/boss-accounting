import React, { useRef } from "react";
import { useFormik } from "formik/dist";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import { en } from "../../helper/languages/en";
import { Text } from "../../components/text/Text";
import { Button } from "../../components/button/Button";
import * as yup from "yup";
import { Modal } from "react-bootstrap";
import { importDatabase } from "../../data/indexed-db/importData";
import { useTranslation } from "react-i18next";
import { Check } from "../../components/check/Check";
import { indexedDbName } from "../../data/indexed-db/indexedDb";

interface ImportPopupType {
  show: boolean;
  onHide: () => void;
}

interface initialValues {
  selectedFile: File;
  terms: boolean;
}

const initialValues: initialValues = {
  selectedFile: null,
  terms: false,
};

export const ImportPopup = ({ show, onHide }: ImportPopupType) => {
  const { t } = useTranslation();

  const fileInputRef = useRef<HTMLInputElement>(null);
  const handleSelectFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, selectedFile: event.target.files[0] });
  };

  const validationSchema = yup.object().shape({
    selectedFile: yup.mixed().nullable().required(t(en.field_is_required)),
    terms: yup.boolean().oneOf([true], t(en.field_is_required)),
  });

  const { values, handleSubmit, setValues, errors, setErrors } = useFormik({
    initialValues,
    validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    validateOnMount: false,
    onSubmit: (values) => {
      var reader = new FileReader();

      reader.onload = function (event) {
        //@ts-ignore
        var jsonObj = JSON.parse(event.target.result);

        const condation1 = jsonObj?.data?.databaseName == indexedDbName;
        const condation2 = jsonObj?.formatName == "dexie";
        if (condation1 && condation2) {
          importDatabase(values.selectedFile);
        } else {
          setErrors({
            ...errors,
            selectedFile:
              "The imported file is not compatible with the system.",
          });
        }
      };

      reader.readAsText(values.selectedFile);
    },
  });

  return (
    <ModalWrap show={show} onHide={onHide}>
      <Modal.Header>
        <Modal.Title>{t(en.backup)}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="d-flex  gap-2 mb-2  align-items-center">
          <div className="w-25">
            <Text>{t(en.chosefile)}</Text>
          </div>

          <input
            accept=".json"
            ref={fileInputRef}
            type="file"
            id="flupld"
            hidden
            onChange={handleSelectFile}
          />

          <Button
            onClick={() => fileInputRef.current.click()}
            variant={errors.selectedFile ? "danger" : "primary"}
          >
            {values.selectedFile?.name
              ? values.selectedFile?.name
              : "select file"}
          </Button>
        </div>

        <div className="error">
          {/* @ts-ignore */}
          {errors.selectedFile ? errors.selectedFile : null}
        </div>

        <div>
          <Check
            label={t(en.replace_terms)}
            defaultChecked={values.terms}
            onChange={() => setValues({ ...values, terms: !values.terms })}
          />
          <div className="error">{errors.terms ? errors.terms : null}</div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHide}>{t(en.cancel)}</Button>
        <Button onClick={handleSubmit}>{t(en.backup)}</Button>
      </Modal.Footer>
    </ModalWrap>
  );
};
