import React, { useEffect, useState } from "react";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import { Button } from "../../components/button/Button";
import { Text } from "../../components/text/Text";
import { ReactSVG } from "react-svg";
import { Check } from "../../components/check/Check";

import "./large-screen-notification.scss";
import { useTranslation } from "react-i18next/";
import { en } from "../../helper/languages/en";
import { Cookies } from "react-cookie";
import { cookiesKey } from "../../data/cookies/cookiesKey";
import { useBreakpoints } from "../../hook/useBreakPoint";

const cookies = new Cookies();

const LargeScreenNotification = () => {
  const { t } = useTranslation();
  const [check, setCheck] = useState(false);
  const [show, setShow] = useState(false);
  const { isPhone } = useBreakpoints();

  const handleClose = () => {
    if (check) cookies.set(cookiesKey.largeScreenNotfication, true);
    else cookies.set(cookiesKey.largeScreenNotfication, false);

    setShow(false);
  };

  useEffect(() => {
    if (isPhone) {
      const isCheckd = cookies.get(cookiesKey.largeScreenNotfication);
      console.log(isCheckd)
      if (isCheckd != true) {
        setShow(true);
      }
    }
  }, [isPhone]);

  return (
    <ModalWrap show={show} onHide={handleClose}>
      <div
        id="large-screen-notification"
        className="d-flex flex-column align-items-center p-4 gap-2"
      >
        <ReactSVG className="img" src="/assets/icons/desktop_screen.svg" />

        <Text center>{t(en.title_large_screen_notification)}</Text>

        <div className="w-100 d-flex gap-4 ">
          <Check
            onChange={() => setCheck(!check)}
            defaultChecked={check}
            label={t(en.do_not_show_again)}
          />
        </div>

        <Button onClick={handleClose}>{t(en.cancel)}</Button>
      </div>
    </ModalWrap>
  );
};

export { LargeScreenNotification };
