import React, { useLayoutEffect, useRef } from "react";
import { createPortal } from "react-dom";
import "./ContextMenu.scss";
import { useColorMemo } from "../../hook/useColorMemo";
export const ContextMenu = ({ contextMenuProps, setContextMenuProps }: any) => {
  const menuRef = useRef<HTMLDivElement | null>(null);
  const colorMode = useColorMemo();
  const isContextMenuOpen = contextMenuProps !== null;

  useLayoutEffect(() => {
    if (!isContextMenuOpen) return;

    function onClick(event: MouseEvent) {
      if (
        event.target instanceof Node &&
        menuRef.current?.contains(event.target)
      ) {
        setContextMenuProps(null);
        return;
      }
      setContextMenuProps(null);
    }

    addEventListener("click", onClick);

    return () => {
      removeEventListener("click", onClick);
    };
  }, [isContextMenuOpen]);

  return (
    <>
      {isContextMenuOpen && (
        <div
          id="ContextMenu"
          ref={menuRef}
          style={
            {
              top: contextMenuProps.top,
              left: contextMenuProps.left,
            } as unknown as React.CSSProperties
          }
        >
          {contextMenuProps.buttons.map((element: any, index: number) => (
            <div key={index} className="menu-item" onClick={element.onClick}>
              {element.title}
            </div>
          ))}
        </div>
      )}
    </>
  );
};
