import React, { useEffect, useMemo, useState } from "react";
import { SelectCompany } from "../../data/redux/data/dataSlice";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import "./company-title.scss";
import { Text } from "../../components/text/Text";
import { EditIcon } from "../../assets/icons/EditIcon";
import { useColors } from "../../styles/variables-styles";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import { Modal } from "react-bootstrap";
import { Input } from "../../components/input/Input";
import * as yup from "yup";

import { selectColorMode, selectLanguage } from "../../data/redux/app/appSlice";
import { en } from "../../helper/languages/en";
import { useTranslation } from "react-i18next";
import { useFormik } from "formik";
import { Button } from "../../components/button/Button";
import { saveCompanyAsync } from "../../data/redux/data/dataAsync";
export const CompanyTitle = () => {
  const company = useAppSelector(SelectCompany);
  const colors = useColors();
  const [show, setShow] = useState(false);
  const handleCloseModal = () => setShow(false);
  const handleOpenModal = () => setShow(true);
  const language = useAppSelector(selectLanguage);
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  const colorMode = useAppSelector(selectColorMode);
  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        name: yup.string().required(t(en.name_is_required)),
      }),
    [language]
  );

  const { values, setValues, handleChange, handleSubmit, errors } = useFormik({
    initialValues: {
      id: undefined,
      name: "",
    },
    validateOnChange: false,
    validationSchema,
    onSubmit: (values) => {
      dispatch(
        saveCompanyAsync({
          ...values,
          color_mode: colorMode,
          language: language,
        })
      );
      handleCloseModal();
    },
  });

  useEffect(() => {
    // @ts-ignore
    if (show == true) setValues({ ...company });
  }, [show]);

  return (
    <div id="company-title" className="card">
      <Text fs="f3">{company?.name}</Text>
      <EditIcon
        fill={colors.text}
        onClick={handleOpenModal}
        className="icons"
      />

      <ModalWrap show={show} onHide={handleCloseModal} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>{t(en.update_company_name)}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Text fs={"f3"}>{t(en.enter_your_company_name)}</Text>
          <Input
            key={"name"}
            onChange={handleChange("name")}
            value={values?.name}
            placeholder={t(en.name)}
            error={errors?.name}
          />
          <br/>
        </Modal.Body>

        <Modal.Footer>
          <div className="d-flex gap-2">
            <div className="w-100" />
            <div className="w-100" />
            <Button onClick={handleCloseModal}>{t(en.close)}</Button>
            <Button onClick={() => handleSubmit()}>{t(en.update)}</Button>
          </div>
        </Modal.Footer>
      </ModalWrap>
    </div>
  );
};
