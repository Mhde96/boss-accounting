import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Outlet } from "react-router-dom";
import "./layout-style.scss";

import { BalanceLottie } from "../../assets/lotties/BalanceLottie";
import { Text } from "../../components/text/Text";
import {
  ContactContainer,
  whatsappLink,
} from "../../containers/system/contact/ContactContainer";
import { useBreakpoints } from "../../hook/useBreakPoint";
import { useAppDispatch } from "../../data/redux/hooks";
import { useSelector } from "react-redux";
import appSlice, { selectLanguage } from "../../data/redux/app/appSlice";
import { en } from "../../helper/languages/en";
import { useTranslation } from "react-i18next";
import { ReactSVG } from "react-svg";

export const AuthLayout = () => {
  const dispatch = useAppDispatch();

  const { isPhone } = useBreakpoints();
  const language = useSelector(selectLanguage);
  const { t, i18n } = useTranslation();
  const handleLanguage = () => {
    let newLanguage = "ar";
    if (language == "ar") newLanguage = "en";
    else newLanguage = "ar";

    dispatch(appSlice.actions.language(newLanguage));
  };

  return (
    <div id="auth-layout-style" dir={i18n.dir()}>
    

      <Container className="auth-container" fluid={isPhone}>
        <div />
        <div className="d-flex flex-column">
          <BalanceLottie />
          <Outlet />
        </div>
        <div className="gap-2 d-flex mt-2 mb-2">
          <Text border>version 1.00</Text>
          <div
            className="d-flex gap-2 whatsapp-border"
            onClick={() => window.open(whatsappLink)}
          >
            <Text>Whatsapp</Text>
            <ReactSVG
              style={{
                fill: "white",
                minWidth: 16,
                maxWidth: 16,
              }}
              src="assets/icons/whatsapp.svg"
            />
          </div>
          <Text onClick={handleLanguage} border>
            {language} 🌍
          </Text>
        </div>
      </Container>
    </div>
  );
};
