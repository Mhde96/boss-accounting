import React from "react";
import { Container } from "react-bootstrap";
import { endroutes } from "../../constant/endroutes";
import { HeaderLinkCard } from "../header/HeaderLinkCard";
import { en } from "../../helper/languages/en";
import { useBreakpoints } from "../../hook/useBreakPoint";
import { useAppDispatch } from "../../data/redux/hooks";
import appSlice from "../../data/redux/app/appSlice";
import { useTranslation } from "react-i18next";
import { logoutAsync } from "../../data/redux/app/appAsync";

export const TopbarLayout = () => {
  const { isPhone } = useBreakpoints();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();


  return isPhone ? (
    <div className="header-link-card-style">
      <Container fluid>
        <HeaderLinkCard
          iconPath="assets/icons/home.svg"
          href={endroutes.home.path}
          title={endroutes.home.title}
        />
        <HeaderLinkCard
          href={endroutes.journals.path}
          title={endroutes.journals.title}
          iconPath="assets/icons/journal.svg"
        />
        <HeaderLinkCard
          href={endroutes.accounts.path}
          title={endroutes.accounts.title}
          iconPath="assets/icons/statement.svg"
        />

        <HeaderLinkCard
          href={endroutes.account_statment().null_path}
          title={endroutes.account_statment().title}
          iconPath="assets/icons/search.svg"
        />

        <HeaderLinkCard
          href={endroutes.trial_balance.path}
          title={endroutes.trial_balance.title}
          iconPath="assets/icons/trial.svg"
        />

        <HeaderLinkCard
          href={endroutes.trading_account.path}
          title={endroutes.trading_account.title}
          iconPath="assets/icons/trade.svg"
        />
        <HeaderLinkCard
          href={endroutes.profit_and_loss_account.path}
          title={endroutes.profit_and_loss_account.title}
          iconPath="assets/icons/profit.svg"
        />

        <HeaderLinkCard
          href={endroutes.balancesheet.path}
          title={endroutes.balancesheet.title}
          iconPath="assets/icons/balance.svg"
        />

        <HeaderLinkCard
          href={endroutes.contact.path}
          title={en.contact}
          iconPath="assets/icons/phone.svg"
        />
      
      </Container>
    </div>
  ) : null;
};
