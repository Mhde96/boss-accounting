import React from "react";

// import { Container, Stack } from "react-bootstrap";
import { useSelector } from "react-redux";

import { Outlet, useNavigate } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import { logoutAsync } from "../../data/redux/app/appAsync";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { HeaderWidget } from "../header/HeaderWidget";

import "./layout-style.scss";
import { useAnimationControls, motion } from "framer-motion";
import { useEffect, useMemo } from "react";
import { useLocation } from "react-router-dom";
import { useColors } from "../../styles/variables-styles";
import appSlice, {
  selectCurrentPathNameLayout,
  // selectUser,
} from "../../data/redux/app/appSlice";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";

// import { JoyrideHelper } from "../../features/joyride/JoyrideHelper";
// import { joyride_story } from "../../features/joyride/joyride_story";
import { ReactSVG } from "react-svg";
import { initializePlatformAsync } from "../../data/redux/data/dataAsync";
import { useIntervalFixCurrentMapId } from "../../hook/useIntervalFixCurrentMapId";
// import { useBreakpoints } from "../../hook/useBreakPoint";
// import { HeaderLinkCard } from "../header/HeaderLinkCard";
// import { Hr } from "../../components/Hr";
import { Sidebar } from "./Sidebar";
import { SelectCompanies } from "../../data/redux/data/dataSlice";
// import NavigationInitializer from "../../helper/navigationService";
// import { syncUserDb } from "../../db/mainDb";

export const PlatformLayout = () => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  const colors = useColors();
  const navigate = useNavigate();
  const currentPathNameLayout = useSelector(selectCurrentPathNameLayout);
  const location = useLocation();
  const companies = useAppSelector(SelectCompanies);

  useIntervalFixCurrentMapId();
  const handleLogout = () => {
    dispatch(
      appSlice.actions.openConfirmBox({
        title: t(en.logout),
        message: t(en.confirm_logout),
        handleSubmit: () => {
          dispatch(logoutAsync());
        },
      })
    );
  };

  useEffect(() => {
    dispatch(initializePlatformAsync());
  }, []);

  useEffect(() => {
    if (companies.loading == false) {
      if (companies.data.length == 0) navigate(endroutes.register);
    }
  }, [companies.loading]);

  const NavCard = ({ title, href, icon, onClick, className }: any) => {
    const active = useMemo(
      () => href == currentPathNameLayout,
      [currentPathNameLayout]
    );

    const controls = useAnimationControls();

    useEffect(() => {
      if (active)
        controls.start({
          background: colors.background,
          color: colors.onSurface,
          // fill: colors.onPrimary,
        });
      if (location?.pathname == endroutes.admin.path) {
        dispatch(
          appSlice.actions.changeCurrentPathNameLayout(endroutes.admin.path)
        );
      }
    }, [active]);

    const handleClick = () => {
      dispatch(appSlice.actions.changeCurrentPathNameLayout(href));
      if (onClick) {
        onClick();
      } else {
        navigate(href);
      }
    };

    return (
      <motion.div
        animate={controls}
        className={"nav-card " + className}
        onClick={handleClick}
      >
        <ReactSVG
          className="img"
          style={{ minWidth: 24, maxWidth: 24 }}
          src={icon}
        />
        <div className="content">{t(title)}</div>
      </motion.div>
    );
  };

  // register 1
  return (
    <div id="platform-layout-style">
      {/* <JoyrideHelper /> */}
      <HeaderWidget />

      <div className="platform-conainer">
        <Sidebar />
        <div className="outlet-container">
          {/* <TopbarLayout /> */}

          <Outlet />
        </div>
      </div>
    </div>
  );
};
