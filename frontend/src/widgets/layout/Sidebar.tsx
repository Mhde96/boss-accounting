import React, { useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import appSlice, {
  selectCurrentFinancialCycleId,
  selectCurrentPathNameLayout,
} from "../../data/redux/app/appSlice";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { endroutes } from "../../constant/endroutes";
import { Stack } from "react-bootstrap";
import { joyride_story } from "../../features/joyride/joyride_story";
import { useTranslation } from "react-i18next";
import { useBreakpoints } from "../../hook/useBreakPoint";

import { NavBarLink } from "../../components/nav-card/NavBarLink";
import { NavBar } from "../../components/nav-card/NavBar";
import { selectAccounts, selectJournals } from "../../data/redux/data/dataSlice";

export const Sidebar = () => {
  const dispatch = useAppDispatch();
  const currentFinancialCycleId = useAppSelector(selectCurrentFinancialCycleId);
  // const accounts = useAppSelector(selectAccounts)
  const journals = useAppSelector(selectJournals)

  const journalsLength = useMemo(()=> journals.length , [journals.length])

  const navigate = useNavigate();
  const currentPathNameLayout = useSelector(selectCurrentPathNameLayout);
  const { t } = useTranslation();
  const { isPhone } = useBreakpoints();

  const handleNavigate = (href: string) => {
    navigate(href);
    dispatch(appSlice.actions.changeCurrentPathNameLayout(href));
  };

  return !isPhone ? (
    <NavBar>
      <NavBarLink
        href={endroutes.home.path}
        text={t(endroutes.home.title)}
        icon="assets/icons/home.svg"
        handleNavigate={() => handleNavigate(endroutes.home.path)}
        active={currentPathNameLayout == endroutes.home.path}
      />
      <NavBarLink
        href={endroutes.accounts.path}
        text={t(endroutes.accounts.title)}
        icon="assets/icons/statement.svg"
        handleNavigate={() => handleNavigate(endroutes.accounts.path)}
        active={currentPathNameLayout == endroutes.accounts.path}
      />

      <NavBarLink
        href={endroutes.journals.path}
        text={t(endroutes.journals.title)}
        icon="assets/icons/journal.svg"
        handleNavigate={() => handleNavigate(endroutes.journals.path)}
        active={currentPathNameLayout == endroutes.journals.path}
        disabled={ currentFinancialCycleId == 0}
      />

      <Stack className={joyride_story.step3.className}>
        <NavBarLink
          href={endroutes.account_statment().null_path}
          text={t(endroutes.account_statment().title)}
          icon="assets/icons/search.svg"
          handleNavigate={() =>
            handleNavigate(endroutes.account_statment().null_path)
          }
          active={
            currentPathNameLayout == endroutes.account_statment().null_path
          }
        />

        <NavBarLink
          href={endroutes.trial_balance.path}
          text={t(endroutes.trial_balance.title)}
          icon="assets/icons/trial.svg"
          handleNavigate={() => handleNavigate(endroutes.trial_balance.path)}
          active={currentPathNameLayout == endroutes.trial_balance.path}
          disabled={journalsLength == 0}
        />

        <NavBarLink
          href={endroutes.trading_account.path}
          text={t(endroutes.trading_account.title)}
          icon="assets/icons/trade.svg"
          handleNavigate={() => handleNavigate(endroutes.trading_account.path)}
          active={currentPathNameLayout == endroutes.trading_account.path}
          disabled={journalsLength == 0}
        />

        <NavBarLink
          href={endroutes.profit_and_loss_account.path}
          text={t(endroutes.profit_and_loss_account.title)}
          icon="assets/icons/profit.svg"
          handleNavigate={() =>
            handleNavigate(endroutes.profit_and_loss_account.path)
          }
          active={
            currentPathNameLayout == endroutes.profit_and_loss_account.path
          }
          disabled={journalsLength == 0}
        />

        <NavBarLink
          href={endroutes.balancesheet.path}
          text={t(endroutes.balancesheet.title)}
          icon="assets/icons/balance.svg"
          handleNavigate={() => handleNavigate(endroutes.balancesheet.path)}
          active={currentPathNameLayout == endroutes.balancesheet.path}
          disabled={journalsLength == 0}
        />
        {/* <NavCard
          href={endroutes.income_statement.path}
          title={endroutes.income_statement.title}
          icon="assets/icons/incomestatement.svg"
        /> */}
      </Stack>

      <NavBarLink
        href={endroutes.releases.path}
        text={t(endroutes.releases.title)}
        icon="assets/icons/info.svg"
        handleNavigate={() => handleNavigate(endroutes.releases.path)}
        active={currentPathNameLayout == endroutes.releases.path}
      />
      {/* <NavCard
          href={endroutes.blog.path}
          title={"Blog"}
          icon="assets/icons/news.svg"
        /> */}

      <NavBarLink
        href={endroutes.contact.path}
        text={t(endroutes.contact.title)}
        icon="assets/icons/phone.svg"
        handleNavigate={() => handleNavigate(endroutes.contact.path)}
        active={currentPathNameLayout == endroutes.contact.path}
      />

      {/* <NavCard
        href={"logout"}
        title={t(en.logout)}
        onClick={handleLogout}
        icon="assets/icons/logout.svg"
      /> */}
    </NavBar>
  ) : null;
};
