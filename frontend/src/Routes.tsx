import React, { useEffect } from "react";
import { HashRouter as BrowserRouter, Routes, Route } from "react-router-dom";
import { endroutes } from "./constant/endroutes";
import { AccountStatmentContainer } from "./containers/account-statment/AccountStatementContainer";
import { AccountsContainer } from "./containers/accounts/AccountsContainer";
import { BalanceSheetContainer } from "./containers/financial-statement/BalanceSheetContainer";

import { ProfitAndLossAccountContainer } from "./containers/financial-statement/ProfitAndLossAccountContainer";
import { TradingAccountContainer } from "./containers/financial-statement/TradingAccountContainer";
import { TrialBalanceContainer } from "./containers/financial-statement/TrialBalanceContainer";
import { EntriesContainer } from "./containers/entry/EntriesContainer";
import { JournalsContainer } from "./containers/journals/JournalsContainer";
import { AboutContainer } from "./containers/system/about/AboutContainer";

import { PlatformLayout } from "./widgets/layout/PlatformLayout";
import { HomeContainer } from "./containers/home/HomeContainer";
import { BlogContainer } from "./containers/system/blog/BlogContainer";
import { ContactContainer } from "./containers/system/contact/ContactContainer";
import NavigationInitializer from "./helper/navigationService";
import { useAppDispatch, useAppSelector } from "./data/redux/hooks";
import { CreateNewCompanyWidget } from "./widgets/create_new_company/CreateNewCompanyWidget";
import { fetchCompaniesAsync } from "./data/redux/data/dataAsync";
import { Middleware } from "./middleware/Middleware";

export const Navigation = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchCompaniesAsync());
  }, []);

  return (
    <BrowserRouter>
      <NavigationInitializer />
      <Middleware />
      <Routes>
        <Route path="/" element={<PlatformLayout />}>
          <Route index element={<HomeContainer />} />
          <Route
            path={endroutes.accounts.path}
            element={<AccountsContainer />}
          />
          <Route
            path={endroutes.journalentaries().path}
            element={<EntriesContainer />}
          />
          <Route
            path={endroutes.journals.path}
            element={<JournalsContainer />}
          />

          <Route
            path={endroutes.account_statment().null_path}
            element={<AccountStatmentContainer />}
          />
          <Route
            path={endroutes.account_statment().path}
            element={<AccountStatmentContainer />}
          />

          <Route
            path={endroutes.trial_balance.path}
            element={<TrialBalanceContainer />}
          />
          <Route
            path={endroutes.profit_and_loss_account.path}
            element={<ProfitAndLossAccountContainer />}
          />
          <Route
            path={endroutes.trading_account.path}
            element={<TradingAccountContainer />}
          />
          <Route
            path={endroutes.balancesheet.path}
            element={<BalanceSheetContainer />}
          />

          <Route path={endroutes.releases.path} element={<AboutContainer />} />
          <Route path={endroutes.contact.path} element={<ContactContainer />} />
        </Route>

        <Route>
          <Route
            path={endroutes.register}
            element={<CreateNewCompanyWidget />}
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

// {/* <Route
//           path={endroutes.income_statement.path}
//           element={<IncomeStatementContainer />}
//         /> */}
