import { endpoints } from "../../../constant/endpoints";
import { journalType } from "../../../containers/journals/journal-type";
import { api } from "../../../helper/api";
import { AppDispatch, store } from "../store";
import { dataSlice } from "./dataSlice";
import { MapType } from "../../../types/MapType";
import { refreshTokenAsync } from "../app/appAsync";
import { getDbType } from "../../cookies/useGetCookies";
import { databaseSource } from "../../../constant/databaseSource";
import { deleteAccountDb, fetchAccountDb, saveAccountDb } from "../../indexed-db/accountsDb";
import { deleteJournalDb, fetchJournalsDb, saveJournalDb } from "../../indexed-db/journalsDb";
import { fetchCompaniesDb, saveCompanyDb } from "../../indexed-db/companiesDb";
import { deleteFinancialCycleDb, fetchFinancialCyclesDb, saveFinancialCycleDb } from "../../indexed-db/financial-cyclesDb";
import { AccountType } from "../../../types/accountType";
import { accountType } from "../../../containers/accounts/account-type";
import { JournalType } from "../../../types/JournalType";
import { JournalApi, removeEmptyRow } from "../../../containers/entry/entries-functions";
import { navigateTo } from "../../../helper/navigationService";
import { dbTableKeys } from "../../indexed-db/dbKeys";
import { CompanyType } from "../../../types/CompanyType";
import { FinancialCycleType } from "../../../types/FinancialCycleType";

const SaveAccountAsync =
  (account: AccountType) =>
    async (dispatch: AppDispatch) => {

      const { dbType } = getDbType();

      if (dbType === databaseSource.server) {
        const method = account.id ? "post" : "post"; // assuming "put" for update, correct if different
        const url = account.id ? endpoints.update_account : endpoints.add_account;
        const data = account;

        try {
          await api({ method, url, data });
        } catch (error) {
          console.error("Failed to save account to server", error);
        }
      } else {
        try {
          await saveAccountDb(account);
        } catch (error) {
          console.error("Failed to save account to local DB", error);
        }
      }


      dispatch(fetchAccountsAsync());
      navigateTo(-1)
    };

const deleteAccountAsync =
  (account: AccountType) => async (dispatch: AppDispatch) => {
    const { dbType } = getDbType();
    try {
      if (dbType === databaseSource.server) {
        await api.post(endpoints.delete_account, account);
      }
      else {
        await deleteAccountDb(account)
      }
      dispatch(fetchAccountsAsync());
    } catch (error) {
      console.error("Failed to delete account", error);
    }
  };

const fetchAccountsAsync = () => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    try {
      const response = await api.post(endpoints.fetch_accounts);
      if (response.data.success) {
        dispatch(dataSlice.actions.fetchAccounts(response.data.data));
      }
    } catch (error) {
      console.error("Failed to fetch accounts from server", error);
    }
  } else {
    try {
      const accounts = await fetchAccountDb();
      dispatch(dataSlice.actions.fetchAccounts(accounts));
    } catch (error) {
      console.error("Failed to fetch accounts from local DB", error);
    }
  }
};

// ==========================================================================
// JOURNALS
// ==========================================================================


const fetchJournalsAsync = () => async (dispatch: AppDispatch) => {

  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    try {
      const response = await api.post(endpoints.fetch_journals);
      dispatch(dataSlice.actions.fetchJournals(response.data.data));
    } catch (error) {
      console.error("Failed to fetch journals from server", error);
    }
  } else {
    try {
      const journals = await fetchJournalsDb();
      dispatch(dataSlice.actions.fetchJournals(journals));
    } catch (error) {
      console.error("Failed to fetch journals from local DB", error);
    }
  }
};

const saveJournalAsync = (journal: JournalType,) => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType()


  if (dbType == databaseSource.server) {
    try {
      await JournalApi(journal)
    } catch (error) {
      console.error("Failed to save journal to server", error);
    }
  }
  else if (dbType == databaseSource.local) {
    try {
      const currentFinancialCycleId = store.getState().appReducer.currentFinancialCycleId

      let entries: Array<any> = await removeEmptyRow(journal.entries);
      let newJournal = { ...journal, entries, [dbTableKeys.journals.financialcycle_id]: currentFinancialCycleId };
      saveJournalDb(newJournal)
    } catch (error) {
      console.error("Failed to save journal to local DB", error);
    }
  }
}


const deleteJournalAsync =
  (journal: journalType) => async (dispatch: AppDispatch) => {
    try {


      const { dbType } = getDbType()


      if (dbType == databaseSource.server) {
        await api.post(endpoints.delete_journal, journal).then((response) => {

        });
      } else {
        // @ts-ignore
        await deleteJournalDb(journal)
      }
      dispatch(fetchJournalsAsync());
    } catch (error) {
      console.error("Failed to delete Journal ", error)
    }
  };

// ==========================================================================
// MAPS
// ==========================================================================

const fetchMapsAsync = () => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    try {
      const response = await api.post(endpoints.fetch_maps);
      dispatch(dataSlice.actions.fetchMaps(response.data.data));
    } catch (error) {
      console.error("Failed to fetch maps from server", error);
    }
  } else {
    try {
      const companies = await fetchCompaniesDb();
      dispatch(dataSlice.actions.fetchMaps(companies));
    } catch (error) {
      console.error("Failed to fetch maps from local DB", error);
    }
  }
};

const addMapsAsync = (map: MapType) => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    try {
      await api.post(endpoints.create_map, map);
      dispatch(fetchMapsAsync());
    } catch (error) {
      console.error("Failed to add map to server", error);
    }
  } else {
    try {
      // @ts-ignore
      await saveCompanyDb(map);
    } catch (error) {
      console.error("Failed to add map to local DB", error);
    }
  }
};


const fetchCompaniesAsync = () => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    return
  } else {
    try {
      const companies = await fetchCompaniesDb();
      dispatch(dataSlice.actions.fetchCompanies(companies));
    } catch (error) {
      console.error("Failed to fetch maps from local DB", error);
    }
  }
};




const saveCompanyAsync = (company: CompanyType) => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();
  if (dbType === databaseSource.server) {
    return
  } else {
    try {
      const newCompany: CompanyType = {
        color_mode: company.color_mode,
        language: company.language,
        name: company.name
      }
      await saveCompanyDb(newCompany);
      dispatch(fetchCompaniesAsync())

    } catch (error) {
      console.error("Failed to add map to local DB", error);
    }
  }
};




const updateMapsAsync = (map: MapType) => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType();

  if (dbType === databaseSource.server) {
    try {
      await api.post(endpoints.update_map, map);
      dispatch(fetchMapsAsync());
    } catch (error) {
      console.error("Failed to update map on server", error);
    }
  } else {
    try {
      // @ts-ignore
      await saveCompanyDb(map);
    } catch (error) {
      console.error("Failed to update map in local DB", error);
    }
  }
};

const deleteMapsAsync = (map: MapType) => async (dispatch: AppDispatch) => {
  try {
    const response = await api.post(endpoints.delete_map, map);
    dispatch(fetchMapsAsync());
    dispatch(changeCurrentMapAsync(response.data.data));
  } catch (error) {
    console.error("Failed to delete map", error);
  }
};

const initializePlatformAsync = () => async (dispatch: AppDispatch) => {
  // dispatch(fetchMapsAsync());
  dispatch(fetchAccountsAsync());
  dispatch(fetchJournalsAsync());
};

const changeCurrentMapAsync =
  (map: MapType) => async (dispatch: AppDispatch) => {
    try {
      await api.post(endpoints.change_current_map, map);
      dispatch(refreshTokenAsync());
      dispatch(initializePlatformAsync());
    } catch (error) {
      console.error("Failed to change current map", error);
    }
  };

const fetchFinicialAsync = () => async (dispatch: AppDispatch) => {
  const { dbType } = getDbType()

  try {

    if (dbType == databaseSource.server) {

      api.post(endpoints.fetch_finincialCycles).then((response) => {
        dispatch(fetchMapsAsync())
        dispatch(dataSlice.actions.fetchFinicialCycles(response.data.data));
      });
    } else {
      // dispatch(fetchMapsAsync())

      const financialcycles = await fetchFinancialCyclesDb()

      dispatch(dataSlice.actions.fetchFinicialCycles(financialcycles));

    }

  } catch (error) {
    console.error("Failed to fetch financial cycles ", error);
  }

};

const addFinicialAsync =
  (body: { map_id: number; start_at: string }) => async (dispatch: AppDispatch) => {
    const { dbType } = getDbType()

    const newFinicialCycle: FinancialCycleType = {
      start_at: body.start_at
    }

    try {
      const data = body;
      if (dbType == databaseSource.server) {
        api.post(endpoints.add_finincialCycles, data).then((response) => {
          dispatch(fetchFinicialAsync());
        });
      } else {
        // @ts-ignore
        await saveFinancialCycleDb(newFinicialCycle)
        await dispatch(fetchFinicialAsync())
      }
    } catch (error) {
      console.error("Failed to add financial cycle ", error);
    }

  };

const deleteFinacialCycles = (data: FinancialCycleType) => (dispatch: AppDispatch) => {

  try {
    const { dbType } = getDbType()
    if (dbType == databaseSource.server) {
      api.post(endpoints.delete_finacialcycle, data).then(() => {
        dispatch(fetchFinicialAsync());
      });
    } else {
      deleteFinancialCycleDb(data.id)
        .then(() => {
          dispatch(fetchFinicialAsync());

        })
    }

  } catch (error) {
    console.error("Failed to delete financial cycle", error);
  }
};




export {
  SaveAccountAsync,
  deleteAccountAsync,
  fetchAccountsAsync,

  fetchJournalsAsync,
  saveJournalAsync,
  deleteJournalAsync,


  addMapsAsync,
  updateMapsAsync,
  deleteMapsAsync,

  initializePlatformAsync,
  changeCurrentMapAsync,
  fetchFinicialAsync,

  addFinicialAsync,
  deleteFinacialCycles,

  saveCompanyAsync,
  fetchCompaniesAsync,
}