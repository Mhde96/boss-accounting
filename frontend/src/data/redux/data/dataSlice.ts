import { createSlice } from "@reduxjs/toolkit";
import { journalType } from "../../../containers/journals/journal-type";
import { RootState } from "../store";
import { CompaniesType, CompanyType } from "../../../types/CompanyType";
import { fetchAccountDb } from "../../../data/indexed-db/accountsDb";


const initialState = {
  // @ts-ignore
  accounts: [],
  journals: {
    // @ts-ignore
    data: [],
    loading: true,
    
  },
  // @ts-ignore
  companies: {
    // @ts-ignore
    data: [],
    loading: true
  },
  // @ts-ignore
  maps: [],
  // @ts-ignore
  finicialCycles: {
    // @ts-ignore
    data: [],
    loading: true
  },
  searchAccount: {
    // @ts-ignore
    list: [],
    open: false,
    // @ts-ignore
    rowIndex: undefined,
  },
};

export const dataSlice = createSlice({
  name: "dataReducer",
  initialState,
  reducers: {
    fetchAccounts: (state, { payload }) => {
      state.accounts = payload;
    },
    fetchJournals: (state, { payload }) => {
      state.journals.data = payload;
      state.journals.loading = false;
    },
    fetchMaps: (state, { payload }) => {
      state.maps = payload;
    },
    fetchFinicialCycles: (state, { payload }) => {
      state.finicialCycles.data = payload;
      state.finicialCycles.loading = false
    },
    fetchCompanies: (state, { payload }) => {
      state.companies.data = payload;
      state.companies.loading = false

    },
    // changeClosingEntries: (state) => {
    //   state.closingEntries = !state.closingEntries
    // }
    // searchAccount: (state, { payload }) => {
    //   const { text, reset, open } = payload;
    //   if (reset == true) {
    //     state.search_account.list = [];
    //     state.searchAccount.open = false;
    //     state.searchAccount.rowIndex = undefined;
    //   } else {
    //     state.searchAccount.open = true;
    //     state.searchAccount.list = state.accounts?.filter((account) =>
    //       account.name.toLowerCase().match(text.toLowerCase())
    //     );
    //   }
    // },
  },
});

export const selectAccounts = (state: RootState): Array<any> =>
  state.dataReducer.accounts;
export const selectJournals = (state: RootState): Array<journalType> =>
  state.dataReducer.journals.data;
export const selectLoaderJournals = (state: RootState): boolean =>
  state.dataReducer.journals.loading;

export const SelectSearchAccount = (state: RootState) =>
  state.dataReducer.searchAccount;

export const SelectMaps = (state: RootState) => state.dataReducer.maps;
export const SelectCompanies = (state: RootState): CompaniesType =>
  state.dataReducer.companies
export const SelectCompany = (state: RootState): CompanyType => state.dataReducer.companies.data[0]

export const SelectFinacialCycles = (state: RootState) => {
  return state.dataReducer.finicialCycles;
};

export const SelectCurrentFinacialCycles = (state: RootState) => {
  const currentFinancialCycleId = state.appReducer.currentFinancialCycleId
  const currentFinancialCycle = state.dataReducer.finicialCycles.data.filter(value => value.id == currentFinancialCycleId)
  // let currentFinancialCycle = state.dataReducer.finicialCycles.filter(value => value.map_id == currentMapId)

  return currentFinancialCycle;
};

// export const SelectclosingEntries = (state: RootState) => state.dataReducer.closingEntries
export default dataSlice;
