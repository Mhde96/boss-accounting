import { createSlice } from "@reduxjs/toolkit";
import { AppStateType, colorModeType, dbType, StatusType } from "./app-type";
import { RootState } from "../store";
import { Cookies } from "react-cookie";
import { cookiesKey } from "../../cookies/cookiesKey";
import i18n from "../../../helper/i18n";
import { userType } from "../../../types/userType";
import { databaseSource } from "../../../constant/databaseSource";
// @ts-ignore
import Appearance from "react-windows-ui/dist/api/Appearance";
import { isElectron } from "../../../utils/isElectron";
import { getLanguage,getAppDirection } from "../../../utils/language-utils";

const cookies = new Cookies();




const setWindowsColorSchema = (colorMode: colorModeType) => {
  if (isElectron) {
    if (colorMode == 'dark') Appearance.setDarkScheme();
    else if (colorMode == 'light') Appearance.setLightScheme();
    else Appearance.setSystemScheme();
  }
}

const getColorMode = () => {
  let colorMode = cookies.get(cookiesKey.colorMode);

  setWindowsColorSchema(colorMode)
  if (!colorMode) return "auto";
  else return colorMode;
};

const getDbType = () => {
  let dbType = cookies.get(cookiesKey.dbType);
  if (!dbType) return databaseSource.local;

  return dbType
}

const getCookiesMapId = () => {
  let value = cookies.get(cookiesKey.mapId);
  if (value != undefined) {
    value = parseInt(value);
    return value;
  }
};

const getCookiesFinancialCycle = () => {
  let value = cookies.get(cookiesKey.currentFinancialCycleId);
  if (value != undefined) {
    value = parseInt(value);
    return value;
  }
  else {
    // alert("currentFinancialCycleId not define ")
    return 0
  }
};

const initialState: AppStateType = {
  user: cookies.get(cookiesKey.user),
  status: false,
  colorMode: getColorMode(),
  language: getLanguage(),
  appDirection: getAppDirection(),
  db: {
    id: undefined,
    name: "",
  },
  confirm: {
    show: false,
    title: "",
    message: "",
    handleSubmit: undefined,
  },
  currentPathNameLayout: "/",
  currentFinancialCycleId: getCookiesFinancialCycle(),
  map_id: getCookiesMapId(),
  dbType: getDbType()
};

export const appSlice = createSlice({
  name: "appReducer",
  initialState,
  reducers: {
    login: (state, { payload }) => {
      state.user = { ...state.user, ...payload };
      let temp = { ...payload };
      delete payload.maps;
      delete payload.currentMap;

      cookies.set(cookiesKey.user, { ...payload });
    },
    logout: (state) => {
      cookies.remove(cookiesKey.user);
      state.user = { email: "", name: "", id: undefined, role: undefined };
    },
    changeStatus: (state, { payload }) => {
      state.status = payload;
    },
    openConfirmBox: (state, { payload }) => {
      state.confirm = {
        show: true,
        title: payload.title,
        message: payload.message,
        handleSubmit: payload.handleSubmit,
      };
    },
    closeConfirmBox: (state, { payload }) => {
      state.confirm = { ...initialState.confirm };
    },
    colorMode: (state, { payload }) => {
      cookies.set(cookiesKey.colorMode, payload);
      setWindowsColorSchema(payload)
      state.colorMode = payload;
    },

    language: (state, { payload }) => {
      cookies.set(cookiesKey.language, payload);
      i18n.changeLanguage(payload);
      state.appDirection = getAppDirection()
      state.language = payload;
    },
    changeDb: (state, { payload }) => {
      // cookies.set(cookiesKey.dbId, payload.id);
      state.db = payload;
    },

    changeFinancialCycleId: (state, { payload }) => {
      cookies.set(cookiesKey.currentFinancialCycleId, payload);
      state.currentFinancialCycleId = payload;
    },
    changeMapId: (state, { payload }) => {
      cookies.set(cookiesKey.mapId, payload);
      state.map_id = payload;
    },
    changeCurrentPathNameLayout: (state, { payload }) => {
      state.currentPathNameLayout = payload;
    },

    changeCurrentDbType: (state, { payload }) => {
      cookies.set(cookiesKey.dbType, payload);
      state.dbType = payload
    }
  },
});

export const selectCurrentPathNameLayout = (state: RootState): string =>
  state.appReducer.currentPathNameLayout;
export const selectUser = (state: RootState): userType => state.appReducer.user;
export const selectStatus = (state: RootState): StatusType =>
  state.appReducer.status;
export const selectColorMode = (state: RootState): string =>
  state.appReducer.colorMode;
export const selectLanguage = (state: RootState): string =>
  state.appReducer.language;
export const selectAppDirection = (state: RootState): string => state.appReducer.appDirection
export const selectDb = (state: RootState): dbType => state.appReducer.db;
export const selectCurrentFinancialCycleId = (
  state: RootState
): number | undefined => state.appReducer.currentFinancialCycleId;
export const selectMapId = (state: RootState): number =>
  state.appReducer.map_id;

export const selectDbType = (state: RootState): string => state.appReducer.dbType

export default appSlice;
