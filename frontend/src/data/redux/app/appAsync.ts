import { endpoints } from "../../../constant/endpoints";
import { api } from "../../../helper/api";
import { dbType, StatusType } from "./app-type";
import appSlice from "./appSlice";
import { AppDispatch } from "../store";

import { endroutes } from "../../../constant/endroutes";
import { NavigateFunction } from "react-router-dom";
import { Cookies } from "react-cookie";
import { cookiesKey } from "../../cookies/cookiesKey";
// import { selectSpecificDataIndexedDb } from "../../db/data/dataDb";
import { userType } from "../../../types/userType";
import { initializePlatformAsync } from "../data/dataAsync";
import { MapType } from "../../../types/MapType";

export const registerAsync =
// @ts-ignore
  (values: userType, navigate: NavigateFunction, setLoading) =>
    async (dispatch: AppDispatch) => {
      dispatch(changeStatusSync(true));

      setLoading(true)
      api
        .post(endpoints.register, values)
        .then((response) => {
          if (response.data.success) {
            dispatch(appSlice.actions.login(response.data.data));
            navigate(endroutes.thankyou);
          } else {
            alert(response.data.message);
          }
        })
        .catch((error) => {
          alert(error.response.data.message)
        })
        .finally(() => {
          dispatch(changeStatusSync(false));
          setLoading(false)
        });
    };

export const changeStatusSync =
  (status: StatusType) => (dispatch: AppDispatch) => {
    dispatch(appSlice.actions.changeStatus(status));
  };

export const loginAsync =
  (values: { email: string; password: string }, navigate: NavigateFunction) =>
    async (dispatch: AppDispatch) => {
      dispatch(changeStatusSync(true));
      api
        .post(endpoints.login, values)
        .then((response) => {
          console.log({ response })
          if (response.data.success) {
            dispatch(appSlice.actions.login(response.data.data));
            // try {
            //   const maps = response.data.data.maps;
            //   const lastMap = maps[maps.length - 1];
            //   console.log(lastMap);
            //   dispatch(changeCurrentMapId(lastMap));
            // } catch (error) {}
            navigate(endroutes.thankyou);
          } else {
            alert(response.data.message);
          }
        })
        .catch((error) => {
          alert(error.response.data.message)

        })
        .finally(() => {
          dispatch(changeStatusSync(false));
        });
    };

export const refreshTokenAsync = () => async (dispatch: AppDispatch) => {
  api
    .post(endpoints.refreshToken)
    .then((response) => {
      if (response.data.success) {
        dispatch(appSlice.actions.login(response.data.data));
      } else if (response.data.success == false) {
        dispatch(logoutAsync());
      }
    })
    .catch(() => {
      dispatch(logoutAsync());
    });
};

export const logoutAsync = () => async (dispatch: AppDispatch) => {
  dispatch(appSlice.actions.logout());
};

export const changeDbAsync =
  (values: dbType) => async (dispatch: AppDispatch) => {
    const cookies = new Cookies();
    cookies.set(cookiesKey.dbId, values?.id);
    dispatch(appSlice.actions.changeDb(values));
  };

export const changeCurrentMapId =
  (map: MapType) => async (dispatch: AppDispatch) => {
    dispatch(appSlice.actions.changeMapId(map.id));


    return 0
    // let lastFinancialCycle = () => {
    //   if (map.financialcycles.length > 0)
    //     return map.financialcycles[map.financialcycles.length - 1].id;
    //   else return 0;
    // };
    // dispatch(changeCurrentFinancialCycleIdAsync(lastFinancialCycle()));
  };

export const changeCurrentFinancialCycleIdAsync =
  (value: number) => async (dispatch: AppDispatch) => {
    dispatch(appSlice.actions.changeFinancialCycleId(value));
    dispatch(initializePlatformAsync());
  };
