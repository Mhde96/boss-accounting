import { userType } from "../../../types/userType";

export type colorModeType = "light" | "dark" | "auto";

export type AppStateType = {
  user: userType;
  status: StatusType;
  colorMode: colorModeType;
  language: "default" | "en" | "ar";
  appDirection: 'ltr' | 'rtl'
  db: dbType;
  confirm: {
    show: boolean;
    title: string;
    message: string;
    handleSubmit?: () => void;
  };
  currentPathNameLayout: string;
  currentFinancialCycleId: number | undefined;
  map_id: number;
  dbType: string;
};

// export type userType = {
//   id: number;
//   name: string;
//   email: string;
//   last_sync: string;
// };

export type StatusType = true | false | "data" | null;

export type dbType = {
  id: number | undefined;
  name: string;
};
