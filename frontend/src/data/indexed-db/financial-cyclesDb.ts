import { useLiveQuery } from "dexie-react-hooks"
import { FinancialCycleType } from "../../types/FinancialCycleType"
import { dbTableKeys } from "./dbKeys"
import { db } from "./indexedDb"
import { deleteJournalsByFinancialCycleId } from "./journalsDb"

const useFetchFinancialCyclesDb = (): Array<FinancialCycleType> => {
    const financialcycles: Array<FinancialCycleType> | undefined = useLiveQuery(() => db.table(dbTableKeys.financialcycles.table).toArray())

    if (financialcycles?.length)
        return financialcycles

    else return []
}



const fetchFinancialCyclesDb = async () => {
    const financialcycles = await db.table(dbTableKeys.financialcycles.table).toArray()

    return financialcycles
}



const saveFinancialCycleDb = async (financialCycle: FinancialCycleType) => {
    return await db.table(dbTableKeys.financialcycles.table).add(financialCycle)
}


export const deleteFinancialCycleDb = async (id: number) => {
    await deleteJournalsByFinancialCycleId(id)
    return await db.table(dbTableKeys.financialcycles.table).delete(id);
};

export { fetchFinancialCyclesDb, saveFinancialCycleDb, useFetchFinancialCyclesDb }