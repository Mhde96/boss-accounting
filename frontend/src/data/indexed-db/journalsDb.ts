import { endroutes } from "../../constant/endroutes";
import { navigateTo } from "../../helper/navigationService";
import { JournalType } from "../../types/JournalType";
import { store } from "../redux/store";
import { dbTableKeys } from "./dbKeys";
import { deleteEntriesByJournalIds, deleteEntriesDb } from "./entriesDb";
import { db } from "./indexedDb";


const fetchJournalsDb = async () => {
    const currentFinancialCycleId = store.getState().appReducer.currentFinancialCycleId

    let entries = await db.table(dbTableKeys.entries.table).toArray();
    let journals = await db.table(dbTableKeys.journals.table)
        .where(dbTableKeys.journals.financialcycle_id)
        .equals(currentFinancialCycleId)
        .toArray();

    if (journals?.length && entries?.length) {

        journals = journals.map(journal => {
            return {
                ...journal, entries: entries.filter(entry => entry.journalId === journal.id)
            }
        })



        return journals

    }
    return []

}



const saveJournalDb = async (journal: JournalType,) => {

    if (journal.id == undefined) {
        const journals = await db.table(dbTableKeys.journals.table)

            .toArray();

        const number = (journals.length > 0 ? journals[journals.length - 1].number : 0) + 1;

        const journalDocuemnt = {
            ...journal,
            number,
        }
        delete (journalDocuemnt.entries)

        const journalId = await db.table(dbTableKeys.journals.table).add({ ...journalDocuemnt });

        journal.entries = journal.entries?.map((entry) => ({
            ...entry,
            journalId: journalId,

        }))

        journal.entries?.map(async (entry: any) => {

            // delete entry.account;
            // delete entry.accountName;
            // delete entry.status;
            delete entry.id;

            try {
                // Add to the entries store
                await db.table(dbTableKeys.entries.table).add(entry);
            } catch (error) {
                console.log("Error adding entry:", entry, error);
            }
        })
        navigateTo(endroutes.journals.path);
    } else


        try {
            const journalTable = db.table(dbTableKeys.journals.table);
            const entriesTable = db.table(dbTableKeys.entries.table);

            // Fetch the existing journal by ID
            const existingJournal = await journalTable.get(journal.id);
            // Case: Journal exists, update date and description
            if (existingJournal) {
                await journalTable.update(journal.id, {
                    date: journal.date,
                    description: journal.description,
                });
            } else {
                throw new Error(`Journal with ID ${journal.id} does not exist.`);
            }

            // Synchronize entries
            const existingEntries = await entriesTable.where(dbTableKeys.entries.journal_id).equals(journal.id).toArray();


            const existingEntryIds = existingEntries.map(entry => entry.id);
            const incomingEntryIds = journal.entries?.map(entry => entry.id) || [];
            // Case: Add new entries (IDs not in existingEntries)
            const newEntries = journal.entries?.filter(entry => !existingEntryIds.includes(entry.id))
                .map(entry => {
                    delete entry.id;
                    return ({
                        ...entry,
                        [dbTableKeys.entries.journal_id]: journal.id, // Inject the journalId into each new entry
                    })
                }) || [];

            if (newEntries.length > 0) {
                await entriesTable.bulkAdd(newEntries);
            }

            // Case: Update existing entries
            const updatedEntries = journal.entries?.filter(entry => existingEntryIds.includes(entry.id)) || [];
            for (const entry of updatedEntries) {
                await entriesTable.update(entry.id, entry);
            }

            // Case: Delete missing entries (IDs in existingEntries but not in incomingEntryIds)
            const entriesToDelete = existingEntries.filter(entry => !incomingEntryIds.includes(entry.id));
            if (entriesToDelete.length > 0) {
                const idsToDelete = entriesToDelete.map(entry => entry.id);
                await entriesTable.bulkDelete(idsToDelete);
            }


            console.log('Journal and entries synchronized successfully.');
            navigateTo(endroutes.journals.path);
        } catch (error) {
            console.error('Error saving journal and synchronizing entries:', error);
        }
}

const deleteJournalDb = async (journal: JournalType) => {
    try {
        // Begin a Dexie transaction to ensure atomicity
        await db.transaction('rw', db.table(dbTableKeys.journals.table), db.table(dbTableKeys.entries.table), async () => {
            // Retrieve all entries associated with the journal
            const entries = await db.table(dbTableKeys.entries.table).where(dbTableKeys.entries.journal_id).equals(journal.id).toArray();

            // Extract IDs of the entries
            const entryIds = entries.map(entry => entry.id);

            // Bulk delete the entries
            if (entryIds.length > 0) {
                await deleteEntriesDb(entryIds);
            }

            // Delete the journal itself
            await db.table(dbTableKeys.journals.table).delete(journal.id);
            console.log(`Journal with ID ${journal.id} and its entries have been deleted.`);
        });
    } catch (error) {
        console.error('Failed to delete journal and its entries:', error);
    }
}

export const deleteJournalsByFinancialCycleId = async (financialcycle_id: number) => {
    const journalsToDelete = await db.table(dbTableKeys.journals.table)
        .where({ financialcycle_id }).toArray();

    const journalsIds = journalsToDelete.map(journal => journal.id)
    await deleteEntriesByJournalIds(journalsIds)
    const deletePromises = journalsToDelete
        .map(journal => db.table(dbTableKeys.journals.table).delete(journal.id));
    return Promise.all(deletePromises);
};


export {
    fetchJournalsDb,
    saveJournalDb,
    deleteJournalDb,
}