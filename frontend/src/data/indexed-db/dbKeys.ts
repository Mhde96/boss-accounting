export const dbKeys = {
    id: "id",
};

export const dbTableKeys = {
    id: "id",
    users: {
        table: "users",
        user_id: "user_id",
        last_sync: "last_sync",
    },
    data: {
        table: "data",
        id: "id", // main db
        user_id: "user_id",
    },
    journals: {
        table: "journals",
        map_id: 'map_id',
        financialcycle_id: 'financialcycle_id'

    },
    companies: {
        table: 'companies'
    },
    financialcycles: {
        table: "financialcycles"
    },
    accounts: {
        table: "accounts",
        map_id: 'map_id',

    },
    entries: {
        table: "entries",
        id:'id',
        journal_id: 'journalId',
        account_id:'accountId'
    },

};
