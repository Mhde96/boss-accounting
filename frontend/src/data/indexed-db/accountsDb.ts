import { en } from "../../helper/languages/en";
import { financialAccounts } from "../../constant/financialAccounts";
import { AccountType } from "../../types/accountType";
import { selectMapId } from "../redux/app/appSlice";
import { useAppSelector } from "../redux/hooks";
import { dbTableKeys } from "./dbKeys";
import { filterEntriesBy } from "./entriesDb";
import { db } from "./indexedDb";
import { useLiveQuery } from 'dexie-react-hooks'
import i18n from "../../helper/i18n";

const useFetchAccountsDb = () => {
    const currentMapId = useAppSelector(selectMapId)
    const accounts = useLiveQuery(() => db.table(dbTableKeys.accounts.table)
        .where(dbTableKeys.accounts.map_id)
        .equals(currentMapId)
        .toArray());

    let values = [...financialAccounts]
    if (accounts?.length)
        values = [...financialAccounts, ...accounts]
    return values

}

const fetchAccountDb = async () => {


    const accounts = await db.table(dbTableKeys.accounts.table)

        .toArray();


    let values = [...financialAccounts]
    if (accounts?.length)
        values = [...financialAccounts, ...accounts]
    return values

}

const saveAccountDb = async (account: AccountType) => {
    if (account.id) {
        await db.table(dbTableKeys.accounts.table).update(account.id, account);
    }
    else await db.table(dbTableKeys.accounts.table).add(account);

    return;
}


const deleteAccountDb = async (account: AccountType) => {
    let accounts = await filterEntriesBy({ account_id: account.id })

    if (accounts.length == 0) await db.table(dbTableKeys.accounts.table).delete(account.id);
    else return alert(i18n.t(en.account_used_error_delete_message))
}

export {
    useFetchAccountsDb,
    saveAccountDb,
    deleteAccountDb,
    fetchAccountDb
}
