import { EntryType } from "../../types/EntryType";
import { dbTableKeys } from "./dbKeys";
import { db } from "./indexedDb";
import { useLiveQuery } from 'dexie-react-hooks'

const useFetchEntriesDb = () => {
    const entries = useLiveQuery(() => db.table(dbTableKeys.entries.table)

        .toArray());

    return entries
}
const saveEntryDb = async (entries: Array<EntryType>) => {
    await db.table(dbTableKeys.entries.table).add(entries);

}

interface FilterByType {
    description?: string;          // Optional: Filter by description (partial match)
    account_id?: number;            // Optional: Filter by account ID
    debitRange?: [number, number]; // Optional: Filter by debit value range [min, max]
    creditRange?: [number, number];// Optional: Filter by credit value range [min, max]
    journal_id?: number;            // Optional: Filter by journal ID
}

const filterEntriesBy = async (filterBy: FilterByType) => {
    try {
        const { description, account_id, debitRange, creditRange, journal_id } = filterBy;

        const results = await db.table(dbTableKeys.entries.table)
            .where("id")
            .above(0) // Ensure we fetch all entries
            .filter(entry => {
                let matches = true;

                if (description) {
                    matches = matches && entry.description.includes(description);
                }
                if (account_id) {
                    matches = matches && entry[dbTableKeys.entries.account_id] === account_id;
                }
                if (debitRange) {
                    const [minDebit, maxDebit] = debitRange;
                    matches = matches && entry.debit >= minDebit && entry.debit <= maxDebit;
                }
                if (creditRange) {
                    const [minCredit, maxCredit] = creditRange;
                    matches = matches && entry.credit >= minCredit && entry.credit <= maxCredit;
                }
                if (journal_id) {
                    matches = matches && entry[dbTableKeys.entries.journal_id] === journal_id;
                }

                return matches;
            })
            .toArray();

        return results;
    } catch (error) {
        console.error("Error filtering entries:", error);
        return [];
    }
}
const deleteEntriesDb = async (entryIds: number[]) => {
    try {
      // Delete all entries with the given IDs
      // @ts-ignore
      await db.entries.where('id').anyOf(entryIds).delete();
      console.log(`Entries with IDs [${entryIds.join(', ')}] have been deleted.`);
    } catch (error) {
      console.error('Failed to delete entries:', error);
    }
  };
  

const deleteEntriesByJournalIds = async (journalIds: Array<number>) => {
    const entriesToDelete = await db.table(dbTableKeys.entries.table)
        .where(dbTableKeys.entries.journal_id).anyOf(journalIds).toArray();

    const deletePromises = entriesToDelete
        .map(entry => db.table(dbTableKeys.entries.table).delete(entry.id));

    return Promise.all(deletePromises);
};



export {
    useFetchEntriesDb,
    saveEntryDb,
    deleteEntriesDb,
    deleteEntriesByJournalIds,
    filterEntriesBy
}