import Dexie from "dexie";
import { importDB, exportDB, importInto } from "dexie-export-import";
import { db } from "./indexedDb";

const databaseName = "BossAccounting";

interface FuncExportDatabase {
  filename: string
}
export async function exportDatabase(params: FuncExportDatabase) {
  const { filename } = params
  // Open an arbitrary IndexedDB database:

  let contentType = "application/json;charset=utf-8;";

  const db = await new Dexie(databaseName).open();
  // Export it
  const blob = await exportDB(db, { prettyJson: true, });

  // let json = await [];

  // const blob = await exportDB(db);
  // const _json = await JSON.parse(await blob.text());
  // await json.push(_json);

  await blobToJSON(blob)
    .then(json => {
      console.log('JSON:', json)


      var a = document.createElement("a");
      a.download = filename;
      a.href =
        "data:" +
        contentType +
        "," +
        encodeURIComponent(JSON.stringify(json));
      a.target = "_blank";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);

    })
    .catch(err => console.error('Error:', err));

}
function blobToJSON(blob: Blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      try {
        const json = JSON.parse(reader.result.toString());
        resolve(json);
      } catch (err) {
        reject(err);
      }
    };

    reader.onerror = () => {
      reject(reader.error);
    };

    reader.readAsText(blob);
  });
}


export const importDatabase = async (file: File) => {
  const fileReader = new FileReader();
  await fileReader.readAsText(file, "UTF-8");
  // { const blobContent = reader.result
  // db = await importDB(file);
  // console.log('import')+
  await db.delete();
  db.open()
  await importInto(db, file);

  window.location.reload();


}