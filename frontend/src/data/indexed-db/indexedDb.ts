import Dexie from 'dexie';


export const indexedDbName = 'BossAccounting'
export const db = new Dexie(indexedDbName);

db.version(1).stores({
    financialcycles: '++id, start_at',
    accounts: '++id, name, financial_statement',
    journals: '++id, description, date, number, financialcycle_id', // Primary key and indexed props
    entries: '++id, description, accountId, debit, credit, journalId',
    companies:'++id, name ,language, color_mode'
});