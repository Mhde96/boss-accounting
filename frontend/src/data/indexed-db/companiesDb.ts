import { CompanyType } from "../../types/CompanyType";
import { FinancialCycleType } from "../../types/FinancialCycleType";
import { MapType } from "../../types/MapType";
import { store } from "../redux/store";
import { dbTableKeys } from "./dbKeys";
import { saveFinancialCycleDb } from "./financial-cyclesDb";
import { db } from "./indexedDb";
import { useLiveQuery } from 'dexie-react-hooks'

const useFetchCompaniesDb = () => {
    const companies = useLiveQuery(() => db.table(dbTableKeys.companies.table).toArray());
    return companies
}


const fetchCompaniesDb = async () => {
    const companies = await db.table(dbTableKeys.companies.table).toArray()

    return companies
}


const saveCompanyDb = async (company: CompanyType) => {

    if (company.id) {
        db.table(dbTableKeys.companies.table).update(company.id, company);
    } else {
        await db.table(dbTableKeys.companies.table).add(company);

    }
}
const deleteCompanyDb = () => { }

export {
    fetchCompaniesDb,

    useFetchCompaniesDb,
    saveCompanyDb,
    deleteCompanyDb,
}