import { Cookies } from "react-cookie";
import { cookiesKey } from "./cookiesKey";

const cookies = new Cookies();

export const useGetCookies = (cookiesKey: string) => {
    const value = cookies.get(cookiesKey)
    return value
}

export const useSetCookies = () => {

    function setCookies(cookieKey: string, value: string) {
        cookies.set(cookieKey, value)
    }

    return setCookies
}


export const getDbType = () => {
    let dbType = ""
    dbType = cookies.get(cookiesKey.dbType)

    return { dbType }
}