export const cookiesKey = {
  user: "user",
  currentFinancialCycleId: "currentFinancialCycleId",
  language: "language",
  colorMode: "colorMode",
  introductionApp: "introductionApp",
  dbId: "dbId",
  mapId: "mapId",
  mapName: "mapName",
  dbType: 'dbType',
  largeScreenNotfication: "lsn"
};