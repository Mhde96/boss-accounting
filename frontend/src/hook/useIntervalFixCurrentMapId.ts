import { useEffect } from "react";
import {
  selectCurrentFinancialCycleId,
  selectMapId,
} from "../data/redux/app/appSlice";
import { SelectMaps } from "../data/redux/data/dataSlice";
import { useAppDispatch, useAppSelector } from "../data/redux/hooks";
import { MapType } from "../types/MapType";
import { initializePlatformAsync } from "../data/redux/data/dataAsync";
import {
  changeCurrentFinancialCycleIdAsync,
  changeCurrentMapId,
} from "../data/redux/app/appAsync";

// @ts-ignore
let _maps = [];
// @ts-ignore
let _currentMapId = undefined;
// @ts-ignore
let timer = undefined;
export const useIntervalFixCurrentMapId = () => {
  // const dispatch = useAppDispatch();
  // const maps = useAppSelector(SelectMaps);
  // const currentMapId = useAppSelector(selectMapId);
  // const currentFinancialcycleId = useAppSelector(selectCurrentFinancialCycleId);
  // const currentMap = (mapId: number): any => {
  //   // @ts-ignore
  //   return _maps.find((map: MapType) => map.id == mapId);
  // };

  // const FixFinancialcycle = async () => {
  //   // @ts-ignore
  //   const map: MapType = currentMap(_currentMapId);
  //   const checkFinancialcycle = map?.financialcycles?.some(
  //     (financialcycle) => financialcycle.id == currentFinancialcycleId
  //   );

  //   if (currentMapId == undefined) {
  //     let lastMap = _maps.length - 1;
  //     // @ts-ignore
  //     const map = _maps[lastMap];
  //     // let lastFinancialcycleInMap = map?.financialcycles.length - 1
  //     dispatch(changeCurrentMapId(map));
  //   }

  //   if (checkFinancialcycle) return;
  //   else {
  //     if (map?.financialcycles?.length > 0) {
  //       let lastFinancialcycleInMap = map.financialcycles.length - 1;
  //       dispatch(changeCurrentFinancialCycleIdAsync(lastFinancialcycleInMap));
  //     } else {
  //       dispatch(initializePlatformAsync());

  //       timer = setTimeout(() => FixFinancialcycle(), 2000);
  //     }
  //   }
  // };

  // useEffect(() => {
  //   FixFinancialcycle();
  // }, []);

  // useEffect(() => {
  //   _maps = maps;
  //   _currentMapId = currentMapId;
  //   setTimeout(() => {
  //     // @ts-ignore
  //     clearTimeout(timer);
  //   }, 2100);
  // }, [maps, currentMapId]);
};
