import { useMedia } from "react-use";

interface ReturnType {
  isPhone: boolean;
  isTablet: boolean;
  isLaptop: boolean;
}

export const useBreakpoints = (): ReturnType => {
  // links to frontend/src/styles/breakpoints.scss
  const isPhone = useMedia("(max-width: 575px)");
  const isTablet = useMedia("(min-width: 576px)");
  const isLaptop = useMedia("(min-width: 992px)");

  return { isPhone, isTablet, isLaptop };
};
