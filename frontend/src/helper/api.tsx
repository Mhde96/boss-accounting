import axios from "axios";
import { baseURL } from "../constant/configEndpoints";
import { Cookies } from "react-cookie";
import { cookiesKey } from "../data/cookies/cookiesKey";

const api = axios.create({
  baseURL: baseURL,
  timeout: 200000,
  headers: {
    // "Content-Type": "multipart/form-data",
    // 'Content-Type': 'application/x-www-form-urlencoded'
    Accept: "application/json",
  },
});

api.interceptors.request.use((config) => {
  const cookies = new Cookies();
  const token = () => cookies.get(cookiesKey.user)?.token;
  const user_id = () => cookies.get(cookiesKey.user)?.id;
  const currentFinancialCycleId = () =>
    cookies.get(cookiesKey.currentFinancialCycleId);
  const currentMapId = () => cookies.get(cookiesKey?.mapId);
  const role = cookies.get(cookiesKey?.user)?.role;

  // @ts-ignore
  config.headers = {
    ...config.headers,
    Authorization: `Bearer ${token()}`,
    financialcycle_id: currentFinancialCycleId(),
    map_id: currentMapId(),
    user_id: user_id(),
    role: role,
  };

  config.data = {
    ...config.data,
    map_id: currentMapId(),
    financialcycle_id: currentFinancialCycleId(),
  };

  return config;
});

export { api };
