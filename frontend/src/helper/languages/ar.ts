import { en, english } from "./en";

export const ar: typeof english = {

  journals: "دفتر اليومية",
  add_journal: " اضافة سند",
  entries: "سندات القيد",

  accounts: "الحسابات",
  account_statement: "كشف حساب",
  statement: "كشف حساب",
  trial_balance: "ميزان المراجعة",
  trading: "المتاجرة",
  profit_and_loss: "الارباح والخسائر",
  income_statement: "قائمة الدخل",
  balance_sheet: "الميزانية الختامية",
  about: "معلومات عنا",
  from_where_we_began: 'من أين بدأنا',
  blog: "مقالات",
  contact: "اتصل بنا",
  accounts_types: "انواع الحسابات",

  journal_card_1: "تحقق من حساباتك",
  journal_card_3: "اضف سندات القيد خاصتك لتصل الى نتيجتك",

  // auth
  password: 'كلمة المرور',
  confirm_password: 'تأكيد كلمة المرور',
  register: 'تسجيل حساب جديد',
  logout: "تسجيل خروج",
  change_password: "تغير كلمة المرور",
  old_passowrd: "كلمة المرور القديمة",
  new_passowrd: "كلمة المرور الجديدة",
  login: "تسحيل دخول",
  sign_up: 'أنشاء حساب',
  loading: 'انتظر',
  occupations: 'المهنة',
  // main
  new: "جديد",
  add: "اضافة",
  update: "تعديل",
  delete: "حذف",
  delete_message: "هل انت متأكد من انك تريد حذف هذا العنصر",
  rows: "الأسطر",

  description: "الوصف",
  account: "الحساب",
  debit: "مدين",
  credit: "دائن",
  date: "التاريخ",
  number: "رقم السند",
  count: "عدد السندات",
  value: "القيمة",
  total: "الأجمالي",
  profile: "الصفحة الشخصية",
  name: "الأسم",
  account_name: "أسم الحساب",
  account_key: "رمز الحساب",
  settings: "الأعدادات",
  theme: "الالوان",
  dark: "مظلم",
  light: "مضيئ",
  auto: "اتوماتيك",
  language: "اللغة",
  yes: "نعم",
  no: "لا",
  confirm_logout: "هل انت متأكد من تسجيل الخروج",
  accept: "موافق",
  home: "الرئيسية",
  close: "أغلاق",
  save: "حفظ",

  select_account: "اختر حساب",
  import: "استيراد",
  export: "تصدير",
  create: "انشاء",
  cancel: "الغاء",
  chooseYourDataBase: "اختر قاعدة البيانات ",
  chooseYourFianacialCycles: "اختر الدورة المالية",
  addFinancialCycle: "اضف دوره ماليه",
  thereIsNoFC: "لايوجد دوره ماليه",
  startDate: "بداية الوقت",
  end_date: "نهاية الوقت",
  email: 'أيميل',

  version: "الاصدار",
  about_us_story: "في عام 2015، لاحظ شخص متحمس للمحاسبة مشكلة: كانت برامج المحاسبة المكلفة عقبة صعبة للطلاب مثله. ومع ذلك، كان لديهم فكرة رائعة! لماذا لا نقوم بإنشاء حلاً برمجيًا مجانيًا وسهل الاستخدام؟ تقدم اليوم، وفجأة! لقد حولوا المستحيل إلى واقع. الآن، حان دورك للانضمام والتأثير في عالم المحاسبة! 🚀",
  free: "مجاني",
  only_support_journals: "فقط تدعم سندات القيد",

  email_is_required: "البريد الإلكتروني مطلوب",
  email_must_be_valid: "البريد الإلكتروني يجب أن يكون صالحًا ",
  password_is_required: "كلمة المرور مطلوبة",
  password_must_be_at_least_characters_long: "كلمة المرور يجب أن تكون على الأقل 4 أحرف",

  name_is_required: "اسم المستخدم مطلوب",
  field_is_required: 'هذا الحقل مطلوب',
  occupation_is_required: 'المهنة مطلوبة',
  passwords_must_match: 'كلمة المرور غير متطابقة',


  // Occupations
  student: 'طالب',
  accountant: "محاسب",
  developer: 'مطور',
  teacher: 'استاذ',
  other: 'اخرى',


  // general
  next: "التالي",
  back: "عودة",
  continue: 'متابعة',
  first_step: ' 🥳🌟 لقد قمت بالخطوة الاولى ',
  open_dashboard: 'لوحة التحكم',
  congratulation: 'مبارك',
  enter_your_start_date: 'أدخل تاريخ البداية',
  enter_your_company_name: 'أدخل اسم الشركة',
  start_using_app: 'كل ماعليك فعله الأن هو البداية بتعريف الحسابات وادخال قيود اليومية',
  update_company_name: 'تعديل اسم الشركة',
  // versions 
  version_1_desc: "  نحن متحمسون للإعلان عن إطلاق النسخة الأولى للإنتاج من Boss-Accounting 🎉🎉  "
  ,
  backup: 'نسخة احتياطية',
  filename: 'أسم الملف',
  agree: 'قبول',
  replace_terms: 'سوف تخسر البيانات القديمة واستبدالها ب بيانات جديدة ',


  title_delete_financial_cycle: 'حذف الدورة المالية',
  body_confirm_delete_financial_cycle: 'هل انت متأكد من حذف هذه الدورة المالية سوف تخسر جميع البيانات وانت غير قادر على اعادها',


  releases: 'الاصدارات',
  title_large_screen_notification: 'نحن ننصح بفتح التطبيق من جهاز شاشته كبيرة للحصول على افضل تجربة ممكنة',

  do_not_show_again: 'عدم اظهارها مرة اخرى',
  net_profit: 'ربح صافي',
  net_loss: 'خسارة صافية',

  gross_profit: 'ربح المجمل',
  gross_loss: 'خسارة مجملة',
  financial_cycle_no_data: 'لم يتم تعريف اي دورة مالية',
  confirm_delete_journal: 'هل انت متأكد من حذف القيد رقم',

  account_used_error_delete_message: 'هذا الحساب مستخدم لايمكن حذفه',

  entriesTableMessageDebitCreditMismatch: 'المدين والدائن غير متساويين.',
  entriesTableMessageZeroDebitCredit: 'يجب ألا يكون المدين والدائن صفراً.',
  entriesTableMessageDescriptionWithoutAccount: 'لا يمكنك إضافة وصف بدون حساب في الصف.',
  entriesTableMessageRowZeroDebitCredit: 'يجب ألا يكون المدين والدائن صفراً في الصف.',
  entriesTableMessageRowZeroDebitOrCredit: 'لايجب اضافة قيمة بدون حساب كما في الصف',
  entriesTableMessageRowGreaterZeroDebit: 'مجموع المدين لايجب ان يساوي 0 ',
  entriesTableMessageRowGreaterZeroCredit: 'مجموع الدائن لايجب ان يساوي 0',

  admin:'المشرف',
  chosefile:'اختر ملف',
  delete_journal:'حذف سند قيد',
  
};


