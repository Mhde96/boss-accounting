import { createPropertyNameObject } from '../../utils/objects'
export const english = {
  journals: "Journals",
  add_journal: "Add Journal",
  entries: "Entries",

  accounts: "Accounts",
  account_statement: "Account Statement",
  statement: "Statement",
  trial_balance: "Trial Balance",
  trading: "Trading",
  profit_and_loss: "Profit And Loss",
  income_statement: "Income Statement",
  balance_sheet: "Balance Sheet",
  about: "About us",
  from_where_we_began: "From where we began",
  blog: "Blog",
  contact: "Contact-Us",
  accounts_types: "Accounts Types",

  journal_card_1: "Check Your Journals",
  journal_card_3: "add your journals to get your results",

  // auth
  password: 'Password',
  confirm_password: "Confirm Password",
  logout: "Logout",
  change_password: "Change Password",
  old_passowrd: "Old Password",
  new_passowrd: "New Password",
  email: "Email",
  login: 'Login',
  register: "Register",
  loading: 'loading',
  name_is_required: "Name is required",
  field_is_required: 'This field is required',
  email_is_required: "Email is required",
  email_must_be_valid: "Email must be valid",
  password_is_required: "Password is required",
  password_must_be_at_least_characters_long: "Password must be at least 4 characters long",
  occupation_is_required: 'Occupation is required',
  passwords_must_match: 'Passwords must match',


  sign_up: 'Sign Up',
  occupations: 'Occupations',
  // main
  new: "New",
  add: "Add",
  update: "Update",
  delete: "Delete",
  delete_message: "are you sure you want to delete this elment",
  rows: "Rows",
  description: "description",
  account: "Account",
  debit: "Debit",
  credit: "Credit",
  date: "Date",
  number: "Number",
  count: "Count",
  value: "Value",
  total: "Total",
  profile: "Profile",
  name: "Name",
  account_name: "Account Name",
  account_key: "Account Key",
  settings: "Settings",
  theme: "Theme",
  dark: "Dark",
  light: "Light",
  auto: "Auto",
  language: "Language",
  yes: "Yes",
  no: "No",
  confirm_logout: "are you sure you want to logout ",
  accept: "Accept",
  home: "Home",
  close: "Close",
  save: "Save",
  select_account: "Select Account",
  import: "Import",
  export: "Export",
  cancel: "Cancel",
  create: "Create",
  chooseYourDataBase: "Choose your data base",
  chooseYourFianacialCycles: "Choose your Financial Cycle",
  addFinancialCycle: "Add financial cycle",
  thereIsNoFC: "there is no finacialCycles",
  startDate: "Start Date",
  end_date: "End Date",
  about_us_story: "In 2015, someone passionate about accounting noticed a problem:  expensive accounting software was a tough hurdle for studentslike them. However, they had a brilliant idea! Why not create afree, user-friendly software solution? Fast forward to today,and voila! They've turned the impossible into reality. Now, it'syour chance to jump in and make a splash in the accounting  world! 🚀",
  version: "version",
  free: 'Free',
  only_support_journals: "Only Support Journals",


  // Occupations
  student: 'Student',
  accountant: "Accountant",
  developer: 'Developer',
  teacher: 'Teacher',
  other: 'Other',
  //admin:
  admin: "admin",



  // versions 
  version_1_desc: " We are thrilled to announce the launch of the first production version of Boss-Accounting ! 🎉",


  // general
  next: 'Next',
  back: 'Back',
  continue: "Continue",
  open_dashboard: 'Open Your Application',
  first_step: "You've taken first step 🥳🌟 ",
  congratulation: 'Congratulation',
  enter_your_start_date: 'Enter your start date',
  enter_your_company_name: 'Enter Your Company Name',
  start_using_app: 'All you need to do now is to start by defining the accounts and entering the journal entries.',
  update_company_name: 'Update Company Name',

  backup: 'Backup',
  filename: 'File Name',
  chosefile: 'Chose File',
  agree: "Agree",
  replace_terms: 'You will lost all your data then replaced with imported file ',

  title_delete_financial_cycle: 'Delete Financial Cycle',
  body_confirm_delete_financial_cycle: "Are you sure you want to delete this financial cycle? You will lose all data and will not be able to recover it"
  ,
  releases: 'Releases',


  title_large_screen_notification: "We recommend opening the application from a device with a large screen for the best possible experience",

  do_not_show_again: "Don't show again",
  net_profit: 'Net Profit',
  net_loss: 'Net Loss',
  gross_profit: 'Gross Profit',
  gross_loss: 'Gross Loss',

  financial_cycle_no_data: ' No financial cycle is defined ',
  delete_journal: 'Delete Journal',
  confirm_delete_journal: 'are you sure you want to delete journal',
  account_used_error_delete_message: 'This account is in use and cannot be deleted',
  
  entriesTableMessageDebitCreditMismatch: 'Debit and credit are not equal.',
  entriesTableMessageZeroDebitCredit: 'Debit and credit must not be zero.',
  entriesTableMessageDescriptionWithoutAccount: "You mustn't add a description without an account in the row.",
  entriesTableMessageRowZeroDebitCredit: 'Debit and credit must not equal 0 in the row ',
  entriesTableMessageRowGreaterZeroDebit: 'Total debit must not equal 0 ',
  entriesTableMessageRowGreaterZeroCredit: 'Total credit must not equal 0',
  entriesTableMessageRowZeroDebitOrCredit: 'A value must not be added without an account, as in row ',


};



export const en = createPropertyNameObject(english);



