import React, { useEffect } from 'react';
import { NavigateFunction, useNavigate } from 'react-router-dom';

let navigate: NavigateFunction;

export const setNavigation = (nav: NavigateFunction) => {
    navigate = nav;
};

export const navigateTo = (path: string | number) => {
    if (navigate) {
        // @ts-ignore
        navigate(path);
    } else {
        console.error('Navigate function is not set');
    }
};

// In a component or somewhere during app initialization

// @ts-ignore
const NavigationInitializer = () => {
    const navigate = useNavigate();

    useEffect(() => {
        setNavigation(navigate);
    }, []);

    return null;
};

export default NavigationInitializer;