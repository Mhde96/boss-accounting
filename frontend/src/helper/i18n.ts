import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { ar } from "./languages/ar";
import { english } from "./languages/en";

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translation: english,
    },
    ar: {
      translation: ar,
    },
    default: {
      translation: english,
    },
  },
  react: {
    useSuspense: false, //   <---- this will do the magic
  },
});

export default i18n;
