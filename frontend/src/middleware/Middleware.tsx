import React, { memo, useEffect } from "react";
import { SelectFinacialCycles } from "../data/redux/data/dataSlice";
import { useAppDispatch, useAppSelector } from "../data/redux/hooks";
import { selectCurrentFinancialCycleId } from "../data/redux/app/appSlice";
import { changeCurrentFinancialCycleIdAsync } from "../data/redux/app/appAsync";
import { LargeScreenNotification } from "../widgets/popup/LargeScreenNotification";
export const Middleware = () => {
  const dispatch = useAppDispatch();
  const financialCycles = useAppSelector(SelectFinacialCycles);
  const currentFinancialCyclesId = useAppSelector(
    selectCurrentFinancialCycleId
  );

  useEffect(() => {
    if (financialCycles.loading == false) {
      const current = financialCycles.data.find(
        (item) => item.id == currentFinancialCyclesId
      );
      if (current == undefined && financialCycles.data.length == 0) {
        dispatch(changeCurrentFinancialCycleIdAsync(0));
      } else if (financialCycles.data.length > 0) {
        const index = financialCycles.data.length - 1;
        const financialCycle = financialCycles.data[index];
        dispatch(changeCurrentFinancialCycleIdAsync(financialCycle.id));
      } else {
        dispatch(changeCurrentFinancialCycleIdAsync(0));
      }
    }
  }, [financialCycles.data.length, financialCycles.loading]);

  return (
    <>
      <LargeScreenNotification />
    </>
  );
};
