import { Menu, MenuItem, MenuItemConstructorOptions } from 'electron';
import { isDev } from '../../utils/util';


const template: Array<(MenuItemConstructorOptions) | (MenuItem)> = [
    {
        id: '1', label: 'File', submenu: [
            // { label: 'Import', },
            // { label: 'Export', },
            { role: 'quit', },

        ]
    },
    {
        id: '2', label: 'View', submenu: [
            // { label: 'Language' },
            // { label: 'Dark Mode' },
            { role: 'togglefullscreen' },
            { role: 'zoomIn' },
            { role: 'zoomOut' }
        ]
    },
    {
        id: '3', label: 'Help', submenu: [
            // { label: 'Welcome' },
            // { label: 'Video Tutorials' },
            // { label: 'Join us on youtube' },
            // { label: 'Check for update' },
            // { label: 'Show Release App' },
            { role: 'about' },



        ]
    },

]

if (isDev()) {
    //@ts-ignore
    template.find(item => item.id == '3').submenu.push({ role: 'toggleDevTools' });
}

export function buildDesktopMenu() {


    const menu = Menu.buildFromTemplate(template)

    Menu.setApplicationMenu(menu)


}


