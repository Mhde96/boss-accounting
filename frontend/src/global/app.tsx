// import { createRoot } from "react-dom/client";
// import { App } from "../Application";


// const root = createRoot(document.body);
// root.render(<App />);

import React from "react";
import { createRoot } from "react-dom/client";
import { App } from "../Application";
// import { electronApp, optimizer, is } from "@electron-toolkit/utils";




function removeAllChildren(parent: HTMLElement) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
} // Use it on the body element, for example removeAllChildren(document.

removeAllChildren(document.body);
const container = document.createElement("div");
document.body.appendChild(container);
const root = createRoot(container);
root.render(<App />);


// let csp = "script-src 'self'; object-src 'none';";
// if (is.dev) {
//   csp = "script-src 'self' 'unsafe-inline'; object-src 'none';";
// }
// process.env['ELECTRON_DISABLE_SECURITY_WARNINGS']=true

