import React, { useEffect } from "react";

import "./home-styles.scss";
import { DbControlContainer } from "../db-control/DbControlContainer";
import {  Col, Container, Row } from "react-bootstrap";
import { AboutContainer } from "../system/about/AboutContainer";
import { CompanyTitle } from "../../widgets/home/CompanyTitle";

export const HomePage = () => {
  // const currentMapId = useSelector(selectMapId);
  // const maps = useSelector(SelectMaps);
  // const currentMap = maps.find((map) => map.id == currentMapId);

  return (
    <div>
      <div id="home-styles">
        <Container>
          <Row>
            <Col xs={12}>
              <CompanyTitle />
            </Col>
          </Row>

          <Row>
            <Col xs={12}>
              <DbControlContainer />
            </Col>
          </Row>

         
          
        </Container>
      </div>
    </div>
  );
};
