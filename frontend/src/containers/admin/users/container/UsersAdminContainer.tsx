import React from 'react'
import GenericTable from "../../../../components/genericTable/components/GenericTable";
import { HeaderWidget } from "../../../../widgets/header/HeaderWidget";
import CustomTable from "../../../../components/genericTable/container/CustomTable";
import { endpoints } from "../../../../constant/endpoints";

const UsersAdminContainer = () => {
  const columnShowInTheTable = [
    "id",
    "name",
    "email",
    "created_at",
    "updated_at",
  ];
  return (
    <>
      {/* <HeaderWidget /> */}
      <CustomTable
        serviceName={endpoints.admin_users}
        mapper={columnShowInTheTable}
      />
    </>
  );
};
export default UsersAdminContainer;
