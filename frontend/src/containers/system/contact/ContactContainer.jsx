import React from "react";

import { Button, Container } from "react-bootstrap";
import { ReactSVG } from "react-svg";
import { Text } from "../../../components/text/Text";
import { useColors } from "../../../styles/variables-styles";
import "./contact-page-styles.scss";
import { useTranslation } from "react-i18next";
import { selectLanguage } from "../../../data/redux/app/appSlice";
import { useSelector } from "react-redux";
import { en } from "../../../helper/languages/en";

const description = {
  en: `In 2015, someone passionate about accounting noticed a problem: expensive accounting software 
was a tough hurdle for students like them. However,
they had a brilliant idea! Why not create a free, user-friendly software solution?
Fast forward to today, and voila! They've turned the impossible into reality. Now,
it's your chance to jump in and make a splash in the accounting world! 🚀`,

  ar: `في عام 2015، لاحظ شخص شغوف بالمحاسبة مشكلة: كانت البرامج المحاسبة مكلفة وعقبة صعبة أمام الطلاب مثله. ومع ذلك، كانت لديه فكرة رائعة! لماذا لا يتم إنشاء برنامج محاسبي لايوجد فيه تكلفة  وسهل الاستخدام؟ ومع مرور الزمن والكثير من التجارب والمحاولات، توصلنا الى مانحن عليه الأن ! لقد تحول المستحيل إلى واقع. 
تم انشاء نظام مبني على الاسس المحاسبية الصحيحة وهو متاح للاستخدام بين ايديكم وننمنى من كل من كان لديه اي فكرة اضافية او وجد اي مشكلة في الاستخدام او يحب ان يشارك في هذا العمل ان يتواصل معنا ولا يتردد 🚀

`,
};

const URL = "https://wa.me";

export const whatsappLink = `${URL}/${"+971522215489"}`;
export const ContactContainer = () => {
  const { t, i18n } = useTranslation();
  const language = useSelector(selectLanguage);

  const colors = useColors();

  return (
    <div id="contact-page-styles" dir={i18n.dir()}>
      <Container style={{ marginTop: 12 }}>
        <div className=" about-container">
          <Text fs="f2" bold>{t(en.from_where_we_began)}</Text>
          <br/>
          <Text fs="f3">
            {language === "ar" ? description.ar : description.en}
          </Text>
        </div>

        <br />
        <div
          onClick={() => {
            window.open(whatsappLink);
          }}
        >
          <Button style={{ display: "flex" }}>
            <Text>Send A message </Text>

            <ReactSVG
              style={{
                fill: colors.text,
                minWidth: 24,
                maxWidth: 24,
              }}
              src="assets/icons/whatsapp.svg"
            />
          </Button>
        </div>
      </Container>
    </div>
  );
};
