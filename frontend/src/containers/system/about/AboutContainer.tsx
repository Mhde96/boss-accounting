import React from "react";
import "./about-page-styles.scss";
import { Text } from "../../../components/text/Text";
import { useSelector } from "react-redux";
import { selectLanguage } from "../../../data/redux/app/appSlice";
import { useTranslation } from "react-i18next";
import { en } from "../../../helper/languages/en";
import { Container } from "react-bootstrap";
const array = [
  // {
  //   title: "Multi-Company Support:",
  //   title_ar: "دعم متعدد الشركات:",
  //   features: ["Seamlessly manage multiple companies within the platform."],
  //   features_ar: ["إدارة متعددة الشركات بسهولة داخل المنصة."],
  // },
  {
    title: "Multi-Financial Cycle Management:",
    title_ar: "إدارة دورات مالية متعددة:",
    features: [
      "Create and manage financial cycles tailored to each company's needs.",
    ],
    features_ar: ["إنشاء وإدارة دورات مالية مصممة وفقًا لاحتياجات كل شركة."],
  },
  // {
  //   title: "Company-Specific Accounts:",
  //   title_ar: "حسابات مخصصة للشركة:",
  //   features: ["Easily add and manage accounts for different companies."],
  //   features_ar: ["إضافة وإدارة الحسابات بسهولة للشركات المختلفة."],
  // },
  {
    title: "Company-Specific Journals:",
    title_ar: "دفاتر يومية مخصصة للشركة:",
    features: [
      "Record transactions in journals specific to each company and financial cycle.",
    ],
    features_ar: ["تسجيل المعاملات في دفاتر يومية مخصصة لكل شركة ودورة مالية."],
  },
  {
    title: "Account Statements:",
    title_ar: "كشوف الحسابات:",
    features: [
      "Generate detailed account statements for comprehensive financial tracking.",
    ],
    features_ar: ["إنشاء كشوف حسابات مفصلة لتتبع مالي شامل."],
  },
  {
    title: "Trial Balance:",
    title_ar: "ميزان المراجعة:",
    features: [
      "Maintain a trial balance to ensure accuracy in financial reporting.",
    ],
    features_ar: ["الحفاظ على ميزان المراجعة لضمان الدقة في التقارير المالية."],
  },
  {
    title: "Trading Account:",
    title_ar: "حساب التداول:",
    features: [
      "Monitor trading activities and performance within the platform.",
    ],
    features_ar: ["مراقبة أنشطة التداول والأداء داخل المنصة."],
  },
  {
    title: "Profit and Loss Accounts:",
    title_ar: "حسابات الأرباح والخسائر:",
    features: [
      "Analyze profit and loss statements to assess financial health.",
    ],
    features_ar: ["تحليل بيانات الأرباح والخسائر لتقييم الصحة المالية."],
  },
  {
    title: "Balance Sheet:",
    title_ar: "الميزانية العمومية:",
    features: ["Access comprehensive balance sheets for each company."],
    features_ar: ["الوصول إلى الميزانيات العمومية الشاملة لكل شركة."],
  },
   {
    title: "English & Arabic Support : ",
    title_ar: "دعم اللغة الانجليزية والعربية",
    features: [
    ],
    features_ar: [
    ],
  },
  {
    title: "Dark & Light Mode:",
    title_ar: "الوضع الليلي:",
    features: [
      "Enjoy a personalized viewing experience with the option to switch between dark and light modes.",
    ],
    features_ar: [
      "التمتع بتجربة مشاهدة مخصصة مع إمكانية التبديل بين الوضع الليلي و المشرق.",
    ],
  },
];

const nextVersionFeatures = {
  title: {
    en: "Next Version 🚀",
    ar: "الإصدار التالي 🚀",
  },
  subtitle: {
    en: "Get ready for an amazing update! We're adding these exciting features in the next version:",
    ar: "سيتم اضافة الميزات التالية في الاصدار التالي : ",
  },
  features: [
    {
      en: "🌐 Desktop Version: Experience our system on your desktop!",
      ar: "🌐 إصدار سطح المكتب: اختبر نظامنا على سطح المكتب الخاص بك!",
    },
    {
      en: "📶 Offline Mode: Use our system even without the internet!",
      ar: "📶 وضع عدم الاتصال: استخدم نظامنا حتى بدون الإنترنت!",
    },
  ],
  stayTuned: {
    en: "Stay tuned for these amazing updates! 🎉",
    ar: "ترقبوا هذه التحديثات المذهلة! 🎉",
  },
};

const visionFeatures = {
  title: {
    en: "Our Vision 🌟",
    ar: "رؤيتنا 🌟",
  },
  description: {
    en: "We are thrilled to share the fantastic features we're working on:",
    ar: "سوف يتم اضافة الميزات التالية في المستقبل : ",
  },
  features: [
    {
      en: "🏬 Warehousing: Easily enter items and generate invoices for sales and purchases!",
      ar: "🏬 إدارة المستودعات: أدخل العناصر بسهولة وأصدر الفواتير للمبيعات والمشتريات!",
    },
    {
      en: "📱 Mobile App: Take our system on the go with a brand-new mobile app!",
      ar: "📱 وجود تطبيق موبايل ( IOS , ANDROID ) لسهولة الاستخدام",
    },
    {
      en: "👗 Item Variations: Add items with different sizes and colors effortlessly!",
      ar: "👗 أمكانية اضافة البضاعة من نوع الالبسة التي تتميز بمواصفات فريدة مثل نفس القطعة لديها لون وقياس",
    },
    {
      en: "🧮 Flexible Units: Add items with various units like grams, kilograms, pieces, boxes, and more!",
      ar: "🧮 الوحدات المرنة: أضف العناصر بوحدات متنوعة مثل الجرامات والكيلوجرامات والقطع والصناديق والمزيد!",
    },
  ],
};

export const AboutContainer = () => {
  const { t, i18n } = useTranslation();
  const language = useSelector(selectLanguage);

  const getFeatures = (item: Array<any>): Array<any> => {
    // @ts-ignore
    if (language == "ar") return item.features_ar;
    // @ts-ignore
    else return item.features;
  };

  const getNextVersionFeatures = () => {
    return nextVersionFeatures.features.map((feature, index) => (
      <Text fs="f4" key={index}>
        {language === "ar" ? feature.ar : feature.en}
      </Text>
    ));
  };

  const getVisionFeatures = () => {
    return visionFeatures.features.map((feature, index) => (
      <Text fs="f4" key={index}>
        {language === "ar" ? feature.ar : feature.en}
      </Text>
    ));
  };

  return (
    <Container>
      <div id="about-page-styles" dir={i18n.dir()}>
        <div className=" vision-container">
          {/* <Text fs="f2">
            {language === "ar"
              ? nextVersionFeatures.title.ar
              : nextVersionFeatures.title.en}
          </Text>
          <Text fs="f4">
            {language === "ar"
              ? nextVersionFeatures.subtitle.ar
              : nextVersionFeatures.subtitle.en}
          </Text> */}
          {/* <ol>{getNextVersionFeatures()}</ol> */}
          <Text fs="f4">
            {language === "ar"
              ? nextVersionFeatures.stayTuned.ar
              : nextVersionFeatures.stayTuned.en}
          </Text>
          <br />
          <Text fs="f2">
            {language === "ar"
              ? visionFeatures.title.ar
              : visionFeatures.title.en}
          </Text>
          <Text fs="f4">
            {language === "ar"
              ? visionFeatures.description.ar
              : visionFeatures.description.en}
          </Text>
          <ol>{getVisionFeatures()}</ol>
        </div>

        <div className="title">
          <Text fs="f2">{t(en.version)} 1.00 🚀</Text>
          <Text fs="f4">2024-05-12</Text>
        </div>
        <Text fs="f3">{t(en.version_1_desc)}</Text>
        <Text fs="f4">
          <ol>
            {array.map((item, index) => (
              <li key={index}>
                {language == "ar" ? item.title_ar : item.title}
                
                {
                // @ts-ignore
                getFeatures(item).map((feature, index) => (
                  <Text key={index}>{feature}</Text>
                ))}
              </li>
            ))}
          </ol>
        </Text>
      </div>
    </Container>
  );
};
