import React from "react";
import { useFormik } from "formik";
import { Button, Modal } from "react-bootstrap";
import { Input } from "../../components/input/Input";
import { ModalWrap } from "../../components/wrap/ModalWrap";
import * as yup from "yup";
import { useEffect } from "react";
import { useNavigate, useSearchParams, Location } from "react-router-dom";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { useSelector } from "react-redux";
import { SelectMaps } from "../../data/redux/data/dataSlice";
import { useAppDispatch } from "../../data/redux/hooks";
import { addMapsAsync, updateMapsAsync } from "../../data/redux/data/dataAsync";

const validationSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

export const DbDialogWidget = () => {
  const { t } = useTranslation();
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const id: any = searchParams.get("db");
  const companies = useSelector(SelectMaps);
  const handleClose = () => {
    navigate(-1);
  };

  

  const {
    values,
    setValues,
    handleChange,
    handleSubmit,
    errors,
    resetForm,
    setErrors,
    initialValues,
  } = useFormik({
    initialValues: {
      id: undefined,
      name: "",
      description: "",

      start_at: new Date().toISOString().substring(0, 10),
      end_at: new Date().toISOString().substring(0, 10),
    },
    validateOnChange: false,
    validationSchema,
    onSubmit: (values) => {
      resetForm();

      if (values.id) {
        // @ts-ignore
        dispatch(updateMapsAsync(values));
        handleClose();
      } else {
        let start_at = moment(values.start_at).valueOf();
        let end_at = moment(values.end_at).valueOf();
        const validation = end_at > start_at;
        if (validation) {
          // @ts-ignore
          dispatch(addMapsAsync(values));

          handleClose();
        } else {
          setErrors(
            // @ts-ignore
            { date: "End Date must be more than Start Date" });
        }
      }
    },
  });

  const handleGetDb = async () => {
    let currentDb = companies.find((company) => company.id == id);
    setValues(currentDb);
  };

  useEffect(() => {
    if (typeof parseInt(id) === "number" && parseInt(id) > 0) handleGetDb();
    else {
      setTimeout(() => {
        setValues(initialValues);
      }, 500);
    }
  }, [id]);

  return (
    <ModalWrap show={id} onHide={handleClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Create New DataBase</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* <Input placeholder={t(en.old_passowrd)} /> */}
        <Input
          key={"name"}
          onChange={handleChange("name")}
          value={values?.name}
          placeholder={"database name"}
          error={errors?.name}
        />
        <br />

        <Input
          key={"description"}
          onChange={handleChange("description")}
          value={values?.description}
          placeholder={"description"}
          error={errors?.description}
          textArea
        />

        <div>
          <label style={{ fontWeight: "bold", marginRight: 5 }}>
            {t(en.startDate)} :{" "}
          </label>
          <input
            type="date"
            value={values.start_at}
            onChange={handleChange("start_at")}
          />
        </div>

        <div>
          <label style={{ fontWeight: "bold", marginRight: 5 }}>
            {t(en.end_date)} :{" "}
          </label>
          <input
            type="date"
            value={values.end_at}
            onChange={handleChange("end_at")}
          />

          <div className="error">{
          // @ts-ignore
          errors?.date} </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button type="submit" variant="primary" onClick={() => handleSubmit()}>
          {id != "new" ? "UPDATE" : "CREATE"}
        </Button>
      </Modal.Footer>
    </ModalWrap>
  );
};

export const OpenDbDialog = (
  location: Location,
  navigate: any,
  dbId?: number
) => {
  if (dbId == undefined) navigate(location.pathname + "?db=" + "new");
  else navigate(location.pathname + "?db=" + dbId);
};
