import React, { useEffect, useState } from "react";
import "./db-control-page-styles.scss";

import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import appSlice from "../../data/redux/app/appSlice";

import { FinancialCycleContainer } from "../financialcycle/FinancialCycleContainer";

import { databaseSource } from "../../constant/databaseSource";
import { useSelector } from "react-redux";
import { SelectMaps } from "../../data/redux/data/dataSlice";

export const DbControlContainer = () => {
  const dispatch = useAppDispatch();
  const companies = useSelector(SelectMaps);

  const handleChangeDatabase = (value) => {
    dispatch(appSlice.actions.changeCurrentDbType(value));
  };

  useEffect(() => {
    handleChangeDatabase(databaseSource.local);
  }, []);

  return (
    <>
      
      <FinancialCycleContainer companies={companies} />
    </>
  );
};
