import React from "react";
import moment from "moment";
import { textEditor } from "react-data-grid";
import { endpoints } from "../../constant/endpoints";
import { endroutes } from "../../constant/endroutes";
import { api } from "../../helper/api";
import { columnsKey, empty_row } from "./journal-entry-type";
import { en } from "../../helper/languages/en";
import { navigateTo } from "../../helper/navigationService";
import { isEmpty } from "../../utils/stringUtils";
import { dbTableKeys } from "../../data/indexed-db/dbKeys";
import i18n from "../../helper/i18n";
import _ from "lodash";

// @ts-ignore
export const jounral_entry_columns = (t) => [
  // { key: columnsKey.id, name: columnsKey.id },
  // { key: columnsKey.account_id, name: columnsKey.account_id,},
  // { key: columnsKey.accountKey, name: columnsKey.accountKey,},
  {
    key: columnsKey.accountName,
    name: t(en.account),
    renderEditCell: textEditor,
    renderSummaryCell: (props: any) => {
      return (
        <div className="d-flex">
          <strong style={{ paddingRight: 5 }}>{t(en.rows) + " - "}</strong>
          <strong>{props.row.accountLength}</strong>
        </div>
      );
    },
  },
  {
    key: columnsKey.description,
    name: t(en.description),
    renderEditCell: textEditor,
    renderSummaryCell: (props: any) => {
      return <strong>{props.row.difference}</strong>;
    },
  },
  {
    key: columnsKey.credit,
    name: t(en.credit),
    renderEditCell: textEditor,
    renderSummaryCell: (props: any) => {
      return <strong>{props.row[columnsKey.credit]}</strong>;
    },
  },
  {
    key: columnsKey.debit,
    name: t(en.debit),
    renderEditCell: textEditor,
    renderSummaryCell: (props: any) => {
      return <strong>{props.row[columnsKey.debit]}</strong>;
    },
  },
  // { key: columnsKey.status, name: columnsKey.status },
];

export const journal_entry_rows = (entries?: Array<any>): any => {
  if (entries?.length) {
    return entries;
  } else {
    let rows = [];
    for (let i = 0; i < 20; i++) {
      rows.push({ ...empty_row });
    }
    return rows;
  }
};

export function rowKeyGetter(row: any) {
  return row.id;
}

export const deleteRow = (rowIdx: number, setValues: any) => {
  setValues((prevValues: any) => ({
    ...prevValues,
    entries: prevValues.entries.filter(
      (entary: any, index: number) => index != rowIdx
    ),
  }));
};

export const removeEmptyRow = async (entries: Array<any>) => {
  let _entries: Array<any> = [];
  _entries = entries.filter(
    (entry) =>
      entry[columnsKey.account_id] !== null &&
      entry[columnsKey.accountKey] !== null
  );
  // await entries.map((entry: any) => {
  //   entry.debit = parseFloat(entry.debit);
  //   entry.credit = parseFloat(entry.credit);

  //   if (
  //     entry[columnsKey.account_id] &&
  //     isString(entry[columnsKey.accountName]) &&
  //     (entry.debit > 0 || entry.credit > 0)
  //   ) {
  //     if (entry.id == null) {
  //       delete entry.id;
  //     }
  //     // delete entry[columnsKey.account_id];
  //     if (entry?.accountKey) delete entry.accountKey;
  //     _entries.push(entry);
  //   }
  // });

  return _entries;
};

export const JournalApi = async (data: any) => {
  let configration = () => {
    let method = "post";
    let url = endpoints.add_journal;

    if (data.number != "new") {
      method = "post";

      url = endpoints.update_journal;
    }

    return { method, url };
  };

  let entries: Array<any> = await removeEmptyRow(data.entries);
  api({
    ...configration(),

    data: {
      ...data,
      description: data.description,
      date: moment(data.date).format("yyyy-MM-DD"),
      entries: entries,
      number: data.number,
      user_id: data.user_id,
    },
  }).then((response) => {
    navigateTo(endroutes.journals.path);

    // alert("you had added journal");
  });
};

export const valedationEntriesValues = async ({
  values,
}: any): Promise<{ isValid: boolean; errors: any }> => {
  try {
    let errors: any = { entries: [] };
    let isTrue = true;

    let totalDebit = _.sumBy(values.entries, (entry: any) => {
      if (isNaN(parseFloat(entry.debit))) {
        return 0;
      } else {
        return parseFloat(entry.debit);
      }
    });

    let totalCredit = _.sumBy(values.entries, (entry: any) => {
      if (isNaN(parseFloat(entry.credit))) {
        return 0;
      } else {
        return parseFloat(entry.credit);
      }
    });

    let entries: Array<any> = await removeEmptyRow(values.entries);

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    entries.map((entry, index) => {
      if (entry.debit == 0 && entry.credit == 0) {
        isTrue = false;
        errors.entries.push({
          index,
          error: i18n.t(en.entriesTableMessageRowZeroDebitCredit) + (index + 1),
        });
      }
    });

    values.entries.map((entry: any, index: any) => {
      if (isEmpty(entry.description) == false && entry.accountId == null) {
        isTrue = false;
        errors.entries.push({
          entry,
          index,
          error:
            i18n.t(en.entriesTableMessageDescriptionWithoutAccount) +
            (index + 1),
        });
      }

      if (entry[dbTableKeys.entries.account_id] == null) {
        if (entry.debit > 0 || entry.credit > 0) {
          isTrue = false;
          errors.entries.push({
            acc_id: entry[dbTableKeys.entries.account_id],
            entry,
            index,
            error:
              i18n.t(en.entriesTableMessageRowZeroDebitOrCredit) + (index + 1),
          });
        }
      }
    });

    if (totalCredit != totalDebit) {
      isTrue = false;
      errors.total = i18n.t(en.entriesTableMessageDebitCreditMismatch);
    }

    if (totalCredit == 0) {
      isTrue = false;
      errors.total = i18n.t(en.entriesTableMessageRowGreaterZeroCredit);
    }

    if (totalDebit == 0) {
      isTrue = false;
      errors.total = i18n.t(en.entriesTableMessageRowGreaterZeroDebit);
    }

    return { isValid: isTrue, errors };
  } catch (err) {
    console.log(err);
  }
};
