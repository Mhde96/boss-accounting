import React from "react";
import { useFormik } from "formik";
import _ from "lodash";
import { useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import {
  fetchAccountsAsync,
  fetchJournalsAsync,
  saveJournalAsync,
} from "../../data/redux/data/dataAsync";
import {
  selectAccounts,
  selectJournals,
} from "../../data/redux/data/dataSlice";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { SelectAccountDialog } from "../../widgets/select-account/SelectAccountDialog";
import { accountType } from "../accounts/account-type";
import { journalType } from "../journals/journal-type";
import {
  jounral_entry_columns,
  journal_entry_rows,
  valedationEntriesValues,
} from "./entries-functions";
import {
  columnsKey,
  empty_row,
  entryType,
  JournalEntryPagePropsType,
} from "./journal-entry-type";
import { EntriesPage } from "./EntriesPage";
import { useTranslation } from "react-i18next";

export const EntriesContainer = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  const accounts = useSelector(selectAccounts);
  const journals = useAppSelector(selectJournals);
  const [searchParams] = useSearchParams();
  const number = searchParams.get("number");
  useEffect(() => {
    dispatch(fetchJournalsAsync());
    dispatch(fetchAccountsAsync());
  }, []);

  useEffect(() => {
    if (accounts.length && journals.length) getData();
  }, [journals, accounts, number]);
  const { values, setValues, handleChange, handleSubmit, setErrors, errors } =
    useFormik({
      initialValues: {
        number,
        description: "",
        date: new Date(),
        entries: journal_entry_rows(),
      },
      validateOnBlur: false,
      validateOnChange: false,
      validateOnMount: false,
      onSubmit: async (values) => {
        try {

          const { errors, isValid } = await valedationEntriesValues({ values });
          
          if (isValid) {
            setErrors({});
            // @ts-ignore
            dispatch(saveJournalAsync(values, navigate));
          } else {
            setErrors(errors);
          }
        } catch (err) {
          console.log(err)
        }
      },
    });

  const getData = () => {
    let data: Array<entryType> = [];
    const journal = journals.find(
      (journal: journalType) => journal.number == number
    );

    journal?.entries?.map((item) => {
      const account = accounts.find(
        (account: accountType) => account.id == item[columnsKey.account_id]
      );
      data.push({
        ...item,
        [columnsKey.accountName]: account?.name,
        // accountKey: account?.key,
      });
    });

    if (number) {
      setValues({
        ...values,
        ...journal,
        number,

        entries: [...data, ...journal_entry_rows()],
      });
    }
  };

  const checkNextJournalExist = () => {
    let index = journals.findIndex((item) => item.number == number) + 1;
    const length = journals.length;

    if (number == "new") return true;

    if (length > index) return false;
    return true;
  };

  const checkPreviousJournalExist = () => {
    let index = journals.findIndex((item) => item.number == number) + 1;
    const length = journals.length;

    if (number == "new") return true;

    if (index != 1) return false;
    return true;
  };

  const getNextJournal = () => {
    let index = journals.findIndex((item) => item.number == number) + 1;
    const length = journals.length;

    if (number == "new") {
      index = length - 1;
    }

    if (length > index) {
      const next = journals[index];
      navigate(endroutes.journalentaries(next.number).go);
      setValues({ ...values, number: next.number.toString() });
    }
  };

  const getPreviousJournal = () => {
    let index = journals.findIndex((item) => item.number == number);

    const length = journals.length;

    if (number == "new") {
      index = length;
    }

    if (index != 0) {
      const next = journals[index - 1];
      navigate(endroutes.journalentaries(next.number).go);
      setValues({ ...values, number: next.number.toString() });
    }
  };

  const handleNavigateNew = () => {
    navigate(endroutes.journalentaries().newJournal);
  };

  const handleNavigateJournals = () => navigate(endroutes.journals.path);
  // ==============================================================================
  // useState
  const [rowIndex, setRowIndex] = useState(0);
  const [open, setOpen] = useState(false);
  // ==========================================
  const [current, setCurrent] = useState({
    idx: null,
    rowIdx: null,
    column: {},
  });
  const onSelectedCellChange = ({ idx, rowIdx }: { idx: any; rowIdx: any }) =>
    setCurrent({
      ...current,
      idx,
      rowIdx,
      column: jounral_entry_columns(t)[idx],
    });

  const onRowsChange = (
    newRows: Array<entryType>,
    data: { indexes: any; column: any }
  ) => {
    const { indexes } = data;
    const index = indexes[0];
    const value = newRows[data.indexes].accountName;

    if (data.column.key == columnsKey.credit) {
      // credit
      newRows[indexes].debit = 0;
    }
    // debit
    else if (data.column.key == columnsKey.debit) {
      newRows[indexes].credit = 0;
    }

    // account_name
    else if (data.column.key == columnsKey.accountName) {
      const accountsfiltered = accounts.filter((e: accountType) =>
        e.name.toLowerCase().match(value.toLowerCase())
      );

      if (accountsfiltered?.length > 1) {
        setValues({ ...values, entries: newRows });
        setRowIndex(index);
        setOpen(true);
        return;
      } else if (accountsfiltered?.length == 1) {
        return fillAccount(accountsfiltered[0], indexes);
      } else if (accountsfiltered?.length == 0) {
        setOpen(true);
        setRowIndex(index);

        // newRows[indexes].accountName = "";
        // newRows[indexes][columnsKey.account_id] = null;
      }
    }

    if (index == values.entries?.length - 1) {
      setValues({
        ...values,
        entries: [...newRows, empty_row],
      });
    } else {
      setValues({ ...values, entries: newRows });
    }
  };

  const fillAccount = (account: accountType, rowIndex: number) => {
    setValues((prevValues) => ({
      ...prevValues,
      entries: prevValues.entries.map((entary: entryType, index: number) => {
        if (index == rowIndex) {
          let _entary = { ...entary };
          return {
            ..._entary,
            [columnsKey.accountName]: account.name,
            [columnsKey.accountKey]: 1, // account.key,
            [columnsKey.account_id]: account.id,
            // [columnsKey.account]: account,
          };
        }
        // nothing happen
        else return entary;
      }),
    }));
  };

  const summaryRows = useMemo(() => {
    const totalCredit = _.sumBy(values.entries, (item: any) =>
      Number(item.credit)
    );
    const totalDebit = _.sumBy(values.entries, (item: any) =>
      Number(item.debit)
    );
    const accountLength = values?.entries?.filter(
      (item: any) => item.account_id
    )?.length;

    const difference = totalDebit - totalCredit;
    return [
      {
        [columnsKey.credit]: totalCredit,
        [columnsKey.debit]: totalDebit,
        accountLength,
        difference,
      },
    ];
  }, [values.entries]);

  const props: JournalEntryPagePropsType = {
    values,
    onRowsChange,
    onSelectedCellChange,
    summaryRows,
    current,
    rowIndex,
    setRowIndex,
    setValues,

    handleChange,
    handleSubmit,
    getNextJournal,
    getPreviousJournal,
    handleNavigateNew,
    handleNavigateJournals,
    errors,
    checkNextJournalExist,
    checkPreviousJournalExist,
    // goAccountStatement
  };

  if (values?.entries?.length > 0)
    return (
      <>
        <SelectAccountDialog
          onSubmit={(account: accountType) => {
            setOpen(false);
            fillAccount(account, rowIndex);
          }}
          open={open}
          setOpen={setOpen}
          rowIndex={rowIndex}
          text={"2"}
          onClose={() => {}}
        />
        <EntriesPage {...props} />
      </>
    );

  return null;
};
