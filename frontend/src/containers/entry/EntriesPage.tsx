import React, { useMemo, useState } from "react";
import DataGrid, { Row as GridRow } from "react-data-grid";

import { JournalEntryPagePropsType } from "./journal-entry-type";
import "./entries-styles.scss";
import { deleteRow, jounral_entry_columns } from "./entries-functions";

import { Input } from "../../components/input/Input";
import { Breadcrumb, Button, Col, Container, Form, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import { PageTransitionProps } from "../../components/animations/AnimationPageProps";
import { navigateAccountStatement } from "../account-statment/account-statement-type";
import { useColorMemo } from "../../hook/useColorMemo";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { dateFormatUi } from "../../utils/date-format";
import "react-data-grid/lib/styles.css";
import { useBreakpoints } from "../../hook/useBreakPoint";

import { ContextMenu } from "../../widgets/data-grid/ContextMenu";
import { ContextButtonsType } from "../../types/components/ContextButtonType";
import { dbTableKeys } from "../../data/indexed-db/dbKeys";

export const EntriesPage = (props: JournalEntryPagePropsType) => {
  const { values, current, rowIndex, setRowIndex, setValues } = props;
  const colorMode = useColorMemo();
  const navigate = useNavigate();
  const { isPhone } = useBreakpoints();
  const { t, i18n } = useTranslation();
  const columns = useMemo(() => jounral_entry_columns(t), [i18n.dir()]);

  // @ts-ignore
  const rowRenderer = (key, props: any): any => {
    const { rowIdx } = props;

    return (
      <GridRow
        key={key}
        //  onContextMenu={displayMenu({ rowIdx })}
        {...props}
      />
    );
  };

  const [contextMenuProps, setContextMenuProps] = useState<{
    rowIdx: number;
    top: number;
    left: number;
    buttons: ContextButtonsType;
  } | null>(null);

  return (
    <Container>
      <motion.div {...PageTransitionProps} id={"entries-styles"}>
        <Breadcrumb>
          <Breadcrumb.Item onClick={props.handleNavigateJournals}>
            {t(en.journals)}
          </Breadcrumb.Item>
          <Breadcrumb.Item active>{t(en.entries)}</Breadcrumb.Item>
        </Breadcrumb>
        <div className="operation-container">
          <Button
            onClick={props.getPreviousJournal}
            disabled={props.checkPreviousJournalExist()}
          >
            <img
              src={"/assets/icons/angle-left-solid.svg"}
              style={{ height: 24, width: "auto" }}
            />
          </Button>

          <Form.Control disabled value={values.number} type="input" />
          <Button
            onClick={props.getNextJournal}
            disabled={props.checkNextJournalExist()}
          >
            <img
              src={"/assets/icons/angle-right-solid.svg"}
              style={{ height: 24, width: "auto" }}
            />
          </Button>

          <Input
            placeholder="date"
            value={props?.values?.date}
            onChange={(date: string) => {
              props.setValues({ ...values, date: dateFormatUi(date) });
            }}
            type="date"
          />

          {/* <Button disabled>{t(en.delete)}</Button> */}

          <Button onClick={props.handleSubmit}>
            {props.values.number == "new" ? t(en.add) : t(en.update)}
          </Button>
          {!isPhone && (
            <Button onClick={props.handleNavigateNew}>{t(en.new)}</Button>
          )}
        </div>

        <Row>
          <Col xs={12}>
            <Input
              value={values.description}
              placeholder={t(en.description)}
              onChange={props.handleChange("description")}
            />
          </Col>
        </Row>

        <hr />

        <div className="error-container">
          <div className="error"> {props.errors.total}</div>

          {props.errors.entries?.length > 0 ? (
            <div className="error"> {props.errors.entries[0].error}</div>
          ) : null}
        </div>

        <DataGrid
          className={`rdg-${colorMode} fill-grid data-grid`}
          columns={columns}
          bottomSummaryRows={props?.summaryRows}
          renderers={{ renderRow: rowRenderer }}
          rows={props.values.entries}
          onRowsChange={props.onRowsChange}
          onCellContextMenu={({ row }, event) => {
            event.preventGridDefault();
            // Do not show the default context menu
            event.preventDefault();

            const rowIdx = props.values.entries.indexOf(row);

            const buttons: ContextButtonsType = [];

            if (
              values.entries[rowIdx]?.accountKey ||
              values.entries[rowIdx]?.[dbTableKeys.entries.account_id] ||
              values.entries[rowIdx]?.[dbTableKeys.entries.id]
            ) {
              buttons.push({
                title: t(en.account_statement),
                onClick: () => {
                  navigateAccountStatement({
                    key: values.entries[rowIdx].accountKey,
                    navigate,
                  });
                },
              });

              buttons.push({
                title: t(en.delete),
                onClick: () => deleteRow(rowIdx, setValues),
              });
            }

            if (buttons.length > 0)
              setContextMenuProps({
                rowIdx,
                top: event.clientY,
                left: event.clientX,
                buttons,
              });
          }}
        />

        <ContextMenu
          contextMenuProps={contextMenuProps}
          setContextMenuProps={setContextMenuProps}
        />
      </motion.div>
    </Container>
  );
};
