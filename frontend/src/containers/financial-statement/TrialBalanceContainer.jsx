import React from 'react'
import { useSelector } from "react-redux";
// import { SelectclosingEntries } from "../../redux/data/dataSlice";
// import { ClosingEntriesWidget } from "../../widgets/ClosingEntries/ClosingEntriesWidget";
import { useFinancialStatement } from "./financial-statement-functions";
import { FinancialStatementPage } from "./FinancialStatementPage";

export const TrialBalanceContainer = () => {
  // const closingEntries = useSelector(SelectclosingEntries);

  const { trial_balance_ammount, trial_balance } = useFinancialStatement({});

  const state = trial_balance;

  const props = { state };
  return <>
    {/* <ClosingEntriesWidget /> */}
    <FinancialStatementPage atementPage {...props} />
  </>;
};
