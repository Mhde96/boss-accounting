import _ from "lodash";
import { financial_statement_obg } from "../../constant/account_constant";
import { accountType } from "../accounts/account-type";
import { entryType } from "../entry/journal-entry-type";
import { journalType } from "../journals/journal-type";
import { JournalType } from "../../types/JournalType";
import { en } from "../../helper/languages/en";

import i18n from '../../helper/i18n'

export const configration_accounts = (
  accounts: accountType[]
): accountType[] => {
  let _accounts = accounts.map((account) => ({
    ...account,
    debit: 0,
    credit: 0,
  }));

  return _accounts;
};

export const filter_journals = (journals: JournalType[], condation: -1 | -2 | 0) => {

  let _journals = []
  if (condation == -1 || condation == -2) {
    _journals = journals.filter((journal, index) => {

      return !journal.entries?.some(value => value.accountId == condation)
    })

  } else {
    _journals = journals.filter((journal, index) => {

      return !journal.entries?.some(value => value.accountId < 0)
    })

  }


  return _journals;
}

export const transform_journals_to_entries = (
  journals: JournalType[],
  accounts: accountType[]
): entryType[] => {

  let _entries: Array<entryType> = [];

  let _accounts = configration_accounts(accounts);

  let _journals = journals

  _journals.map((journal: any) => {
    journal.entries?.map((entry: any) => {
      const account = _accounts?.find((item) => {
        return item.id == entry.accountId;
      });
      if (entry.accountId == account?.id) {
        _entries.push({ ...entry, accountName: account?.name });
      }
    });
  });

  return _entries;
};

export const get_total_each_account_from_entries = ({
  accounts,
  entries,
}: {
  accounts: accountType[];
  entries: entryType[];
}) => {
  let _accounts = accounts;
  let _entries = entries;

  _entries.map((entry: any) => {
    _accounts = _accounts.map((account) => {
      if (account.id == entry.accountId) {
        return {
          ...account,
          debit: Number(account.debit) + Number(entry.debit),
          credit: Number(account.credit) + Number(entry.credit),
        };
      } else return { ...account };
    });
  });

  return _accounts;
};

export const filter_each_account_from_entries_with_filter = (
  trial_balance_ammount: Array<accountType>
) => {
  let _accounts = trial_balance_ammount;

  _accounts = _accounts.map((account) => {
    if (account.debit > account.credit) {
      return {
        ...account,
        debit: account.debit - account.credit,
        credit: 0,
      };
    } else {
      return {
        ...account,

        debit: 0,
        credit: account.credit - account.debit,
      };
    }
  });

  return _accounts;
};

// trading

export const get_journals_related_to_final_account = ({
  finalAccountId,
  journals,
}: {
  journals: journalType[];
  finalAccountId: string;
}) => {
  let _journals: journalType[] = [];


  journals.map((journal) => {
    let check = journal.entries?.some(
      (item) => item.accountId == parseInt(finalAccountId)
    );

    if (check) _journals.push(journal);
  });

  return _journals;
};

export const split_accounts_to_credits_and_debits = ({
  accounts,
}: {
  accounts: accountType[];
}) => {
  let debits = accounts.filter((item: any) => item.debit > 0);
  let credits = accounts.filter((item: any) => item.credit > 0);

  return { credits, debits };
};



export const get_result_from_debit_and_credit_accounts = ({
  credits,
  debits,
  type
}: {
  credits: accountType[];
  debits: accountType[];
  type: 'trading' | 'profit'
}) => {
  let _debits: accountType[] = debits;
  let _credits: accountType[] = credits;

  const totalCredit = _.sumBy(credits, (item: accountType) =>
    Number(item.credit)
  );
  const totalDebit = _.sumBy(debits, (item: accountType) => Number(item.debit));

  let result: accountType = {
    name: "",
    debit: 0,
    credit: 0,
    financial_statement: "",
  };



  if (totalCredit > totalDebit) {
    result = {
      name: type == 'profit' ? i18n.t(en.net_profit) : i18n.t(en.gross_profit),
      debit: totalCredit - totalDebit,
      credit: 0,
      financial_statement: "",
    };
    _debits = [...debits, { ...result }];
  } else {
    result = {
      name: type == 'profit' ? i18n.t(en.net_loss) : i18n.t(en.gross_loss),
      credit: totalDebit - totalCredit,
      debit: 0,
      financial_statement: "",
    };
    _credits = [...credits, { ...result }];
  }




  const total = _.sumBy(credits, (item: any) => Number(item.credit));

  return { debits: _debits, credits: _credits, total, result };
};

export const filter_accounts = ({ accounts, finalAccountId }: { accounts: accountType[], finalAccountId: number }) => {

  let _accounts = accounts.filter(account => {
    if (account.id) {
      if (account.id > 0 || account.id == finalAccountId) return true
    }
    else return false
  })

  return _accounts


}


export const get_credit_debit_results = ({ accounts, }: { accounts: accountType[] }) => {
  // تفعيل صافي الحسابات


  let _accounts = accounts.map(account => {
    if (account.debit > account.credit) return { ...account, debit: 0, credit: account.debit - account.credit }
    else return { ...account, debit: account.credit - account.debit, credit: 0 }
  })

  return _accounts
}