import React from 'react'
import { useSelector } from "react-redux";
import { useFinancialStatement } from "./financial-statement-functions";
import { FinancialStatementPage } from "./FinancialStatementPage";
// import { SelectclosingEntries } from "../../redux/data/dataSlice";
// import { ClosingEntriesWidget } from "../../widgets/ClosingEntries/ClosingEntriesWidget";
import { useAppDispatch } from "../../data/redux/hooks";
import { useEffect } from "react";
import {
  fetchAccountsAsync,
  fetchJournalsAsync,
} from "../../data/redux/data/dataAsync";
export const TradingAccountContainer = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchAccountsAsync());
    dispatch(fetchJournalsAsync());
  }, []);

  // const closingEntries = useSelector(SelectclosingEntries);

  // @ts-ignore
  const state = useFinancialStatement({}).trading_account;

  const props = { state };
  return (
    <>
      {/* <ClosingEntriesWidget /> */}
      <FinancialStatementPage {...props} />
    </>
  );
};
