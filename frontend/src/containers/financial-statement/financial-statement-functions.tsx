import React from 'react'
import _ from "lodash";
import { useSelector } from "react-redux";
// import { useDbFetchAccounts } from "../../db/data/accountsDb";
// import { useFetchJournalsIndexedDb } from "../../db/data/journalsDb";
import { selectAccounts, selectJournals } from "../../data/redux/data/dataSlice";
import { accountType } from "../accounts/account-type";
import { entryType } from "../entry/journal-entry-type";
import { journalType } from "../journals/journal-type";
import { JournalType } from "../../types/JournalType";
import {
  configration_accounts,
  get_total_each_account_from_entries,
  filter_each_account_from_entries_with_filter,
  transform_journals_to_entries,
  get_journals_related_to_final_account,
  split_accounts_to_credits_and_debits,
  get_result_from_debit_and_credit_accounts,
  filter_journals,
  filter_accounts,
  get_credit_debit_results,
} from "./helper";
import { financial_statement_obg } from "../../constant/account_constant";

const trial_balance_ammount = (
  accounts: Array<accountType>,
  journals: Array<JournalType>
) => {
  let _accounts: accountType[] = configration_accounts(accounts);
  let _journals = filter_journals(journals, 0);
  const _entries = transform_journals_to_entries(_journals, _accounts);

  _accounts = get_total_each_account_from_entries({
    accounts: _accounts,
    entries: _entries,
  });

  return _accounts;
};

const trial_balance = (trial_balance_ammount: Array<accountType>) => {
  let _accounts = filter_each_account_from_entries_with_filter(
    trial_balance_ammount
  );

  let debits = _accounts.filter((item: any) => item.debit > 0);
  let credits = _accounts.filter((item: any) => item.credit > 0);

  const total = _.sumBy(credits, (item: any) => Number(item.credit));

  return { _accounts, credits, debits, total };
};

// ==================================================================================
// ==================================================================================
// ==================================================================================

const trading = ({ journals, accounts }: any) => {
  let _journals = get_journals_related_to_final_account({
    finalAccountId: financial_statement_obg.trading.value,
    journals,
  });
  // @ts-ignore
  let _entries = transform_journals_to_entries(_journals, accounts);

  let _accounts: accountType[] = configration_accounts(accounts);
  _accounts = get_total_each_account_from_entries({
    entries: _entries,
    accounts: _accounts,
  });

  _accounts = filter_accounts({
    accounts: _accounts,
    finalAccountId: parseInt(financial_statement_obg.profit_loss.value),
  });

  _accounts = get_credit_debit_results({ accounts: _accounts });

  let { debits, credits } = split_accounts_to_credits_and_debits({
    accounts: _accounts,
  });

  let result = get_result_from_debit_and_credit_accounts({ credits, debits , type:'trading' });
  return { ...result };
};

// const trading_account = (_trial_balance: any) => {
//   const financial_statement = 2;

//   let debits = _trial_balance.debits.filter(
//     (item: any) =>
//       item.debit > 0 && item.financial_statement == financial_statement
//   );

//   let credits = _trial_balance.credits.filter(
//     (item: any) =>
//       item.credit > 0 && item.financial_statement == financial_statement
//   );

//   const totalCredit = _.sumBy(credits, (item: any) => Number(item.credit));
//   const totalDebit = _.sumBy(debits, (item: any) => Number(item.debit));

//   let result = {
//     name: "",
//     debit: 0,
//     credit: 0,
//   };

//   if (totalCredit > totalDebit) {
//     result = {
//       name: "الربح",
//       debit: totalCredit - totalDebit,
//       credit: 0,
//     };
//     debits = [...debits, { ...result }];
//   } else {
//     result = {
//       name: "خسارة",
//       credit: totalDebit - totalCredit,
//       debit: 0,
//     };
//     credits = [...credits, { ...result }];
//   }

//   const total = _.sumBy(credits, (item: any) => Number(item.credit));

//   return { debits, credits, total, result };
// };

// ==================================================================================
// ==================================================================================
// ==================================================================================

const profit = ({ journals, accounts }: any) => {
  let _journals = get_journals_related_to_final_account({
    finalAccountId: financial_statement_obg.profit_loss.value,
    journals,
  });

  // @ts-ignore
  let _entries = transform_journals_to_entries(_journals, accounts);

  let _accounts: accountType[] = configration_accounts(accounts);

  _accounts = filter_accounts({
    accounts: _accounts,
    finalAccountId: parseInt(financial_statement_obg.trading.value),
  });

  _accounts = get_total_each_account_from_entries({
    entries: _entries,
    accounts: _accounts,
  });
  _accounts = get_credit_debit_results({ accounts: _accounts });

  let { debits, credits } = split_accounts_to_credits_and_debits({
    accounts: _accounts,
  });

  let result = get_result_from_debit_and_credit_accounts({ credits, debits, type:'profit' });

  return { ...result };
};

export const profit_account = (_trial_balance: any, trading_account: any) => {
  const financial_statement = 1;

  let debits = _trial_balance.debits.filter(
    (item: any) =>
      item.debit > 0 && item.financial_statement == financial_statement
  );

  let credits = _trial_balance.credits.filter(
    (item: any) =>
      item.credit > 0 && item.financial_statement == financial_statement
  );

  //
  if (trading_account.result.debit > trading_account.result.credit) {
    credits = [
      ...credits,
      {
        name: "ربح مجمل",
        debit: 0,

        credit: trading_account.result.debit,
      },
    ];
  } else {
    debits = [
      ...debits,
      {
        name: "ربح مجمل",
        debit:
          Number(trading_account.result.credit) -
          Number(trading_account.result.debit),
        credit: 0,
      },
    ];
  }

  const totalCredit = _.sumBy(credits, (item: any) => Number(item.credit));
  const totalDebit = _.sumBy(debits, (item: any) => Number(item.debit));

  let result = {
    name: "",
    debit: 0,
    credit: 0,
  };

  if (totalCredit > totalDebit) {
    result = {
      name: "الربح",
      debit: totalCredit - totalDebit,
      credit: 0,
    };
    debits = [...debits, { ...result }];
  } else {
    result = {
      name: "خسارة",
      credit: totalDebit - totalCredit,
      debit: 0,
    };
    credits = [...credits, { ...result }];
  }

  const total = _.sumBy(credits, (item: any) => Number(item.credit));

  return { debits, credits, total, result };
};

// ==================================================================================
// ==================================================================================
// ==================================================================================

const balance_sheet = (_trial_balance: any, _profit_account: any) => {
  const financial_statement = 0;

  let debits = _trial_balance.debits.filter(
    (item: any) =>
      item.debit > 0 && item.financial_statement == financial_statement
  );

  let credits = _trial_balance.credits.filter(
    (item: any) =>
      item.credit > 0 && item.financial_statement == financial_statement
  );

  if (_profit_account.result.debit > _profit_account.result.credit) {
    credits = [
      ...credits,
      {
        name: "ربح صافي",
        debit: 0,

        credit: _profit_account.result.debit,
      },
    ];
  } else {
    debits = [
      ...debits,
      {
        name: "ربح صافي",
        debit:
          Number(_profit_account.result.credit) -
          Number(_profit_account.result.debit),
        credit: 0,
      },
    ];
  }

  const total = _.sumBy(credits, (item: any) => Number(item.credit));

  return { debits, credits, total };
};

const __balance_sheet = ({
  journals,
  accounts,
}: {
  journals: JournalType[];
  accounts: accountType[];
}) => {
  let _accounts: accountType[] = configration_accounts(accounts);
  const _entries = transform_journals_to_entries(journals, _accounts);

  _accounts = get_total_each_account_from_entries({
    accounts: _accounts,
    entries: _entries,
  });

  _accounts = _accounts.filter(
    (account) => parseInt(account.financial_statement) == 0
  );

  _accounts = _accounts.map((account) => {
    if (account.debit > account.credit)
      return { ...account, debit: account.debit - account.credit, credit: 0 };
    else
      return { ...account, debit: 0, credit: account.credit - account.debit };
  });

  let { debits, credits } = split_accounts_to_credits_and_debits({
    accounts: _accounts,
  });

  const total = _.sumBy(credits, (item: any) => Number(item.credit));

  return { debits, credits, total };
};

type FinancialStatement = {
  // closingEntries: any;
};

export const useFinancialStatement = () => {
  const accounts = useSelector(selectAccounts);
  const journals: any = useSelector(selectJournals);

  // const _journals: any = selectedJournals(journals, props?.closingEntries);

  const _trial_balance_ammount = trial_balance_ammount(accounts, journals);
  const _trial_balance = trial_balance(_trial_balance_ammount);

  const _trading_account = trading({ journals, accounts });
  const _profit_account = profit({ journals, accounts });
  const _balance_sheet = __balance_sheet({ journals, accounts });

  return {
    trial_balance_ammount: _trial_balance_ammount,
    trial_balance: _trial_balance,
    profit_account: _profit_account,
    trading_account: _trading_account,
    balance_sheet: _balance_sheet,
  };
};
