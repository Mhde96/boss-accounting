import React from 'react'
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  // SelectclosingEntries,
  selectAccounts,
  selectJournals,
} from "../../data/redux/data/dataSlice";
import { useFinancialStatement } from "./financial-statement-functions";
import { FinancialStatementPage } from "./FinancialStatementPage";
// import { ClosingEntriesWidget } from "../../widgets/ClosingEntries/ClosingEntriesWidget";
import { useAppDispatch } from "../../data/redux/hooks";
import { fetchAccountsAsync, fetchJournalsAsync } from "../../data/redux/data/dataAsync";

export const BalanceSheetContainer = () => {
  // const closingEntries = useSelector(SelectclosingEntries);

  const dispatch = useAppDispatch();

  
  useEffect(() => {
    dispatch(fetchAccountsAsync());
    dispatch(fetchJournalsAsync());

  }, []);


  const state = useFinancialStatement().balance_sheet;
  const props = { state };
  return (
    <>
      {/* <ClosingEntriesWidget /> */}
      <FinancialStatementPage {...props} />
    </>
  );
};
