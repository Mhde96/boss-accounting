import React, { useMemo } from "react";

import { useFormik } from "formik";
import { LoginState } from "./login-type";
import { LoginPage } from "./LoginPage";
import * as yup from "yup";
import { useAppDispatch } from "../../data/redux/hooks";
import { loginAsync } from "../../data/redux/app/appAsync";
import { useNavigate } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { useSelector } from "react-redux";
import { selectLanguage } from "../../data/redux/app/appSlice";

export const LoginContainer = () => {
  const { t } = useTranslation();
  const language = useSelector(selectLanguage);
  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        email: yup
          .string()
          .required(t(en.email_is_required))
          .email(t(en.email_must_be_valid)),
        password: yup
          .string()
          .required(t(en.password_is_required))
          .min(4, t(en.password_must_be_at_least_characters_long)),
      }),
    [language]
  );

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { values, handleChange, errors, handleSubmit } = useFormik({
    validationSchema,
    initialValues: LoginState,
    onSubmit: (values) => {
      dispatch(loginAsync(values, navigate));
    },
    validateOnChange: false,
    validateOnBlur: false,
    validateOnMount: false,
  });

  const handleRegister = () => navigate(endroutes.register);
  const handleForgotPassword = () => navigate(endroutes.forgetpassword);
  const props = {
    values,
    handleChange,
    errors,
    handleSubmit,
    handleRegister,
    handleForgotPassword,
  };
  return <LoginPage {...props} />;
};
