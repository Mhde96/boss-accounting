import React from "react";

import { Button } from "../../components/button/Button";
import { Input } from "../../components/input/Input";
import { Text } from "../../components/text/Text";
import { LoginPagePropsType } from "./login-type";
import "./login-page-style.scss";
import { motion } from "framer-motion";
import { PageTransitionProps } from "../../components/animations/AnimationPageProps";
import { Break } from "../../components/Break";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
// import { useColors } from "../../styles/variables-styles";
export const LoginPage = ({
  handleChange,
  values,
  errors,
  handleSubmit,
  handleRegister,
  handleForgotPassword,
}: LoginPagePropsType) => {
  const { t } = useTranslation();

  // const colors = useColors()
  return (
    <motion.div {...PageTransitionProps}>
      <Break />
      <Text bold center fs={"f2"}>
        Boss Accounting
      </Text>
      <Text bold center fs={"f2"}>
        Hello Again!
      </Text>

      <Break />

      <Input
        placeholder={t(en.email)}
        onChange={handleChange("email")}
        value={values.email}
        error={errors?.email}
      />
      <Break />
      <Input
        placeholder={t(en.password)}
        onChange={handleChange("password")}
        value={values.password}
        error={errors?.password}
        typeOfInput="password"
      />
      {/* <Text color={colors.link} textAlign="right" onClick={handleForgotPassword}>forgot password</Text> */}
      <Break />
      <Button onClick={handleSubmit}>{t(en.login)}</Button>

      <Break />
      <Button onClick={handleRegister}>{t(en.register)}</Button>
    </motion.div>
  );
};
