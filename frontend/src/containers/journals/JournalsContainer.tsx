import React from "react";

import { useEffect, useState, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { ConfirmationDeleteDialog } from "../../components/dialogs/ConfirmationDeleteDialog";
import { endroutes } from "../../constant/endroutes";

import {
  deleteJournalAsync,
  fetchAccountsAsync,
  fetchJournalsAsync,
} from "../../data/redux/data/dataAsync";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { journalType } from "./journal-type";
import { JournalsPage } from "./JournalsPage";
import {
  selectJournals,
  selectLoaderJournals,
} from "../../data/redux/data/dataSlice";
import { Filter } from "../../components/filter/Filter";
import moment from "moment";
import { JOURNAL_SORT_OPTIONS } from "../../types/JournalType";
import { Container } from "react-bootstrap";
import "./journals-page-styles.scss";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";

const columns = [
  { id: "description", name: "description" },
  { id: "date", name: "date" },
];

export const JournalsContainer = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  let journals = useAppSelector(selectJournals);
  const loaderJournals = useAppSelector(selectLoaderJournals);
  const {t} = useTranslation()

  const [sort, setSort] = useState("byNumberReverse");

  // @ts-ignore
  function compare(a, b) {
    switch (sort) {
      case JOURNAL_SORT_OPTIONS.BY_NUMBER_REVERSE:
        return b.number - a.number;
      case JOURNAL_SORT_OPTIONS.BY_NUMBER:
        return a.number - b.number;
      case JOURNAL_SORT_OPTIONS.OLDER:
        return moment(a.date).valueOf() - moment(b.date).valueOf();
      case JOURNAL_SORT_OPTIONS.NEWER:
        return moment(b.date).valueOf() - moment(a.date).valueOf();
      default:
        return 0;
    }
  }

  const filteredJournalsMemo = useMemo(() => {
    let filteredJournals = new Array(...journals);
    return filteredJournals.sort(compare);
  }, [sort, journals]);

  const DeleteJournalAsync = (journal: journalType, isDelete: boolean) => {
    if (isDelete && journal.id) {
      // deleteJournalIndexedDb(journal)
      dispatch(deleteJournalAsync(journal));
    } else {
      setShowConfirmationDialog({
        show: true,
        data: journal,
        title: t(en.delete_journal),
        body: t(en.confirm_delete_journal) + " " + journal?.number,
      });
    }
  };
  const handleNavigateToEntries = (number: number) =>
    navigate(endroutes.journalentaries(number).go);

  useEffect(() => {
    dispatch(fetchJournalsAsync());
    dispatch(fetchAccountsAsync());
  }, []);

  useEffect(() => {
    if (loaderJournals == false && journals.length == 0) {
      navigateToNewEntry();
    }
  }, [journals.length, loaderJournals]);

  const navigateToNewEntry = () =>
    navigate(endroutes.journalentaries().newJournal);

  const [showConfirmationDialog, setShowConfirmationDialog] = useState<any>({
    show: false,
    data: {},
    title: "",
    body: "",
  });

  const props = {
    journals: filteredJournalsMemo,
    columns,
    handleNavigateToEntries,
    DeleteJournalAsync,
    navigateToNewEntry,
  };

  // return <EmptyData />;
  return (
    <Container id="journals-page-styles">
      <ConfirmationDeleteDialog
        data={showConfirmationDialog}
        setData={setShowConfirmationDialog}
        handleSubmit={() =>
          DeleteJournalAsync(showConfirmationDialog.data, true)
        }
      />

      <Filter
        onChange={
          // @ts-ignore
          (state) => setSort(state)
        }
      />
      <JournalsPage {...props} />
    </Container>
  );
};
