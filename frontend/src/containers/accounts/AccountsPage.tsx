import React from "react";
import { Container } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";
import { AccountCard } from "../../components/cards/account/AccountCard";
import { AccountsPropsType } from "./account-type";
import { OpenAccountDialog } from "./AccountDialog";
import "./account-page-styles.scss";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { Button } from "../../components/button/Button";
export const AccountsPage = (props: AccountsPropsType) => {
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();
  return (
    <Container>
      <div id="account-page-styles">
        <Button
          onClick={() => {
            OpenAccountDialog(location, navigate, undefined);
          }}
        >
          {t(en.add)}
        </Button>
        <hr />

        <AccountCard name={t(en.account)} />
        {props.accounts?.map((item: any, index) => (
          <div key={index}>
            <AccountCard
              id={item.id}
              isHeader
              financial_statement={item.financial_statement}
              name={item.name}
              account_key={1}
              index={index}
              operations={{
                open: () => props.handleNavigateAccount(item.id),
                update: () => OpenAccountDialog(location, navigate, item),
                delete: () => {
                  props.DeleteAccountAsync(item);
                },
              }}
            />
          </div>
        ))}
      </div>
    </Container>
  );
};
