import { Card, ListGroup } from "react-bootstrap";
import React from "react";
import "../../styles/components/bootstrap/ListGroup.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { PopUp } from "../../components/popUp/PopUp";
import {
  addFinicialAsync,
  deleteFinacialCycles,
} from "../../data/redux/data/dataAsync";
import { useAppDispatch } from "../../data/redux/hooks";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
import { motion } from "framer-motion";
import { ConfirmationDeleteDialog } from "../../components/dialogs/ConfirmationDeleteDialog";
import { useColors } from "../../styles/variables-styles";
import { useFormik } from "formik";
import { dateFormatUi } from "../../utils/date-format";
import { Button } from "../../components/button/Button";

import { ExportPopup } from "../../widgets/export-import-dialog/ExportPopup";
import { ImportPopup } from "../../widgets/export-import-dialog/ImportPopup";
export const FinancialCycle = (props) => {
  const {
    currentFinancialCycleId,
    handleChangeCurrentFinancialCycleId,
    AllfinacialCycles,
  } = props;

  const dispatch = useAppDispatch();
  const colors = useColors();
  const { t } = useTranslation();

  const [idItem, setIdItem] = React.useState();
  const [showModal, setShowModal] = React.useState(false);
  const [showExportModal, setShowExportModal] = React.useState(false);
  const [showImportModal, setShowImportModal] = React.useState(false);
  const [showConfirmationDialog, setShowConfirmationDialog] = React.useState({
    show: false,
    data: {},
    title: "",
    body: "",
  });

  const AnimatedListGroupItem = motion(ListGroup.Item);

  const { values, handleSubmit, handleChange, errors } = useFormik({
    initialValues: {
      start_at: new Date().toISOString().substring(0, 10),
    },

    onSubmit: (values) => {
      const body = {
        start_at: values.start_at,
      };
      dispatch(addFinicialAsync(body));
      handleCloseModal();
    },
  });

  const handleShowModal = () => setShowModal(true);

  const handleCloseModal = () => setShowModal(false);
  const handleOpenRemovePopUp = (id) => {
    setShowConfirmationDialog({
      show: true,
      data: {},
      title: t(en.title_delete_financial_cycle),
      body: t(en.body_confirm_delete_financial_cycle),
    });
    setIdItem(id);
  };

  const handleRemove = () => {
    const data = {
      id: idItem,
    };
    dispatch(deleteFinacialCycles(data));
  };

  return (
    <div>
      <Card id={"db-control-page-styles"} className="primary-border ">
        <Card.Header className="header" as="h6">
          {t(en.chooseYourFianacialCycles)}
        </Card.Header>

        <Card.Body className="d-flex flex-column">
          {AllfinacialCycles?.length === 0 && (
            <AnimatedListGroupItem
              className="list-group-item mt-2 d-flex align-items-center justify-content-center rounded"
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
            >
             {t(en.financial_cycle_no_data)}
            </AnimatedListGroupItem>
          )}
          {AllfinacialCycles?.map((item, index) => (
            <motion.div
              onClick={() => handleChangeCurrentFinancialCycleId(item.id)}
              className={`list-group-item d-flex align-items-center justify-content-between  rounded`}
              key={index}
              initial={{ opacity: 0, y: -10 }}
              animate={{
                colors: colors.text,
                opacity: 1,
                y: 0,
                borderColor:
                  currentFinancialCycleId == item.id
                    ? colors.primary
                    : colors.border,
              }}
              exit={{ opacity: 0, y: -10 }}
              whileHover={{
                scale: 1.01, // Slightly scale up on hover
                color: colors.primary, // Change text color on hover
                borderColor: colors.primary,
              }}
            >
              <span>
                <span style={{ width: 20, display: "inline-block" }}>
                  {index + 1}.
                </span>
                <span> {dateFormatUi({date:item?.start_at,})}</span>
              </span>
              <FontAwesomeIcon
                icon={faTrash}
                className="delete-icon flex al"
                onClick={(e) => {
                  handleOpenRemovePopUp(item?.id);
                  // e.stopPropagation();
                  // Implement your delete logic here using item.id
                }}
              />
            </motion.div>
          ))}

          <div className="d-flex gap-2 " dir={"rtl"}>
            <Button
              variant="primary"
              className="align-self-end w- mt-3"
              style={{ width: "131px", height: "41px" }}
              onClick={handleShowModal}
            >
              {t(en.add)}
            </Button>
            <Button
              variant="primary"
              className="align-self-end w- mt-3"
              style={{ width: "131px", height: "41px" }}
              onClick={() => setShowImportModal(true)}
            >
              {t(en.import)}
            </Button>

            <Button
              variant="primary"
              className="align-self-end w- mt-3"
              style={{ width: "131px", height: "41px" }}
              onClick={() => setShowExportModal(true)}
            >
              {t(en.export)}
            </Button>
          </div>
        </Card.Body>
      </Card>

      <ExportPopup
        show={showExportModal}
        onHide={() => setShowExportModal(false)}
      />

      <ImportPopup
        show={showImportModal}
        onHide={() => setShowImportModal(false)}
      />

      <ConfirmationDeleteDialog
        data={showConfirmationDialog}
        setData={setShowConfirmationDialog}
        handleSubmit={handleRemove}
      />

      <PopUp
        show={showModal}
        onHide={handleCloseModal}
        title={t(en.addFinancialCycle)}
        onSubmit={handleSubmit}
      >
        <div>
          <label style={{ fontWeight: "bold", marginRight: 5 }}>
            {t(en.startDate)} :{" "}
          </label>
          <input
            type="date"
            value={values.start_at}
            onChange={handleChange("start_at")}
          />
        </div>
      </PopUp>
    </div>
  );
};
