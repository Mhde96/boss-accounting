import React from "react";
import { useAppDispatch, useAppSelector } from "../../data/redux/hooks";
import { FinancialCycle } from "./FinancialCycle";
import { selectCurrentFinancialCycleId } from "../../data/redux/app/appSlice";
import { changeCurrentFinancialCycleIdAsync } from "../../data/redux/app/appAsync";
import { fetchCompaniesAsync, fetchFinicialAsync } from "../../data/redux/data/dataAsync";
import { SelectCompany, SelectFinacialCycles } from "../../data/redux/data/dataSlice";
import { useSelector } from "react-redux";
export const FinancialCycleContainer = ({ map_id, maps }) => {
  const currentFinancialCycleId = useSelector(selectCurrentFinancialCycleId);

  const dispatch = useAppDispatch();

  const handleChangeCurrentFinancialCycleId = React.useCallback((value) =>
    dispatch(changeCurrentFinancialCycleIdAsync(value))
  );

  const AllfinacialCycles = useAppSelector(SelectFinacialCycles);

  React.useEffect(() => {
    dispatch(fetchFinicialAsync());
  }, []);

  
  
  const props = {
    // financialcycles,
    currentFinancialCycleId,
    handleChangeCurrentFinancialCycleId,
    AllfinacialCycles: AllfinacialCycles.data,
    map_id,
  };

  return <FinancialCycle {...props} />;
};
