import React, { useMemo } from "react";

import { useFormik } from "formik";
import { SignupState } from "./signup-type";
import { SignupPage } from "./SignupPage";
import * as yup from "yup";
import { registerAsync } from "../../data/redux/app/appAsync";
import { useAppDispatch } from "../../data/redux/hooks";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { selectLanguage } from "../../data/redux/app/appSlice";
import { en } from "../../helper/languages/en";

export const SignupContainer = () => {
  const { t } = useTranslation();
  const language = useSelector(selectLanguage);

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        name: yup.string().required(t(en.name_is_required)),
        email: yup
          .string()
          .required(t(en.email_is_required))
          .email(t(en.email_must_be_valid)),
        // phone: yup.string().required(),
        // countryCode: yup.string().required(),
        occupation: yup.string().required(t(en.occupation_is_required)),
        password: yup
          .string()
          .required(t(en.password_is_required))
          .min(4, t(en.password_must_be_at_least_characters_long)),
        confirm_password: yup
          .string()
          .required(t(en.password_is_required))
          .min(4, t(en.password_must_be_at_least_characters_long))
          .oneOf([yup.ref("password"), null], t(en.passwords_must_match)),
      }),
    [language]
  );

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);

  const { values, handleChange, errors, handleSubmit } = useFormik({
    validationSchema,
    initialValues: SignupState,
    onSubmit: (values) => {
      // @ts-ignore
      dispatch(registerAsync(values, navigate, setLoading));
    },
    validateOnChange: false,
    validateOnBlur: false,
    validateOnMount: false,
  });

  const props = { values, handleChange, errors, handleSubmit, loading };
  return <SignupPage {...props} />;
};
