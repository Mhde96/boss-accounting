
type SignupPagePropsType = {
  handleChange: any;
  values: SignupStateType;
  errors: any;
  handleSubmit: any;
  loading: boolean
};

type SignupStateType = {
  name: string;
  email: string;
  phone: string;
  countryCode: string;
  password: string;
  confirm_password: string;
  occupation?: number;
};
export const SignupState: SignupStateType = {
  name: "",
  email: "",
  phone: '',
  countryCode: '',
  password: "",
  confirm_password: "",
  // occupation: null,
};

export type { SignupPagePropsType, SignupStateType };
