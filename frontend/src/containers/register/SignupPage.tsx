import React from "react";

import { Button } from "../../components/button/Button";
import { Input } from "../../components/input/Input";
import { Text } from "../../components/text/Text";
import { SignupPagePropsType } from "./signup-type";
import "./signup-page-style.scss";
import { PageTransitionProps } from "../../components/animations/AnimationPageProps";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import { Break } from "../../components/Break";
import { useTranslation } from "react-i18next";
import { en } from "../../helper/languages/en";
export const SignupPage = ({
  handleChange,
  values,
  errors,
  handleSubmit,
  loading,
}: SignupPagePropsType) => {
  const navigate = useNavigate();
  const handleNavigateLogin = () => navigate(endroutes.login);
  const { t } = useTranslation();

  return (
    <motion.div {...PageTransitionProps} id="signup-page-style">
      <Text fs={"f2"} bold center>
        Boss Accounting
      </Text>
      <Text fs={"f2"} center>
        {t(en.register)}
      </Text>

      <Break />

      <Input
        placeholder={t(en.name)}
        onChange={handleChange("name")}
        value={values.name}
        error={errors?.name}
      />

      <Break />

      <Input
        placeholder={t(en.email)}
        onChange={handleChange("email")}
        value={values.email}
        error={errors?.email}
      />
      {/* <br /> */}

      {/* <div style={{ display: "flex", gap: 5 }}>
        <Input
          typeOfInput="countryCode"
          placeholder="Country Code"
          onChange={handleChange("countryCode")}
          value={values.countryCode}
          error={errors?.countryCode}
        />
        <Input
          placeholder="Phone"
          onChange={handleChange("phone")}
          value={values.phone}
          error={errors?.phone}
        />
      </div> */}

      <Break />
      <Input
        typeOfInput="SelectOccupations"
        placeholder={t(en.occupations)}
        onChange={handleChange("occupation")}
        value={values.occupation}
        error={errors?.occupation}
      />
      <Break />
      <Input
        placeholder={t(en.password)}
        onChange={handleChange("password")}
        value={values.password}
        error={errors?.password}
        typeOfInput="password"
      />

      <Break />

      <Input
        placeholder={t(en.confirm_password)}
        onChange={handleChange("confirm_password")}
        value={values.confirm_password}
        error={errors?.confirm_password}
        typeOfInput="password"
      />
      <Break />

      <Button loading={loading} onClick={handleSubmit}>
        {loading ? `${t(en.loading)}...` : t(en.sign_up)}
      </Button>
      <Break />
      <Button onClick={handleNavigateLogin}>{t(en.login)}</Button>
    </motion.div>
  );
};
