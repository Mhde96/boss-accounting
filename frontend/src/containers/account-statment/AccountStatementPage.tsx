import React from "react";

import DataGrid from "react-data-grid";
import {
  AccountStatementPagePropsType,
  account_table_columns,
} from "./account-statement-type";
import Select from "react-select";
import { Break } from "../../components/Break";
import "./account-statement-page-styles.scss";
import { PageTransitionProps } from "../../components/animations/AnimationPageProps";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import { endroutes } from "../../constant/endroutes";
import { useColorMemo } from "../../hook/useColorMemo";
import { accountType } from "../accounts/account-type";
import { Container } from "react-bootstrap";
export const AccountStatmentPage = (props: AccountStatementPagePropsType) => {
  const { selectedAccount } = props;
  const colorMode = useColorMemo();
  const navigate = useNavigate();
  const accounts = props.accounts.map((item: accountType) => ({
    value: item.id,
    label: item.name,
  }));

  return (
    <Container >
      <motion.div {...PageTransitionProps} id="account-statement-page-styles">
        <Select
          options={accounts}
          onChange={(account: any) => {
            props.handleGetAccountData(account.value);
          }}
          // inputValue={props?.account?.key}
          value={selectedAccount}
          defaultValue={selectedAccount}
          // defaultInputValue={props?.account?.key}
        />
        <Break />
        <DataGrid
          className={`rdg-${colorMode} fill-grid data-grid`}
          // components={{ rowRenderer }}

          // @ts-ignore
          onRowClick={(d: any) => {
            if (d.number) navigate(endroutes.journalentaries(d.number).go);
          }}
          columns={account_table_columns}
          summaryRows={props.summaryRows}
          rows={props?.entries}
          // onRowsChange={props.onRowsChange}
        />
      </motion.div>
    </Container>
  );
};
