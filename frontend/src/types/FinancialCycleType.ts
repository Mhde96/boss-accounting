export type FinancialCycleType = {
    id?: number;
    start_at: string;
}