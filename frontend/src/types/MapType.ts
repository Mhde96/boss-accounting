import { FinancialCycleType } from "./FinancialCycleType";

export type MapType = {
  id?: number;
  name: string;
  description: string;
  financialcycles: FinancialCycleType[]
};
