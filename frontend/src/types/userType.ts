import { MapType } from "./MapType";

export type userType = {
  id?: number;
  name: string;
  email: string;
  last_sync?: string;
  role: number | undefined;
  currentMap?: MapType;
};
