
export type CompanyType = {
    id?: number;
    name: string;
    language: string;
    color_mode: string;
};

export type CompaniesType = {
    data: Array<CompanyType>
    loading: boolean
}