export type ContextButtonsType = Array<{
    onClick: () => void,
    title: string;
}>
