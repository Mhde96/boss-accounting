import { EntryType } from "./EntryType";

export type JournalType = {
    id?: number;
    description: string;
    date: any;

    entries?: Array<EntryType>;
    number: number | string;

    closingEntries: boolean
}


export const JOURNAL_SORT_OPTIONS = {
    BY_NUMBER_REVERSE: "byNumberReverse",
    BY_NUMBER: "byNumber",
    OLDER: "Older",
    NEWER: "Newer",
};


export type JOURNAL_SORT_OPTIONS_TYPES = String | "byNumber" | "byNumberReverse" | "News" | "Older"
