// http://localhost:3001/admin/users?page=1&limit=10&searchTerm=&sortOrder=asc&sortField=name

export type getAdminUsersApiType = {
  serviceName: string;

  currentPage: string | number;
  pageSize: string | number;
  debounceSearchTerm: string | number;
  sortOrder: string | number;
  sortedColumn: string | number | null;
};

export const getAdminUsersApi = ({
  serviceName,
  currentPage,
  pageSize,
  debounceSearchTerm,
  sortOrder,
  sortedColumn,
}: getAdminUsersApiType) => {
  const endPointAdminUsers = `/admin/${serviceName}?page=${currentPage}&limit=${pageSize}&searchTerm=${debounceSearchTerm}&sortOrder=${sortOrder}&sortField=${sortedColumn}`;
  return endPointAdminUsers;
};
