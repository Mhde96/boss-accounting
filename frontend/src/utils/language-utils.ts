
import { Cookies } from "react-cookie";
import { cookiesKey } from "../data/cookies/cookiesKey";
import i18n from "../helper/i18n";

const cookies = new Cookies();

const getLanguage = () => {
    let language = cookies.get(cookiesKey.language);
    if (!language) {
      i18n.changeLanguage("en");
      return "en";
    } else {
      i18n.changeLanguage(language);
      return language;
    }
  };
  
  
  const getAppDirection = () => {
    let language = cookies.get(cookiesKey.language);
    if (!language) {
  
      return "ltr";
    } else {
  
      return i18n.dir();
    }
  };
  


  export {getLanguage , getAppDirection}
  