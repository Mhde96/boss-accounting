import moment from "moment";

type datePickerFormatPorps = {
  date: string | Date
  formatType: '/' | '-' | undefined
} | undefined

export const datePickerFormat = "yyyy/MM/dd";
export const dateFormatUi = ( props: datePickerFormatPorps) => {
  const { formatType , date } = props
  let format = ''
  if (formatType == '-')
    format = 'YYYY-MM-DD'

  else format = "YYYY/MM/DD"

  if (moment(date).isValid()) return moment(date).format(format);
  return date.toString();
};

