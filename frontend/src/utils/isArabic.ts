export function isArabic(text: string) {
  var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
  let result = pattern.test(text);
  return result;
}
