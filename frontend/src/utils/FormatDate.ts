export const FormatDate = (selectedDate: any) => {
  const formattedDate = `${selectedDate?.getDate()}-${
    selectedDate?.getMonth() + 1
  }-${selectedDate?.getFullYear()}`;
  return formattedDate;
};
