export const getColorMode = () => {
  const isDark = window.matchMedia("(prefers-color-scheme: dark)").matches;

  if (isDark) return "dark";
  else return "light";
};
