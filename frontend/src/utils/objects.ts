type PropertyNameObject<T> = {
    [K in keyof T]: K;
  };
  
 export function createPropertyNameObject<T extends object>(obj: T): PropertyNameObject<T> {
    return Object.keys(obj).reduce((acc, key) => {
      acc[key as keyof T] = key as keyof T;
      return acc;
    }, {} as PropertyNameObject<T>);
  }
  