export function isEmpty(input: string) {
  // Return true if the input is a string that is empty, contains only whitespace, or is neither a string nor a number
  return (
    (typeof input === 'string' && input.trim().length === 0) || // Empty string or string with only whitespace
    (input === null || input === undefined) // null or undefined
  );
}

// // Example Usage function isEmpty(input)
// console.log(isEmpty(""));           // true (empty string)
// console.log(isEmpty("   "));        // true (only whitespace)
// console.log(isEmpty(42));           // false (number)
// console.log(isEmpty("hello"));      // false (non-empty string)
// console.log(isEmpty("  text  "));   // false (valid string with whitespace)
// console.log(isEmpty(null));         // true (not a string or number)
// console.log(isEmpty(undefined));    // true (not a string or number)


