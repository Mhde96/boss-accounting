export function timeFormat(time: Date) {
    const now: Date = time
    let hours = now.getHours();
    let minutes = now.getMinutes();

    if (hours < 10) hours = 0 + hours;
    if (minutes < 10) minutes = 0 + minutes;

    return `${hours}-${minutes}`;
}
