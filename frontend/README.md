# Getting Started with Frontend App

### Run desktop application with electron
```bash
npm run dev
npm run electron
```
### Build desktop application with electron
```bash
npm run electron-package
```



### Break Points
```bash
isPhone =  max-width:575px;
isTablet =  min-width:576px;
isLaptop =  min-width:992px;
```