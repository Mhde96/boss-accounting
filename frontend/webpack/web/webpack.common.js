const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");


module.exports = {
  entry: path.resolve(__dirname, "../../", "src/global/web/index.tsx"),
  plugins: [

    new HtmlWebpackPlugin({
      template: path.join(__dirname, "../../", "public", "index.html"),
      title: "Caching",


    }),

  ],

  module: {

    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
              "@babel/preset-typescript",
            ],
          },
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader",
          {
            loader: "sass-loader",
            options: {
              sassOptions: {
                quiet: true,
                quietDeps: true,
                silenceDeprecations: ['import'],
              },
            },
          }
        ],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".tsx", ".ts", ".scss", ".css"],
  },
};

// https://medium.com/@april9288/how-i-got-a-100-lighthouse-score-with-my-react-app-2b676ef62113
