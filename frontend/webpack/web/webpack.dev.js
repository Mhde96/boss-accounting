// webpack.dev.js: Webpack configuration only used by development mode.
const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");



const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = {
  mode: "development",
  devtool: "eval-source-map",

//   plugins: [new CopyPlugin({
//     patterns: [{ from: path.join(__dirname, '../../src'), to: path.join(__dirname, '../src/frontend') }],
//   })
// ],
  devServer: {
    historyApiFallback: true,

    static: {
      directory: path.join(__dirname, '../../', "public"), // by mhde // public was build
      watch: true, // hot relode
    },
    client: {
      progress: true,
    },

    devMiddleware: {
      writeToDisk: true,

    },

    port: 3001, // open: true,
  },


};
// new BundleAnalyzerPlugin()
