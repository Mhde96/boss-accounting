// webpack.prod.js: Webpack configuration only used by production mode.
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");
const webpack = require('webpack');

const CopyPlugin = require("copy-webpack-plugin");
const zlib = require("zlib");

module.exports = {
  mode: "production",
  // devtool: "source-map",
  output: {
    filename: "[name].[contenthash].js",
    sourceMapFilename: "bundle.map",
    path: path.resolve(__dirname, "../../out", "web"),
    chunkFilename: "[id].[fullhash:8].js",
    clean: true,
  },
  optimization: {
    moduleIds: "deterministic",
    runtimeChunk: "single",

    splitChunks: {
      chunks: "all",
      // cacheGroups: {
      //   vendor: {
      //     test: /[\\/]node_modules[\\/]/,
      //     name: "vendors",
      //     chunks: "all",
      //   },
      // },
    },
  },

  plugins: [
    new webpack.DefinePlugin({ 'process.env.NODE_OPTIONS': JSON.stringify('--max-old-space-size=512') })
,
    new CompressionPlugin({
      filename: "[name][ext].gz",
      algorithm: "gzip",
      test: /\.(js|css|html|svg)$/,
      threshold: 8192,
      minRatio: 0.8,
      // deleteOriginalAssets: true,
    }),

    new CompressionPlugin({
      filename: "[name][ext].br",
      algorithm: "brotliCompress",
      test: /\.(js|css|html|svg)$/,
      compressionOptions: {
        params: {
          [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
        },
      },
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }),

    new CopyPlugin({
      patterns: [
        {
          from: path.join(__dirname, "../../", "public/assets"),
          to: path.join(__dirname, "../../out", "web/assets"),
        },

        {
          from: path.join(__dirname, "../../", "public/favicon.ico"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/favicon-16x16.png"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/favicon-32x32.png"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(
            __dirname,
            "../../",
            "public/android-chrome-192x192.png"
          ),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/browserconfig.xml"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/mstile-150x150.png"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/safari-pinned-tab.svg"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/apple-touch-icon.png"),
          to: path.join(__dirname, "../../out", "web"),
        },
        {
          from: path.join(__dirname, "../../", "public/android-chrome-512x512.png"),
          to: path.join(__dirname, "../../out", "web"),
        },

      ],
    }),
  ],
};

// https://medium.com/@april9288/how-i-got-a-100-lighthouse-score-with-my-react-app-2b676ef62113
