import type IForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';

const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');


// eslint-disable-next-line @typescript-eslint/no-var-requires
const ForkTsCheckerWebpackPlugin: typeof IForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');


const copyPlugins = new CopyWebpackPlugin(
  {
    patterns: [
      {
        from: path.resolve(__dirname, '../../public', 'assets'),
        to: path.resolve(__dirname, '../../.webpack/renderer', 'assets')
      }
    ],
  },

);


export const plugins = [
  new ForkTsCheckerWebpackPlugin({
    logger: 'webpack-infrastructure',
  }),
  copyPlugins
];
