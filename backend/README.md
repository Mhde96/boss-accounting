<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

<h1 align="center">🚀 BOSS ACCOUNTING BACKEND 🚀</h1>

# [Demo 😎🚀](www.boss-accounting.com)

### This project has been initialized using [NestJs](https://nestjs.com/).

## Installation

```bash
git clone git@gitlab.com:Mhde96/nest-boss-accounting.git

cd nest-boss-accounting

npm install
```

## 🏗️ Structure of project
📚 <a href="https://medium.com/the-crowdlinker-chronicle/best-way-to-structure-your-directory-code-nestjs-a06c7a641401"> Best Way to Structure Your Directory/Code (NestJS)<a>


📚 <a href="https://medium.com/the-crowdlinker-chronicle/best-way-to-inject-repositories-using-typeorm-nestjs-e134c3dbf53c">Best Way to Inject Repositories using TypeORM (NestJS) <a>


## 📖 Documntation

For documentation, we use 📚 <a href="https://compodoc.app/guides/getting-started.html"> @compodoc/compodoc <a>


### to generate it

```bash
npx @compodoc/compodoc -p tsconfig.json -s
```

Then go to the generated documentation at documentation->index.html.

## Running the app

```bash
# development
npm run dev

# production mode
npm run start:prod
```

## 🧪 Test

```bash
# E2E TESTS
$ npm run test:e2e
```

```bash

This comand above will run 
start cmd.exe /K timeout /t 7  ^& jest --config ./test/jest-e2e.json && SET NODE_ENV=test&& nest start --watch

==> you can change time if it is not working 
```

Feel free to explore and contribute! 🚧
