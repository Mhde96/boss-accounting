// example.controller.ts
import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserAdminService } from './users.admin.service';
import { AuthGuard } from 'src/modules/users/auth.guard';
import { Request } from '@nestjs/common';
import { AdminRole } from 'src/modules/users/constants';
import { checkAdminEmail } from 'src/modules/users/utils';
@Controller('/admin')
export class UserAdminController {
  constructor(private readonly userService: UserAdminService) { }
  @UseGuards(AuthGuard)
  @Get('/users')
  async findAll(
    @Request() req,
    @Query('page') page: number = 1,
    @Query('limit') limit: number = 5,
    @Query('sortField') sortField: string = 'name',
    @Query('sortOrder') sortOrder: 'asc' | 'desc' = 'asc',
    @Query('searchTerm') searchTerm?: string,
  ): Promise<any> {
    const role = checkAdminEmail(req.body.user.email);
    if (+role !== AdminRole) {
      throw new HttpException(
        { message: 'This service is intended for cetrain role' },
        HttpStatus.FORBIDDEN,
      );
    }
    return await this.userService.findAll(
      page,
      limit,
      sortField,
      sortOrder,
      searchTerm,
    );
  }
}
