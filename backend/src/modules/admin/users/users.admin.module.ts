// example.module.ts
import { Module } from '@nestjs/common';
import { UserAdminController } from './users.admin.controller';
import { UserAdminService } from './users.admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/modules/users/user.entity';
import { UserService } from 'src/modules/users/users.service';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UserAdminController],
  providers: [UserAdminService, UserService],
})
export class UsersAdminModule {}
