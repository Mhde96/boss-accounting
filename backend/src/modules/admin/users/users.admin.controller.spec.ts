import { Test, TestingModule } from '@nestjs/testing';
import { UserAdminController } from './users.admin.controller';
import { UserAdminService } from './users.admin.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from 'src/modules/users/user.entity';
import { Repository } from 'typeorm';

describe('UserAdminController', () => {
  let controller: UserAdminController;
  let userService: UserAdminService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserAdminController],
      providers: [
        UserAdminService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<UserAdminController>(UserAdminController);
    userService = module.get<UserAdminService>(UserAdminService);
  });

  describe('findAll', () => {
    it('should call userService.findAll with the correct parameters', async () => {
      const findAllSpy = jest
        .spyOn(userService, 'findAll')
        .mockResolvedValueOnce({
          currentPage: 1,
          totalPages: 1,
          pageSize: 5,
          totalItems: 0,
          users: [],
        });

      await controller.findAll(1, 5, 'name', 'asc', 'John');

      expect(findAllSpy).toHaveBeenCalledWith(1, 5, 'name', 'asc', 'John');
    });
  });
});
