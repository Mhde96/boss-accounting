// example.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/modules/users/user.entity';
import { FindManyOptions, Like, Repository } from 'typeorm';

@Injectable()
export class UserAdminService {
  constructor(
    @InjectRepository(User)
    private readonly userReposiroty: Repository<User>,
  ) {}
  async findAll(
    page: number = 1,
    limit: number = 10,
    sortField: string = 'name',
    sortOrder: 'asc' | 'desc' = 'asc',
    searchTerm?: string,
  ) {
    const findOptions: FindManyOptions<User> = {
      where: searchTerm ? { name: Like(`%${searchTerm}%`) } : {},
      order: { [sortField]: sortOrder.toUpperCase() as 'ASC' | 'DESC' },
      skip: (page - 1) * limit,
      take: limit,
    };

    const [users, totalItems] = await this.userReposiroty.findAndCount(
      findOptions,
    );
    const totalPages = Math.ceil(totalItems / limit);
    return {
      currentPage: page,
      totalPages: totalPages,
      pageSize: limit,
      totalItems: totalItems,
      data: users,
    };
  }
}
