import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserAdminService } from './users.admin.service';
import { Like, Repository } from 'typeorm';
import { User } from 'src/modules/users/user.entity';

describe('UserAdminService', () => {
  let service: UserAdminService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserAdminService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<UserAdminService>(UserAdminService);
    userRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  describe('findAll', () => {
    it('should return paginated user data with searchTerm', async () => {
      const page = 1;
      const limit = 10;
      const sortField = 'name';
      const sortOrder = 'asc';
      const searchTerm = 'John';

      const findAndCountSpy = jest
        .spyOn(userRepository, 'findAndCount')
        .mockResolvedValueOnce([[], 0]); // Mock an empty result for simplicity

      const result = await service.findAll(
        page,
        limit,
        sortField,
        sortOrder,
        searchTerm,
      );

      expect(findAndCountSpy).toHaveBeenCalledWith({
        where: { name: Like(`%${searchTerm}%`) },
        order: { [sortField]: sortOrder.toUpperCase() as 'ASC' | 'DESC' },
        skip: 0,
        take: limit,
      });

      expect(result).toHaveProperty('currentPage', page);
      expect(result).toHaveProperty('pageSize', limit);
    });
  });
});
