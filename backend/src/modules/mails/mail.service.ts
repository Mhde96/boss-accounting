import { MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(
    @Inject(MailerService) private readonly mailerService: MailerService,
  ) {}

  async forgotPassword(token, user) {
    let response;
    await this.mailerService
      .sendMail({
        to: user.email, // List of receivers email address
        subject: 'Testing Nest Mailermodule with template ✔',
        template: 'forgot_password', // The `.pug` or `.hbs` extension is appended automatically.

        context: {
          // Data to be sent to template engine.
          code: 'asdasdasd',
          username: 'Mohamd Almahdi from',
          link: 'http://www.boss-accounting.com',
        },
      })
      .then((success) => {
        // console.log(success);
        response = success;
      })
      .catch((err) => {
        console.log(err);
      });

    if (response.accepted[0] == user.email) return true;
    else return false;
  }
}
