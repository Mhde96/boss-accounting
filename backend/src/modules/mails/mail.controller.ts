import { Controller, Get, Inject } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailerService } from '@nestjs-modules/mailer';
@Controller()
export class MailController {
  constructor(private readonly mailService: MailService) {}
  @Get('template1')
  sendMail(): any {
    console.log('hi');
    // console.log(process.cwd());
    // this.mailService.example();
    return 'Hellow template 13456';
  }

  @Get('template2')
  sendTemplate(): any {
    // this.mailService.example2();
    return 'Hellow template 212312321';
  }
}
