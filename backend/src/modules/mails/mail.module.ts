import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailService } from './mail.service';
import { EmailConfig } from '../../common/constants/index';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: EmailConfig.HOST,
        port: EmailConfig.PORT,
        secure: true,
        auth: {
          user: EmailConfig.DOMAIN,
          pass: EmailConfig.PASSWORD,
        },
      },
      defaults: { from: EmailConfig.DOMAIN },
      template: {
        dir: EmailConfig.DIR,
        adapter: new HandlebarsAdapter(),
        options: { strict: true },
      },
    }),
  ],
// controllers:[UserController],
  providers: [MailService],
})
export class MailModule {}
