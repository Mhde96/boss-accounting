import { Account } from '../accounts/accounts.entity';
import { Journal } from '../journals/journals.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'entries' })
export class Entry {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Journal, (journal) => journal.entries)
  @JoinTable()
  journal: Journal;

  @Column()
  description: string;

  // @ManyToOne(() => Account, (account) => account.id, { cascade: true })
  @Column({ name: "accountId" })
  accountId: number;

  @Column({ type: 'decimal', precision: 18, scale: 2 })
  debit: number;

  @Column({ type: 'decimal', precision: 18, scale: 2 })
  credit: number;
}
