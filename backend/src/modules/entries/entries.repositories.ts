import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Entry } from './entries.entity';
import { toApi } from 'src/common/helper/toApi';



@Injectable()
export class EntryRepository {
    constructor(
        @InjectRepository(Entry)
        private readonly entryRepository: Repository<Entry>

    ) { }

    async findOneByAccountId(accountId) {
        try {
            const data = await this.entryRepository.findOneBy({ accountId })
            return toApi({ data, success: true, message: 'We found one account' })
        } catch (error) {
            console.log('findOneByAccountId Error : ' + error)
            return toApi({ data: null, success: false, message: 'Error : ' + error })
        }
    }
}