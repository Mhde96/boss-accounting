import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { InjectRepository } from '@nestjs/typeorm';
import { Entry } from './entries.entity';
import { CraeteEntryDto } from './dto/create-entry';

import { UpdateJournalDto } from 'src/modules/journals/dto/update-journal-dto';
import { UpdateEntryDto } from './dto/update-entry';
import { EntriesType, EntryType } from './entries.type';
import { CheckType } from 'src/types/CheckType';
const _ = require("lodash")
@Injectable()
export class EntryService {
  constructor(
    @InjectRepository(Entry)
    private readonly EntryRepository: Repository<Entry>,
  ) { }

  async fetch() {
    // return await this.EntryRepository.find()
  }

  async add(entries: Array<CraeteEntryDto>) {
    return await this.EntryRepository.save(entries);
  }

  async update(journal: UpdateJournalDto) {
    let idsForSave = journal.entries.map((entry) => entry.id);
    let differenceIds: Array<number> = [];
    let entries = await this.EntryRepository.find({
      where: { journal: journal },
    });

    differenceIds = _.difference(
      entries.map((entry) => entry.id),
      idsForSave,
    );

    if (differenceIds.length > 0)
      await this.EntryRepository.delete(differenceIds);

    await this.EntryRepository.save(journal.entries);
  }

  async delete(journal: UpdateJournalDto) {
    let entries = await this.EntryRepository.find({
      where: { journal: journal },
    });

    let ids = entries.map((entry) => entry.id);
    if (ids.length > 0) this.EntryRepository.delete(ids);
  }


  validationEntries(entries: EntriesType): CheckType {
    let check: CheckType = {
      success: true,
      message: ""
    }
    const totalDebit = _.sumBy(entries, entry => +entry.debit);
    const totalCredit = _.sumBy(entries, entry => +entry.credit);
    if (totalCredit !== totalDebit)
      check = {
        success: false,
        message: `totalCredit ${totalCredit} Not Equale totalDebit ${totalDebit}`
      }

    else if (totalCredit == 0) check = {
      success: false,
      message: "totalCredit == 0"
    }
    else if (totalDebit == 0) check = {
      success: false,
      message: "totalDebit == 0"
    }

    entries.map((entry, index) => {
      if (entry.debit == 0 && entry.credit == 0)
        check = {
          success: false,
          message: "There is a account his value is Zero"
        }
    })


    return check
  }
}
