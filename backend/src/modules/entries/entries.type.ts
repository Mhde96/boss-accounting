export type EntriesType = EntryType[]
export type EntryType = {
    id?: number,
    description?: string;
    debit?: number;
    credit?: number;
    journal_id?: number;
}