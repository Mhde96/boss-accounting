import { PartialType } from '@nestjs/mapped-types';
import { CraeteEntryDto } from './create-entry';
import { IsDefined } from 'class-validator';

export class UpdateEntryDto extends PartialType(CraeteEntryDto) {

    @IsDefined()
    readonly id: number

}

