import { IsDefined } from 'class-validator';
import { Account } from 'src/modules/accounts/accounts.entity';
export class CraeteEntryDto {
  @IsDefined()
  user_id: number;

  @IsDefined()
  description: string;

  @IsDefined()
  accountId: number;

  @IsDefined()
  debit: number;

  @IsDefined()
  credit: number;

  @IsDefined()
  journal_id: number;
}
