import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Headers,
} from '@nestjs/common';
import { CraeteJournalDto } from './dto/create-journal-dto';
import { UpdateJournalDto } from './dto/update-journal-dto';
import { JournalService } from './journals.service';
import { toApi } from '../../common/helper/toApi';
import { AuthGuard } from '../users/auth.guard';
import { EntryService } from 'src/modules/entries/entries.services';
import { CheckType } from 'src/types/CheckType';
import { FinancialcycleValidate } from 'src/common/validations/FinancialcycleValidate';
import { MapService } from 'src/modules/maps/maps.service';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import { MapValidate } from 'src/common/validations/MapValidate';
import { FeatchJournalDto } from './dto/featch-journal-dto';

@Controller()
export class JournalController {
  constructor(
    private readonly journalService: JournalService,
    private readonly entryService: EntryService,
    private readonly mapService: MapService,
    private readonly financialCycleService: FinancialCycleService
  ) { }

  @UseGuards(AuthGuard)
  @Post('/fetch_journals')
  async fetch(@Body() body: FeatchJournalDto, @Request() req, @Headers() headers) {
    let user = req.user;
    let map_id = body.map_id
    let financialcycle_id = body.financialcycle_id;

    let data = await this.journalService.fetchAll(user, map_id, financialcycle_id);
    return toApi({ data, success: true, message: 'Success' });
  }

  @UseGuards(AuthGuard)
  @Post('/add_journal')
  async add(@Body() journal: CraeteJournalDto, @Request() req, @Headers() headers) {
    // Variables
    const user = req.user;
    const financialcycle_id = journal.financialcycle_id;
    const map_id = journal.map_id

    // Validation
    const mapValidate = await MapValidate(user, map_id, this.mapService)
    if (mapValidate) {
      return toApi({ data: [], success: false, message: 'Map Validate' });
    }

    const financialcycleValidate = await FinancialcycleValidate(this.financialCycleService, this.mapService, user, financialcycle_id, map_id)
    if (financialcycleValidate) {
      return toApi({ data: [], success: false, message: 'FinancialcycleValidate' });
    }



    let check: CheckType = this.entryService.validationEntries(journal.entries)
    if (check.success == false) return toApi({ data: [], ...check });
    let data = await this.journalService.add(journal, financialcycle_id, map_id);
    return toApi({ data, success: true, message: 'Success' });
  }

  @UseGuards(AuthGuard)
  @Post('/update_journal')
  async update(@Body() journal: UpdateJournalDto, @Request() req, @Headers() headers) {
    try {
      let user = req.user;
      const financialcycle_id = journal.financialcycle_id;
      const map_id = journal.map_id

      // Validation
      const mapValidate = await MapValidate(user, map_id, this.mapService)
      if (mapValidate) {
        return toApi({ data: [], success: false, message: 'Map Validate' });
      }

      const financialcycleValidate = await FinancialcycleValidate(this.financialCycleService, this.mapService, user, financialcycle_id, map_id)

      if (financialcycleValidate) {
        return toApi({ data: [], success: false, message: 'FinancialcycleValidate' });
      }

      let check: CheckType = this.entryService.validationEntries(journal.entries)

      if (check.success == false) return toApi({ data: [], ...check });




      let data = await this.journalService.update(journal, user);

      return toApi({ data, success: true, message: 'Success' });
    } catch (error) {
      console.log('update_journal error : ', error)
    }
  }

  @UseGuards(AuthGuard)
  @Post('/delete_journal')
  async delete(@Body() journal: UpdateJournalDto, @Request() req, @Headers() headers) {
    let user = req.user;
    const financialcycle_id = journal.financialcycle_id;
    const map_id = journal.map_id

    // Validation
    const mapValidate = await MapValidate(user, map_id, this.mapService)
    if (mapValidate) {
      return toApi({ data: [], success: false, message: 'Map Validate' });
    }

    const financialcycleValidate = await FinancialcycleValidate(this.financialCycleService, this.mapService, user, financialcycle_id, map_id)
    if (financialcycleValidate) {
      return toApi({ data: [], success: false, message: 'FinancialcycleValidate' });
    }

    let data = await this.journalService.delete(journal, user);
    return toApi({ success: true, data });
  }
}
