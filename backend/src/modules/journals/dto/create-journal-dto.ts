import { PartialType } from '@nestjs/mapped-types';
import { IsDefined } from 'class-validator';
import { CraeteEntryDto } from 'src/modules/entries/dto/create-entry';
import { User } from 'src/modules/users/user.entity';

export class JournalDto {
  user_id: number;

  @IsDefined()
  readonly description: string;

  @IsDefined()
  readonly financialcycle_id: number;

  @IsDefined()
  readonly map_id: number;

  @IsDefined()
  readonly date: Date;

  // @IsDefined()
  // closingEntries: boolean;
}

export class CraeteJournalDto extends PartialType(JournalDto) {
  @IsDefined()
  entries: CraeteEntryDto[];

  user: User;
}
