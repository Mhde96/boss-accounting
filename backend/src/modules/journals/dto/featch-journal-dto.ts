import { PartialType } from '@nestjs/mapped-types';
import { IsDefined } from 'class-validator';
import { CraeteEntryDto } from 'src/modules/entries/dto/create-entry';
import { User } from 'src/modules/users/user.entity';

class JournalDto {
    @IsDefined()
    readonly financialcycle_id: number;

    @IsDefined()
    readonly map_id: number;
}

export class FeatchJournalDto extends PartialType(JournalDto) {

}
