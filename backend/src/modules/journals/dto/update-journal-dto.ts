import { PartialType } from '@nestjs/mapped-types';
import { JournalDto } from './create-journal-dto';
import { IsDefined } from 'class-validator';
import { UpdateEntryDto } from 'src/modules/entries/dto/update-entry';

export class UpdateJournalDto extends PartialType(JournalDto) {

    @IsDefined()
    readonly id: number;

    @IsDefined()
    readonly entries: Array<UpdateEntryDto>;
}

