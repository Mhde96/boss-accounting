import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { InjectRepository } from '@nestjs/typeorm';
import { Journal } from './journals.entity';
import { CraeteJournalDto } from './dto/create-journal-dto';
import { EntryService } from '../entries/entries.services';
import { User } from '../users/user.entity';
import { Entry } from '../entries/entries.entity';

@Injectable()
export class JournalService {
  constructor(
    @InjectRepository(Journal)
    private readonly journalRepository: Repository<Journal>,
    @InjectRepository(Entry)
    private readonly entryRepository: Repository<Entry>,

    private readonly entryService: EntryService, // private readonly mapService: MapService,
  ) { }

  async fetchAll(user: User, map_id, financialcycle_id) {
    // let accounts = await this.journalRepository.find({
    //   where: { user: user, maps: user.currentMap },
    //   // select: {
    //   //   date: true,
    //   //   description: true
    //   // },
    //   // relations: { entries: { account: true } },
    // });

    let journals;
    journals = await this.journalRepository
      .createQueryBuilder('journal')
      .where('map_id=:map_id', { map_id: map_id })
      .andWhere('financialcycle_id=:financialcycle_id', { financialcycle_id })
      .leftJoinAndSelect('journal.entries', 'entries')
      .getMany();
    return journals;
  }

  async fetchAllWithIgnoreMap(map_id) {
    let journals;
    journals = await this.journalRepository
      .createQueryBuilder('journal')
      .where('mapsId=:mapsId', { mapsId: map_id })
      .getMany();

    return journals;
  }


  async fetchById({ id, user }) {
    let data = await this.journalRepository.findOne({
      where: { id, user },

      // relations: { entries: { account: true } },
    });
    return data;
  }

  async deleteAllJournal({ financialcycle_id, user_id }) {

    console.log({
      financialcycle_id,
      user_id
    })
    let data = await this.journalRepository.find({
      where: { financialcycle_id, user_id },

      // relations: { entries: { account: true } },
    });

    if (data)
      data?.map((item) => {
        const journal = item;
        this.entryRepository.delete({ journal });
        this.journalRepository.delete(journal.id);
      });
    return data;
  }

  async deleteAllJournalAndEntries({ }) { }

  async add(journal: CraeteJournalDto, financialcycle_id, map_id) {
    let number = await this.createNewJournalNumber(journal.user);

    console.log({ map_id })
    let data: Journal = await this.journalRepository.save({
      ...journal,
      number,
      map_id: map_id,

      financialcycle_id,
    });

    journal.entries.map((entry) => {
      entry['accountId'] = entry.accountId;
      entry['journal_id'] = data.id;
    });

    await this.entryService.add(journal.entries);

    let _data = await this.fetchById({ id: data.id, user: journal.user });

    return _data;
  }

  async update(journal: any, user) {
    await this.journalRepository.save(journal);
    await this.entryService.update(journal);

    let _data = await this.fetchById({
      id: journal.id,
      user: user,
    });

    return _data;
  }

  async delete(journal: any, user) {
    let check = await this.checkJournalExist({
      id: journal.id,
      user: journal.user,
    });

    if (check) {
      this.entryRepository.delete({ journal });
      this.journalRepository.delete(journal.id);

      return 'jurnals had been deleted ';
    } else return false;
  }

  async checkJournalExist({ user, id }) {
    let data = await this.fetchById({ user, id });
    if (data == null) return false;
    else if (data.id != undefined) return true;
    else return false;
  }

  async createNewJournalNumber(user) {
    // this.journalRepository.

    const lastRow = await this.journalRepository.findOne({
      order: { id: 'DESC' },
      where: { user },
    });

    if (lastRow == null) return 1;
    return lastRow.number + 1;
  }
}
