import { User } from 'src/modules/users/user.entity';
import { Entry } from '../entries/entries.entity';
import { Map } from '../maps/maps.entity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'journals' })
export class Journal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @ManyToOne(() => User, (user) => user.id, { cascade: true })
  @JoinColumn()
  user: User;

  @Column()
  description: string;

  @Column()
  date: Date;

  @OneToMany(() => Entry, (entry) => entry.journal, { cascade: true })
  @JoinTable()
  entries: Entry[];

  @Column()
  number: number;

  @ManyToOne(() => Map, (map) => map.id, { cascade: true })
  @JoinTable()
  @Column({ name: "map_id" })
  map_id: number;

  // @Column({ default: 0 })
  // closingEntries: boolean;

  @Column({ default: null })
  financialcycle_id: number;
}
