import { Module } from '@nestjs/common';
import { JournalController } from './journals.controller';
import { JournalService } from './journals.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Journal } from './journals.entity';
import { EntryService } from '../entries/entries.services';
import { Entry } from '../entries/entries.entity';
import { UsersModule } from '../users/users.module';
import { MapsModule } from 'src/modules/maps/maps.module';
import { MapService } from 'src/modules/maps/maps.service';
import { FinancialCyclesModule } from 'src/modules/financialcycles/financialcycles.module';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import { Map } from 'src/modules/maps/maps.entity';
import { FinancialCycle } from 'src/modules/financialcycles/financialcycles.entity';
import { MapRepository } from '../maps/map.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ Journal, Entry,Map, FinancialCycle]), UsersModule, MapsModule, FinancialCyclesModule],
  controllers: [JournalController],
  providers: [ JournalService, EntryService, MapService, FinancialCycleService,MapRepository],
})

export class JournalsModule { }
