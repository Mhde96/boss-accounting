import { adminEmail } from './constants';

export function checkAdminEmail(email: string): number | undefined {
  let role = undefined;
  for (let i = 0; i < adminEmail.length; i++) {
    if (email === adminEmail[i]) {
      role = 1;
      break;
    }
  }
  return role;
}
