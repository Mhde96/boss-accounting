import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Request,
  Get,
  Param,
  Res,
  HttpStatus,
  HttpException,
  Inject,
} from '@nestjs/common';
import { UserService } from './users.service';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateUserDto } from './dto/create-user.dto';
import { AuthGuard } from './auth.guard';
import { diskStorage } from 'multer';
import { MailService } from '../mails/mail.service';
import { Exception, toApi } from '../../common/helper/toApi';
import { randomName } from '../../common/helper/randomName';
// import fse from 'fs-extra';
import { readFileSync } from 'fs';
import { MapService } from 'src/modules/maps/maps.service';
import { checkAdminEmail } from './utils';
import * as moment from 'moment'
import { exampleOnRegister } from 'src/common/constants/example-on-register';
const fse = require('fs-extra');

@Controller()
export class UserController {
  constructor(
    private readonly mailService: MailService,
    private readonly userService: UserService, // private readonly mapService: MapService,

    private readonly mapService: MapService,
  ) { }

  @Post('/register')
  async create(@Body() user: CreateUserDto): Promise<any> {
    const foundUser = await this.userService.findOne(user.email);
    if (foundUser)
      throw new HttpException(
        { message: 'this email used before' },
        HttpStatus.FORBIDDEN,
      );
    let data = await this.userService.create(user);
    let token = await this.userService.generateJwtToken(data);
    let map = await this.mapService.createMap({
      user: { ...data, token },
      description: exampleOnRegister.description,
      name: exampleOnRegister.name,
      end_at: exampleOnRegister.end_at,
      start_at: exampleOnRegister.start_at,
    });
    let role = checkAdminEmail(user.email);
    user = await this.userService.update({ ...data, currentMap: map, token });
    data = {
      ...data,
      token,
      currentMap: map,
      role: role,
    };
    return toApi({ data, success: true });
  }

  @Post('/login')
  async login(@Body() user: User): Promise<any> {
    // console.log(user);
    const foundUser = await this.userService.findOne(user.email);

    if (foundUser == null)
      throw new HttpException(
        { message: 'this email not existed' },
        HttpStatus.FORBIDDEN,
      );

    const isPasswordValid = await bcrypt.compare(
      user.password,
      foundUser.password,
    );

    if (!isPasswordValid)
      throw new HttpException(
        { message: 'email or password is invalid' },
        HttpStatus.FORBIDDEN,
      );

    try {
      const token = await this.userService.generateJwtToken(foundUser);
      let role = checkAdminEmail(user.email);
      return toApi({ success: true, data: { ...foundUser, token, role } });
    } catch (error) {
      throw new HttpException(
        { message: 'email or password is invalid' },
        HttpStatus.FORBIDDEN,
      );
    }
  }

  @UseGuards(AuthGuard)
  @Post('backup_upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          return cb(null, `${randomName()}.json`);
        },
      }),
    }),
  )
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Request() req) {
    try {
      let user = req.user;
      const filePath = './uploads/' + user.file;
      fse.remove(filePath);

      const backup = JSON.parse(await readFileSync(filePath, 'utf8'));
      user.last_sync = backup.last_sync;
      user.file = file.filename;
      user = await this.userService.update(req.user);
      return toApi({ data: user, success: true });
    } catch (err) {
      console.log(err);
      Exception({ message: 'server internal error ' });
    }
  }

  @UseGuards(AuthGuard)
  @Get('backup_download')
  async downloadFile(@Res() res: any, @Request() req) {
    let user = req.user;
    const filePath = `./uploads/${user.file}`;

    if (user.file == 'null') Exception({ message: ' no backup for this user' });

    try {
      const stats = fse.statSync(filePath);
      const fileSizeInBytes = stats.size;
      res.setHeader('Content-Length', fileSizeInBytes);
      res.setHeader('Content-Type', 'application/octet-stream');
      res.setHeader('Content-Disposition', `attachment; filename=${user.file}`);
      const fileStream = fse.createReadStream(filePath);
      fileStream.pipe(res);

      // const backup = JSON.parse(await readFileSync(filePath, 'utf8'))
      // user.last_sync = backup.last_sync;

      user = await this.userService.update(req.user);
    } catch (error) {
      console.log(`Failed to download ${filePath}: ${error.message}`);
      res.sendStatus(404);
    }
  }

  @Get('/forgot_password/:email')
  async forgotPassword(@Param('email') email: string) {
    let user: any = await this.userService.findOne(email);
    if (!user)
      throw new HttpException(
        { message: 'this email not existed' },
        HttpStatus.FORBIDDEN,
      );

    let token = await this.userService.generateJwtToken(user);

    let success = await this.mailService.forgotPassword(token, user);

    if (success == true) {
      return toApi({
        message: 'we sent email to your account ',

        success: true,
        data: null,
      });
    } else {
      throw new HttpException(
        { message: 'this email not existed' },
        HttpStatus.FORBIDDEN,
      );
    }
  }

  @Post('/rest_password')
  async restPassword(@Body() user: any) {
    let { token, new_password } = user;

    let _user = await this.userService.fingByToken(token);
    const salt = await bcrypt.genSalt();
    _user.password = await bcrypt.hash(new_password, salt);

    return toApi({ data: this.userService.update(_user), success: true });
  }

  @UseGuards(AuthGuard)
  @Post('/refreshToken')
  async refreshToken(@Request() req) {
    let user = req.user;
    const token = await this.userService.generateJwtToken(user);
    let role = checkAdminEmail(user.email);
    return toApi({ data: { ...user, token, role }, success: true });
  }

  @Get('/getTimeFromServer')
  getTimeFromServer() {
    return toApi({ data: Date.now(), success: true });
  }

  @Post('test')
  test() {
    let ms1 = new Date(1681899919329);
    let ms2 = new Date(1681900282053);

    return { ms1, ms2 };
  }
}
