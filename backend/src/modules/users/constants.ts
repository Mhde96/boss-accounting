export const jwtConstants = {
  // secret: 'DO NOT USE THIS VALUE. INSTEAD, CREATE A COMPLEX SECRET AND KEEP IT SAFE OUTSIDE OF THE SOURCE CODE.',
  secret: '11223344',
};

export const adminEmail = [
  'mohammedyosef712@gmail.com',
  'm.mhde96@hotmail.com',
  'm.mhde96@gmail.com',
];

export const AdminRole = 1;
