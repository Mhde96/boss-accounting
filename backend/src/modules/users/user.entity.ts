import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Map } from '../maps/maps.entity';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({ nullable: true }) // Assuming phone number is optional
  phone: string;

  @Column({ nullable: true })
  occupation: string;

  // Counter code in the user's name
  @Column({ nullable: true }) // Assuming counter code is optional
  countryCode: string;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    onUpdate: 'CURRENT_TIMESTAMP',
  })
  updated_at: Date;

  @Column({ default: 'now' })
  last_sync: string;

  @Column({ default: 'null' })
  file: string;



  @OneToMany(() => Map, (map) => map.user)
  // @JoinTable()
  maps: Map[];

  @OneToOne(() => Map, (map) => map.user)
  @JoinColumn()
  @JoinTable()
  currentMap: Map;

  @Column()
  password: string;
}
