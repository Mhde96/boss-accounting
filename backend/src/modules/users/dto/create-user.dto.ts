import { IsDefined, IsEmail, isDefined, isString } from 'class-validator';
import { UserExists } from '../validator/UserExistsRule';

export class CreateUserDto {
  id: number;

  @IsDefined()
  readonly name: string;

  // @UserExists()
  @IsEmail()
  readonly email: string;

  // @IsDefined()
  // readonly phone: string;

  // @IsDefined()
  // readonly countryCode: string;

  @IsDefined()
  readonly occupation: string;

  @IsDefined()
  password: string;

  last_sync?: string;

  file?: string;
}
