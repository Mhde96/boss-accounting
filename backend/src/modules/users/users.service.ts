import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { User } from './user.entity';
import { jwtConstants } from './constants';
import { JwtService } from '@nestjs/jwt';
import { UpdateMapDto } from 'src/modules/maps/dto/update-map-dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async findOne(email: string): Promise<any> {
    return await this.userRepository.findOne({
      where: { email },
      relations: { maps: true, currentMap: true },
    });
  }

  async findById(id: number): Promise<User> {
    return this.userRepository.findOne({
      where: { id },
      relations: {
        maps: true,
        currentMap: true,
      },
      select: {
        email: true,
        id: true,
        name: true,
        currentMap: { id: true, description: true, name: true },
        maps: { description: true, name: true, id: true },
      },
    });
  }

  async create(user: any) {
    const salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(user.password, salt);

    return await this.userRepository.save(user);
  }

  async update(user: any) {
    return this.userRepository.save(user);
  }

  async deleteFile(fileName: string) {}

  async getOneOrFail(email: string) {
    return this.userRepository.findOneOrFail({ where: { email } });
  }

  async generateJwtToken(user: User): Promise<string> {
    const payload = { sub: user.id };
    return jwt.sign(payload, jwtConstants.secret);
  }

  async fingByToken(token: string) {
    const payload = await this.jwtService.verifyAsync(token, {
      secret: jwtConstants.secret,
    });
    let user = await this.findById(payload.sub);
    return user;
  }

  async changeCurrentMap(map: UpdateMapDto) {
    await this.userRepository
      .createQueryBuilder()
      .update(User)
      .set({ currentMap: map })
      .where('id = :id', { id: map.user.id })
      .execute();

    return await this.findById(map.user.id);
  }
}
