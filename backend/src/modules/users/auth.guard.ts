import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AdminRole, jwtConstants } from './constants';
import { Request } from 'express';
import { UserService } from './users.service';
import { checkAdminEmail } from './utils';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);

    if (!token) {
      // toApi({success:false , data:null , message:'Unauthorized 1'});
      throw new UnauthorizedException("Unauthorized 1");
    }

    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: jwtConstants.secret,
      });

      let user = await this.userService.findById(payload.sub);
      // let role = checkAdminEmail(user.email);
      // if (role != AdminRole)
      //   if (!request['headers']?.financialcycle_id) {
      //     throw new HttpException(
      //       { message: 'financialcycle_id is required' },
      //       HttpStatus.FORBIDDEN,
      //     );
      //   }

      request['user'] = { ...user };
      request['body'] = {
        ...request['body'],
        user_id: user.id,
        user,
      };
    } catch {
      throw new UnauthorizedException("Unauthorized 2");
    }

    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
