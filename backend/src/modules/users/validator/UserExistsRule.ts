import {
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  registerDecorator,
} from 'class-validator';
import { Injectable, createParamDecorator } from '@nestjs/common';
import { UserService } from '../users.service';

@ValidatorConstraint({ name: 'email', async: true })
@Injectable()
export class UserExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly userService: UserService) {}

  async validate(email: string) {
    try {
      await this.userService.getOneOrFail(email);
    } catch (e) {
      return true;
    }

    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `user exist`;
  }
}

export function UserExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserExistsRule,
    });
  };
}
