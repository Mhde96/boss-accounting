import { Module } from '@nestjs/common';
import { UserService } from './users.service';
import { UserController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { UserExistsRule } from './validator/UserExistsRule';
import { MailService } from '../mails/mail.service';
import { MapService } from 'src/modules/maps/maps.service';
import { Map } from 'src/modules/maps/maps.entity';
import { FinancialCycle } from 'src/modules/financialcycles/financialcycles.entity';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import { Journal } from 'src/modules/journals/journals.entity';
import { JournalService } from 'src/modules/journals/journals.service';
import { Entity } from 'typeorm';
import { EntryService } from 'src/modules/entries/entries.services';
import { Entry } from 'src/modules/entries/entries.entity';
import { MapRepository } from '../maps/map.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Map, FinancialCycle, Journal, Entry]),
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
    }),
  ],

  controllers: [UserController],
  providers: [
    UserService,
    UserExistsRule,
    MailService,
    MapService,
    FinancialCycleService,
    JournalService,
    EntryService,
    MapRepository
  ],
  exports: [UserService],
})
export class UsersModule {}
