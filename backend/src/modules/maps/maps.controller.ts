import {
  Controller,
  Post,
  UseGuards,
  Request,
  Body,
  HttpException,
  HttpStatus,
  Headers,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MapService } from './maps.service';
import { toApi } from '../../common/helper/toApi';
import { UserService } from '../users/users.service';
import { AuthGuard } from '../users/auth.guard';
import { CreateMapDto } from './dto/create-map-dto';
import { UpdateMapDto } from './dto/update-map-dto';
import { AccountService } from 'src/modules/accounts/account.service';
import { JournalService } from 'src/modules/journals/journals.service';
import { Map } from './maps.entity';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import * as moment from 'moment';
@Controller()
export class MapController {
  constructor(
    private readonly mapService: MapService,
    private readonly userService: UserService,
    private readonly accountService: AccountService,
    private readonly journalService: JournalService,
    private readonly financialcycleService: FinancialCycleService,
  ) { }

  @UseGuards(AuthGuard)
  @Post('/fetch_maps')
  async fetch(@Request() req) {
    let user = req.user;
    let data: Array<Map> = null;

    data = await this.mapService.findAll(user);

    for (let i = 0; i < data.length; i++) {
      let map_id = data[i].id;
      let financialcycles = await this.financialcycleService.fetchAll(map_id);
      data[i] = { ...data[i], financialcycles };
    }

    return await toApi({ data, success: true, message: 'success' });
  }

  @UseGuards(AuthGuard)
  @Post('/create_map')
  async create(@Body() map: CreateMapDto, @Request() request) {
    // let params = map;

    let data = await this.mapService.createMap(map);

    // this.financialcycleService.add({
    //   map_id: data.id,

    //   start_at: moment(params.start_at).format(),
    //   end_at: moment(params.end_at).format(),
    // });
    return toApi({ data, success: true, message: 'success' });
  }

  @UseGuards(AuthGuard)
  @Post('/update_map')
  async update(@Body() map: UpdateMapDto) {
    let data = await this.mapService.updateMap(map);
    return toApi({ data, success: true });
  }

  @UseGuards(AuthGuard)
  @Post('/delete_map')
  async delete(@Body() map: UpdateMapDto, @Headers() headers) {
    let user = map.user;
    let isTrue = false;
    let map_id = map.id;
    let financialcycle_id = headers.financialcycle_id;
    // step 1
    // check if there is no accounts are related to this map
    isTrue = await this.accountService.checkIfAccountsRelatedToMap(map_id);
    if (isTrue == false) {
      throw new HttpException(
        { message: 'there is accounts related to this map' },
        HttpStatus.FORBIDDEN,
      );
    }

    const journals = await this.journalService.fetchAllWithIgnoreMap(
      map_id,
    );
    if (journals.length > 0) {
      throw new HttpException(
        { message: 'there is journals related to this map' },
        HttpStatus.FORBIDDEN,
      );
    }

    const maps = await user.maps;
    if (maps.length <= 1) {
      throw new HttpException(
        { message: 'there is only one map' },
        HttpStatus.FORBIDDEN,
      );
    }

    let shiftToMap = await user.maps.find(
      (item) => item.id != user.currentMap.id,
    );
    shiftToMap['user'] = user;

    // let data = (await this.userService.changeCurrentMap(shiftToMap)).currentMap;
    this.financialcycleService.deleteAll(map_id)
    let deleted = await this.mapService.delete(map);
    if (deleted.affected >= 1) return toApi({ data: [], success: true });
    else
      throw new HttpException(
        { message: 'somthing happen' },
        HttpStatus.FORBIDDEN,
      );
  }

  @UseGuards(AuthGuard)
  @Post('/change_current_map')
  async changeCurrentMap(@Body() map: UpdateMapDto) {
    let data = await this.userService.changeCurrentMap(map);
    return toApi({ data, success: true });
  }
}
