import { FinancialCycle } from 'src/modules/financialcycles/financialcycles.entity';
import { User } from '../users/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'maps' })
export class Map {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.maps)
  @JoinTable()
  @JoinColumn()
  user: User;

  @Column()
  name: string;

  @Column({ type: 'longtext' })
  description: string;

  financialcycles: Array<FinancialCycle>
}
