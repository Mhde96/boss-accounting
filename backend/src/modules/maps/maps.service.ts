import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Map } from './maps.entity';
import { Repository } from 'typeorm';
import { CreateMapDto } from './dto/create-map-dto';
import { User } from 'src/modules/users/user.entity';
import { UpdateMapDto } from './dto/update-map-dto';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import * as moment from 'moment';
import { MapRepository } from './map.repository';
import { toApi } from 'src/common/helper/toApi';
@Injectable()
export class MapService {
  constructor(
    private readonly financialcycleService: FinancialCycleService,
    private readonly _mapRepository: MapRepository,

    @InjectRepository(Map)
    private readonly mapRepository: Repository<Map>,
  ) { }

  async findAll(user: User) {
    user.maps.map((item) => {
      // let financialcycles =
    });
    return user.maps;
  }

  async createMap(map: CreateMapDto) {
    let newMap = await this.mapRepository.save(map);
    this.financialcycleService.add({
      map_id: newMap.id,

      start_at: map.start_at,
      end_at:map.end_at,
    });


    return newMap
  }

  async updateMap(map: UpdateMapDto) {
    await this.mapRepository
      .createQueryBuilder()
      .update(Map)
      .set({
        description: map.description,
        name: map.name,
      })
      .where('id = :id', { id: map.id })
      .execute();

    return await this.mapRepository.findOneOrFail({ where: { id: map.id } });
  }

  async delete(map: UpdateMapDto) {
    return await this.mapRepository.delete(map.id);
  }

  async checkUserIdIsRelatedToThisMap(mapId, userId) {
    const data = await this._mapRepository.findOne(mapId)
    if (data.success) {
      if (data?.data?.user?.id == userId) {
        return toApi({ data: [], success: true, message: 'this map match this user' })
      }

      else return toApi({ data: [], success: false, message: 'this map do not match this user' })

    } else {

      return toApi({ data: [], success: false, message: 'there is a problem happen 22144 ' })
    }

  }
}
