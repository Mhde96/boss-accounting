import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Map } from './maps.entity';
import { MapController } from './maps.controller';
import { MapService } from './maps.service';
import { UsersModule } from '../users/users.module';
import { AccountService } from 'src/modules/accounts/account.service';
import { Account } from 'src/modules/accounts/accounts.entity';
import { JournalService } from 'src/modules/journals/journals.service';
import { Journal } from 'src/modules/journals/journals.entity';
import { JournalsModule } from 'src/modules/journals/journal.module';
import { AccountsModule } from 'src/modules/accounts/account.module';
import { Entry } from 'src/modules/entries/entries.entity';
import { EntryService } from 'src/modules/entries/entries.services';
import { FinancialCycleService } from 'src/modules/financialcycles/financialcycles.service';
import { FinancialCycle } from 'src/modules/financialcycles/financialcycles.entity';
import { FinancialCyclesModule } from 'src/modules/financialcycles/financialcycles.module';
import { AccountRepository } from '../accounts/account.repository';
import { EntryRepository } from '../entries/entries.repositories';
import { MapRepository } from './map.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([Map, Account, Journal, Entry, FinancialCycle]),
    UsersModule,
  ],
  controllers: [MapController],
  providers: [
    MapService,
    AccountService,
    JournalService,
    EntryService,
    FinancialCycleService,
    AccountRepository,
    EntryRepository,
    MapRepository
  ],
})
export class MapsModule {}
