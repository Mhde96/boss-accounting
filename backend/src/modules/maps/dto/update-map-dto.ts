import { PartialType } from '@nestjs/mapped-types';
import { IsDefined } from 'class-validator';
import { User } from 'src/modules/users/user.entity';
import { CreateMapDto } from './create-map-dto';

export class UpdateMapDto extends PartialType(CreateMapDto) {
  @IsDefined()
  id: number
}
