import { IsDefined } from 'class-validator';
import { User } from '../../users/user.entity';

export class CreateMapDto {
  @IsDefined()
  description: string;

  @IsDefined()
  name: string;

  @IsDefined()
  user: User;

  @IsDefined()
  start_at: string;

  @IsDefined()
  end_at: string;
}
