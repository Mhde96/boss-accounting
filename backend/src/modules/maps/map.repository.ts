import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Map } from './maps.entity';
import { toApi } from 'src/common/helper/toApi';


@Injectable()
export class MapRepository {
    constructor(
        @InjectRepository(Map)
        private readonly mapRepository: Repository<Map>
    ) { }

    async findOne(mapId) {
        try {
            const data = await this.mapRepository.findOne({
                where: { id: mapId },
                relations: { user: true }
            })
            if (data?.id == undefined) {
                return toApi({ data, success: false, message: "we didn't find this map" })
            } else {
                return toApi({ data, success: true, message: "we find this map successfully" })
            }
        } catch (error) {
            console.log("findOne Error : ", error)
            return toApi({ data: null, success: false, message: "Map Repository findOne Error" })
        }
    }
}