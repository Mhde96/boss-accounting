import { Account } from './accounts.entity';

export const financialAccounts: Account[] = [
  {
    id: -2,
    name: 'متاجرة',
    financial_statement: -1,
    map_id: undefined,
    user: undefined,
  },
  {
    id: -1,
    name: 'ارباح وخسائر',
    financial_statement: 0,
    map_id: undefined,
    user: undefined,
  },
];
