import { Controller, Post, Body, UseGuards, Request, Headers } from '@nestjs/common';
import { CraeteAccountDto } from './dto/create-account-dto';
import { AccountService } from './account.service';
import { toApi } from '../../common/helper/toApi';
import { UpdateAccountDto } from './dto/update-account-dto';
import { AuthGuard } from '../users/auth.guard';
import { User } from '../users/user.entity';
import { HttpException, HttpStatus } from '@nestjs/common';
@Controller()
export class AccountController {
  constructor(private readonly accountService: AccountService) { }


  @UseGuards(AuthGuard)
  @Post('/find_account_by_id')
  async findOne(@Request() req, @Headers() headers, @Body() account: UpdateAccountDto,) {
    const map_id = headers.map_id
    const user: User = req.user;

    // console.log("map_id : ",map_id , " user : " , user.name)
    const data = await this.accountService.findAccountById(account, user, map_id)


    return data
  }

  @UseGuards(AuthGuard)
  @Post('/fetch_accounts')
  async fetch(@Body() body, @Request() req, @Headers() headers) {
    let map_id = body.map_id
    let user: User = req.user;
    let data = await this.accountService.fetch(user, map_id);
    return toApi({ data, success: true, message: 'Success' });
  }

  @UseGuards(AuthGuard)
  @Post('/add_account')
  async add(@Body() account: CraeteAccountDto, @Request() req, @Headers() headers) {
    const map_id = account.map_id;
    const user = req.user


    let checkExist = await this.accountService.checkAccountExisBefore(account, map_id, user);
    if (checkExist == false) {
      return toApi({ data: null, success: false, message: 'checkAccountExisBefore is false and it must be true' });
    }

    let checkMap = await this.accountService.checkMapAccount(account, map_id, user);

    if (checkMap == false) {
      return toApi({ data: null, success: false, message: 'checkMapAccount is false and it must be true' });
    }

    try {

      if (checkExist && checkMap) {

        let data = await this.accountService.add(account, map_id);
        return toApi({ data, success: true, message: 'Success' });
      } else {
        throw new HttpException(
          { message: 'this request have a problem with checkExist or checkMap' },
          HttpStatus.FORBIDDEN,
        );
      }

    } catch (error) {
      console.log('add account error : ', error)
    }
  }

  @UseGuards(AuthGuard)
  @Post('/update_account')
  async update(@Body() account: UpdateAccountDto) {
    let data = await this.accountService.update(account);
    return toApi({ data, success: true, message: 'Success' });
  }

  @UseGuards(AuthGuard)
  @Post('/delete_account')
  async delete(@Body() account: UpdateAccountDto) {
    return await this.accountService.delete(account);
  }

  // @UseGuards(AuthGuard)
  // @Post('/generate_dummy_data')
  // async generateDummyData(@Request() req) {
  //   if (process.env.DB_NAME == 'test_nest') {
  //     const user = req.user;

  //     for (let i = 0; i < 10; i++) {
  //       await this.accountService.add({
  //         map: user.currentMap,
  //         financial_statement: 0,
  //         name: 'acc ' + i,
  //         user,
  //         user_id: user.id,
  //       });
  //     }
  //   }
  // }
}
