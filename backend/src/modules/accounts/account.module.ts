import { Module } from '@nestjs/common';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './accounts.entity';
import { UsersModule } from '../users/users.module';
import { AccountRepository } from './account.repository';
import { EntryRepository } from '../entries/entries.repositories';
import { Entry } from '../entries/entries.entity';
import { MapService } from '../maps/maps.service';
import { FinancialCycleService } from '../financialcycles/financialcycles.service';
import { Map } from '../maps/maps.entity';
import { FinancialCycle } from '../financialcycles/financialcycles.entity';
import { JournalService } from '../journals/journals.service';
import { Journal } from '../journals/journals.entity';
import { EntryService } from '../entries/entries.services';
import { MapRepository } from '../maps/map.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Account, Entry, Map, FinancialCycle, Journal]), UsersModule],
  controllers: [AccountController],
  providers: [AccountService, AccountRepository, EntryRepository, MapService, FinancialCycleService, JournalService, EntryService, MapRepository],
})
export class AccountsModule { }
