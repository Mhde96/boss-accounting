import { Injectable, UseGuards } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Account } from './accounts.entity';
import { CraeteAccountDto } from './dto/create-account-dto';
import { UpdateAccountDto } from './dto/update-account-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthGuard } from '../users/auth.guard';
import { User } from '../users/user.entity';
import { Exception, toApi } from 'src/common/helper/toApi';
import { financialAccounts } from './accounts.constant';
import { AccountRepository } from './account.repository';
import { EntryRepository } from '../entries/entries.repositories';
import { MapService } from '../maps/maps.service';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,

    private readonly _accountRepository: AccountRepository,
    private readonly entryRepository: EntryRepository,
    private readonly mapService: MapService
  ) { }

  @UseGuards(AuthGuard)
  async fetch(user: User, map_id: number) {
    let data = await this.accountRepository.find({
      where: { map_id },
    });

    return (data = [...financialAccounts, ...data]);
  }

  async add(account: CraeteAccountDto, map_id) {
    try {
      let data = await this.accountRepository.save({
        ...account,
        map_id: map_id,
      });

      return data;
    } catch (error) {
      console.log("add account error : ", error)
    }

  }

  async update(account: UpdateAccountDto) {
    return await this.accountRepository.save(account);
  }

  async delete(account: UpdateAccountDto) {
    try {
      const isUsedInEntries = await this.entryRepository.findOneByAccountId(account.id)

      if (isUsedInEntries?.data?.id) {
        return toApi({ data: null, success: false, message: "we coduln't delete this account" })
      }
      else return await this._accountRepository.deleteOne(account);
    } catch (error) {
      console.log('account delete Error : ' + error)
      return toApi({ data: null, success: false, message: 'Error : ' + error })

    }
  }

  async checkMapAccount(account: CraeteAccountDto, map_id, user) {
    let check = user.maps.some(map => map.id == map_id)

    return check
  }

  async checkAccountExisBefore(account: CraeteAccountDto, map_id, user) {
    let check = await this.accountRepository.findOne({
      where: {
        name: account.name,
        user: user,
        map_id
        // maps: account.user.currentMap,
      },
    });

    if (check == null) return true;
    else Exception({ message: 'this account used before' });
  }

  async findAccountById(account, user, mapId) {
    let checkMap
    try {

      checkMap = await this.mapService.checkUserIdIsRelatedToThisMap(mapId, user?.id)
      if (checkMap.success) {

        const receiveAccount = await this._accountRepository.findOne(account)

        if (receiveAccount?.data?.map_id == mapId) {
          return toApi({ data: receiveAccount.data, success: true, message: 'Success' })
        }

        else {
          return toApi({ data: null, success: false, message: 'This Account for another user' })

        }
      }
    } catch (error) {
      console.log("findAccountById ", error)
    } return toApi({ data: [], success: false, message: 'there is a problem happen 65465 ' })

  }

  async findOne(map_id) {
    let data = await this.accountRepository.findOne({
      where: {
        map_id,
      },
    });

    return data;
  }

  async checkIfAccountsRelatedToMap(map_id) {
    let isTrue = true;

    let data = await this.findOne(map_id);
    if (data?.id > 0) {
      isTrue = false;
    }

    return isTrue;
  }
}
