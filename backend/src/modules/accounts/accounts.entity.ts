import { User } from 'src/modules/users/user.entity';
import { Map } from '../maps/maps.entity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'accounts' })
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  // @Column()
  // user_id: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn()
  user: User;

  @Column({ nullable: false })
  name: string;

  @Column()
  financial_statement: number;

  @Column()
  map_id: number;

  // @ManyToOne(() => Map, (map) => map.id, { cascade: true })
  // @JoinTable({ name: 'map_id' })
  // maps: Map;
}
