import { PartialType } from '@nestjs/mapped-types';
import { CraeteAccountDto } from './create-account-dto';
import { IsDefined } from 'class-validator';

export class UpdateAccountDto extends PartialType(CraeteAccountDto) {

    @IsDefined()
    readonly id: number

}

