import { IsDefined, IsNotEmpty } from 'class-validator';
import { User } from '../../users/user.entity';
import { Map } from 'src/modules/maps/maps.entity';

export class CraeteAccountDto {
  @IsDefined()
  @IsNotEmpty()
  readonly name: string;

  @IsDefined()
  readonly user_id: number;

  @IsDefined()
  readonly financial_statement: number;

  @IsDefined()
  readonly user: User;

  @IsDefined()
  readonly map_id: number;

  map: Map;
}
