import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { Injectable } from '@nestjs/common';


import { Account } from './accounts.entity'
import { UpdateAccountDto } from './dto/update-account-dto';
import { toApi } from 'src/common/helper/toApi';
import { tables } from 'src/common/constants/tables';

@Injectable()
export class AccountRepository {
    constructor(
        @InjectRepository(Account)
        private readonly accountRepository: Repository<Account>,

    ) { }

    async findOne(account: Account) {
        try {
            const data = await this.accountRepository.findOneBy({ id: account?.id })
            if (data?.id == undefined) {
                return toApi({ data, success: false, message: "we didn't find this account" })
            } else {
                return toApi({ data, success: true, message: "we find this account successfully" })
            }
        } catch (error) {
            console.log("findOne Error : ", error)
            return toApi({ data: null, success: false, message: "Account Repository findOne Error" })

        }
    }

    async deleteOne(account: UpdateAccountDto) {
        try {

            const data = await this.accountRepository.createQueryBuilder(tables.accounts._)
                .delete()
                .from(Account)
                .where("id = :id", { id: account.id })
                .execute()

            return toApi({ data, success: true, message: "we deleted this account successfully" })
        }

        catch (error) {
            console.log("deleteOne Error : ", error)
            return toApi({ data: null, success: false, message: "Account Repository deleteOne Error" })
        }
    }
}