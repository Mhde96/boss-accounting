import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FinancialCycle } from './financialcycles.entity';
import { FinancialCycleController } from './financialcycles.controller';
import { FinancialCycleService } from './financialcycles.service';
import { UsersModule } from 'src/modules/users/users.module';
import { Journal } from 'src/modules/journals/journals.entity';
import { JournalService } from 'src/modules/journals/journals.service';
import { EntryService } from 'src/modules/entries/entries.services';
import { Entry } from 'src/modules/entries/entries.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([FinancialCycle, Journal, Entry]),
    UsersModule,
  ],
  controllers: [FinancialCycleController],
  providers: [FinancialCycleService, JournalService, EntryService],
})
export class FinancialCyclesModule {}
