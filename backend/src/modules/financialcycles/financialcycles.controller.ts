import {
  Controller,
  Post,
  Body,
  UseGuards,
  Request,
  Headers,
} from '@nestjs/common';
import { FinancialCycleService } from './financialcycles.service';
import { AuthGuard } from 'src/modules/users/auth.guard';
import { CreateFinancialCycleDto } from './dto/create-fanancialcycle-dto';
import { toApi } from 'src/common/helper/toApi';
@Controller()
export class FinancialCycleController {
  constructor(private readonly financialcycleService: FinancialCycleService) { }

  @UseGuards(AuthGuard)
  @Post('/add_financialcycle')
  async add(@Body() request: CreateFinancialCycleDto) {
    let params = request;
    let isTrue = true;
    let data = null;

    isTrue = await this.financialcycleService.checkDateIsNextDate(request);

    if (isTrue) {
      data = await this.financialcycleService.add(params);
      return toApi({ data, success: true });
    } else return toApi({ data: null, success: false, message: 'data old' });
  }

  @UseGuards(AuthGuard)
  @Post('/fetch_financialcycles')
  async fetch(@Request() req) {
    const map_id = req['headers']?.map_id;
    let data = await this.financialcycleService.fetchAll(map_id);
    return toApi({ data, success: true, message: 'Success' });
  }

  @UseGuards(AuthGuard)
  @Post('/delete_finacialcycle')
  async delete(@Request() req, @Body() body) {
    const user = req.user;
    const id = body.id

    if (id == undefined) {
      return toApi({ data: [], success: false, message: 'You must send id ' });

    }

    await this.financialcycleService.delete(id, user.id);
    return toApi({ data: [], success: true, message: 'Success' });
  }
}
