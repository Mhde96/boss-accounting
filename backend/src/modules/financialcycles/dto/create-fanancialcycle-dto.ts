import { IsDefined, IsNotEmpty } from 'class-validator';

export class CreateFinancialCycleDto {

  @IsDefined()
  readonly map_id: number;

  @IsDefined()
  readonly start_at: string | Date;

  @IsDefined()
  readonly end_at: string | Date;


}
