import { PartialType } from '@nestjs/mapped-types';
import { CreateFinancialCycleDto } from './create-fanancialcycle-dto';
import { IsDefined } from 'class-validator';

export class UpdateFinancialCycleDto extends PartialType(CreateFinancialCycleDto) {

    @IsDefined()
    readonly id: number

}

