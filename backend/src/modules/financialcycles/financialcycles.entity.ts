import { Journal } from 'src/modules/journals/journals.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'financialcycles' })
export class FinancialCycle {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  map_id: number;

  @Column({ type: 'date' })
  start_at: Date;

  @Column({ type: 'date' , default: null })
  end_at: Date;

  //   @OneToMany(() => Journal, (journal) => journal.financialcycle_id)
  //   journals: Journal[];
}
