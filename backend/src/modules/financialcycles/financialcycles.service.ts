import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FinancialCycle } from './financialcycles.entity';
import { Repository } from 'typeorm';
import { CreateFinancialCycleDto } from './dto/create-fanancialcycle-dto';
import * as moment from 'moment';
import { Journal } from 'src/modules/journals/journals.entity';
import { JournalService } from 'src/modules/journals/journals.service';
@Injectable()
export class FinancialCycleService {
  constructor(
    @InjectRepository(FinancialCycle)
    private readonly financialcycleReposiroty: Repository<FinancialCycle>,

    // @InjectRepository(Journal)
    // private readonly journalRepository: Repository<Journal>,
    private readonly journalService: JournalService,
  ) { }

  async fetchAll(map_id: number) {
    let data = await this.financialcycleReposiroty.find({
      where: { map_id }
    });
    return data;
  }

  async add(financialcycle: CreateFinancialCycleDto) {
    let data = await this.financialcycleReposiroty.save(financialcycle);
    return data;
  }
  async update() { }
  async delete(
    id: number | string | undefined | any,
    user_id: number,
  ): Promise<void> {
    const financialcycle_id = id;

    this.journalService.deleteAllJournal({
      financialcycle_id,
      user_id,
    });

    await this.financialcycleReposiroty.delete(id);
  }
  async deleteAll(map_id: number) {
    let financialcycleReposiroty = await this.fetchAll(map_id)
    financialcycleReposiroty.map(async (item) => await this.financialcycleReposiroty.delete(item.id))
    return financialcycleReposiroty

  }
  async checkDateIsNextDate(request: CreateFinancialCycleDto) {
    let isTrue = false;
    let data = await this.financialcycleReposiroty.find({
      where: { map_id: request.map_id },
    });

    if (data.length > 0) {
      let lastIndex = data.length - 1;
      let endAtLastFinancialCycle = moment(data[lastIndex].end_at).valueOf();
      let startAtNewFinancialCycle = moment(request.start_at).valueOf();

      if (startAtNewFinancialCycle > endAtLastFinancialCycle) isTrue = true;
    } else if (data.length == 0) isTrue = true;

    return isTrue;
  }

  async checkIsFinancialCyclesRelatedToMap(map_id: number) {
    let check = false;
    let data = await this.financialcycleReposiroty.findOne({
      where: { map_id },
    });
    if (data.id > 0) check = true;

    return check;
  }
}
