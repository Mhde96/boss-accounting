import { HttpException, HttpStatus } from '@nestjs/common';
interface ToApiType {
  data: any;
  success: boolean;
  message?: string;
}
export const toApi = ({ data, success, message }: ToApiType) => {

  if (message == undefined) message = 'Success';
  return { data, success, message };
};

interface ExceptionType {
  message: string;
  status?: number;
}
export const Exception = ({ message, status }: ExceptionType) => {
  if (status) {
    throw new HttpException({ message }, status);
  }
  throw new HttpException({ message }, HttpStatus.FORBIDDEN);

  // return toApi({
  //   message: message,
  //   data: null,
  //   success: false
  // })
  // if (status) {
  //   throw new HttpException(
  //     {
  //       status: status,
  //       error: message,
  //       success: false,
  //     },
  //     HttpStatus.FORBIDDEN,
  //     {
  //       // cause: error
  //     },
  //   );
  // }
  // throw new HttpException(
  //   {
  //     // status: HttpStatus.FORBIDDEN,
  //     error: message,
  //     success: false,
  //   },
  //   HttpStatus.FORBIDDEN,
  //   {
  //     // cause: error
  //   },
  // );
};
