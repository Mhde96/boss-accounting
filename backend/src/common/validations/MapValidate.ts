const MapValidate = async (user, map_id, mapService) => {
    const maps = await mapService.findAll(user)

    let isMap = false
    
    isMap = maps.some(item => item.id == map_id)
    
    if (isMap == false) { return true }
    else return false
}

export { MapValidate }