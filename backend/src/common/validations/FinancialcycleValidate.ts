import { FinancialCycleService } from "src/modules/financialcycles/financialcycles.service"
import { toApi } from "src/common/helper/toApi"
import { MapService } from "src/modules/maps/maps.service"

const FinancialcycleValidate = async (
    // userService, financialcycleService,
    financialcycleService: FinancialCycleService,
    mapService: MapService, user, financialcycle_id, map_id
) => {
    // Acceptance Criteria
    // Verify if the map_id and financialcycle_id are associated with the current user
    // Check whether the financialcycle is related to the specified map which is pass it as parameters
    // - return true means the condation will execute and in our case we don't need to execute it 

    if (map_id == undefined) return true
    if (financialcycle_id == undefined) return true

    let isMap = false

    const maps = await mapService.findAll(user)

    isMap = maps.some(item => item.id == map_id)
    if (isMap == false) { return true }


    const financialcycles = await financialcycleService.fetchAll(map_id)


    const data = financialcycles?.some(item => item.id == financialcycle_id)

    if (data == true) {
        return false
    }
    else return true


}


export { FinancialcycleValidate }