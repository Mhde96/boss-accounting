// import dotenv from 'dotenv'

// export class DatabaseConfig {
//   static readonly TYPE = 'mysql';
//   static readonly PORT: number = 3306; // parseInt(process.env.PORT) || 3307;
//   static readonly HOST = '127.0.0.1'; //  || process.env.HOST || String();
//   static readonly USERNAME = 'root'; // process.env.USERNAME || String();
//   static readonly PASSWORD = '0965527892@Mohamd'; // process.env.PASSWORD || String();
//   static readonly DATABASENAME = 'boss'; // process.env.DATABASENAME || String();
// }

export const DatabaseConfig = {
  TYPE: 'mariadb',
  PORT: 3306, // parseInt(process.env.PORT) || 3307;
  HOST: process.env.DB_HOST, //  || process.env.HOST || String();
  USERNAME: process.env.DB_USERNAME, // process.env.USERNAME || String();
  PASSWORD: process.env.PASSWORD, // process.env.PASSWORD || String();
  DATABASENAME: process.env.DB_NAME //'backend_simple_boss_test' ; // process.env.DATABASENAME || String();
}

// export const DatabaseConfig = {
//   TYPE:'mariadb',
//   PORT:'3306',
//   HOST:'127.0.0.1',
//   USERNAME:'root',
//   PASSWORD:'',
//   DATABASENAME:'backend_simple_boss_test'
// }
// console.log(process.env)
// console.log(process.env.NODE_ENV)
// console.log(process.env.test)


// export class DatabaseConfig {
//   static readonly TYPE = process.env.NODE_ENV == 'dev' ? 'mariadb' : 'mysql';
//   static readonly PORT: number = 3306; // parseInt(process.env.PORT) || 3307;
//   static readonly HOST = '127.0.0.1'; //  || process.env.HOST || String();
//   static readonly USERNAME = 'root'; // process.env.USERNAME || String();
//   static readonly PASSWORD = '' // process.env.PASSWORD || String();
//   static readonly DATABASENAME = process.env.NODE_ENV == 'dev' ? 'backend_simple_boss_test' : 'boss'; // process.env.DATABASENAME || String();
// }



export class EmailConfig {
  static readonly DOMAIN = 'info@boss-accounting.com';
  static readonly PASSWORD = '0965527892@Mohamd';
  static readonly HOST = 'smtpout.secureserver.net';
  static readonly PORT: number = 465;
  static readonly DIR: string = process.cwd() + '/src/mails/template/';
}

// export class PasswordConfig {
//   static readonly ROUNDS = 10;
//   static readonly AUTH_SECRET_KEY = 'secretKey';
// }
// export class MediaConfig {
//   static readonly FORDER_FILE = 'media';
//   static readonly FORDER_FILE_PUBLIC = '/public';
//   static readonly FORDER_FILE_PRIVATE = '/private';
//   static readonly FORDER_FILE_PUBLIC_ROOT = 'media/public';
// }
// export class Config {
//   static readonly PRODUCT = false;
//   static readonly PORT = process.env.port || 3000;
//   static readonly CACHE_MAXAGE = 36000; // cache 10 hours
// }
// export class LoggerConfig {
//   static readonly FOLDER = 'logs';
//   static readonly ERROR_FILE = 'error.log';
//   static readonly INFO_FILE = 'info.log';
//   static readonly FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss';
//   static readonly LEVEL_ERROR = 'error';
//   static readonly LEVEL_INFO = 'info';
//   static readonly LEVEL_WARNING = 'warn';
// }
