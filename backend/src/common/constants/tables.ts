export const tables = {
    users: {
        _: 'users',
        name: 'name',
        email: 'email',
        created_at: 'created_at',
        updated_at: 'updated_at'
    },
    maps: {
        _: 'maps',
        name: 'name',
        description: 'description',
        userId: 'userId'
    },
    financialcycles: {
        _: "financialcycles",
        id: 'id',
        map_id: 'map_id',
        start_at: 'start_at',
        end_at: 'end_at'
    },
    accounts: {
        _: 'accounts',
        id: "id",
        name: 'name',
        financial_statement: "financial_statement",
        map_id: "map_id",
        userId: 'userId'
    },
    journals: {
        _: 'journals',
        userId: 'userId',
        description: 'description',
        date: 'date',
        number: 'number',
        financialcycle_id: 'financialcycle_id',
        mapId: 'mapsId',

    },
    entries: {
        _: 'entries',
        id: 'id',
        description: 'description',
        accountId: 'accountId',
        debit: 'debit',
        credit: 'credit',
        journalId: 'journalId',
    },



}