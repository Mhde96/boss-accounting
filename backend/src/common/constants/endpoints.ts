export const endpoints = {

    users: {
        login: 'login',
        register: 'register',
        refreshToken: 'refreshToken',
        forgot_password: 'forgot_password'
    },

    journals: {
        add: 'add_journal',
        fetchAll: 'fetch_journals',
        update: 'update_journal',
        delete: 'delete_journal',

    },


    maps: {
        fetchAll: 'fetch_maps',
        create: 'create_map',
        update: 'update_map',
        delete: 'delete_map',
        change_current_map: 'change_current_map',
    },

    accounts: {
        add: 'add_account',
        create: 'add_account',
        fetch: 'fetch_accounts',
        find_account_by_id: "find_account_by_id",
        delete: 'delete_account',
        dummy: 'generate_dummy_data',
    },
    financialcycles: {
        fetch: "fetch_financialcycles",
        add: "add_financialcycle",
        delete: "delete_finacialcycle",
    }

}