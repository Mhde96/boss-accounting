import { Module, Global } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './modules/users/users.module';
import { User } from './modules/users/user.entity';
import { MailModule } from './modules/mails/mail.module';
import { DatabaseConfig } from './common/constants';

import { ConfigModule } from '@nestjs/config';
import { Entry } from './modules/entries/entries.entity';
import { Journal } from './modules/journals/journals.entity';
import { Account } from './modules/accounts/accounts.entity';
import { AccountsModule } from './modules/accounts/account.module';
import { JournalsModule } from './modules/journals/journal.module';
import { MapsModule } from './modules/maps/maps.module';
import { FinancialCyclesModule } from './modules/financialcycles/financialcycles.module';
import { UsersAdminModule } from './modules/admin/users/users.admin.module';


// const envFilePath = process.env.PWD == "/var/www/boss-accounting/backend/dist" ? "production" : process.env.NODE_ENV


const envFilePath = () => {
  console.log(1)
  console.log(process.env.NODE_ENV)
  if (process.env.PWD == "/var/www/boss-accounting/backend/dist"){
    console.log("production")
   return "production" 
  }
 
  else return process.env.NODE_ENV }



@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [`env/${envFilePath()}.env`],
    }),


    TypeOrmModule.forRoot({
      type: "mariadb",
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [User, Account, Journal, Entry],
      synchronize: true,
      autoLoadEntities: true,
      dropSchema: process.env.DB_DROP_SCHEMA == "1" ? true : false,
    }),

    // TypeOrmModule.forRoot({
    //   type: "mariadb",
    //   host: process.env.DB_HOST, //  '64.227.120.186', // 64.227.120.186/
    //   port: parseInt(process.env.DB_PORT),
    //   username: process.env.DB_USERNAME, // 'phpmyadmin',
    //   password: process.env.DB_PASSWORD, // '0965527892@Mohamd', // 0965527892@Mohamd
    //   database: process.env.DB_NAME, // boss
    //   entities: [User, Account, Journal, Entry],
    //   synchronize: true,
    //   autoLoadEntities: true,
    //   // dropSchema:true
    //   // dropSchema: Boolean(process.env.DB_DROP_SCHEMA),
    // }),

    MailModule,
    UsersModule,
    UsersAdminModule,
    AccountsModule,
    JournalsModule,
    MapsModule,
    FinancialCyclesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {


    console.log({ envFilePath })
    console.log("process.env.NODE_ENV : ", process.env.NODE_ENV)
    console.log("process.env.PWD : ", process.env.PWD)
    console.log("DB_HOST : ", process.env.DB_HOST)
    console.log("DB_PASSWORD : ", process.env.DB_PASSWORD)
    console.log("DB_DROP_SCHEMA : ", process.env.DB_DROP_SCHEMA == "1" ? true : false)
    console.log("DB_DROP_SCHEMA : ", process.env.DB_DROP_SCHEMA)
    console.log("ONLY_PRODUCTION : ", process.env.ONLY_PRODUCTION)

    // console.log(process.env.DB_HOST)
  }
}
