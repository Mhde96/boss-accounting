import * as request from 'supertest';
import { BASE_URL_TEST, setUserTest, testMessage } from '../data';
import { mockUser__test } from '../mock/users__mock';
import { endpoints } from '../../src/common/constants/endpoints';
import { setUser__test } from '../utility/usre__test__utility';

let BASE_URL = BASE_URL_TEST;
let token = '';

export const testUser = () => {

  describe('TEST~~E2E~~REGISTER', () => {
    const email = mockUser__test.email;
    const password = mockUser__test.password;
    const name = 'Mohamd';

    let mockUser = mockUser__test

    it('should create new user ( Happy Senario )', async () => {
      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != true) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })
      }

      if (response.body.success == true) {
        setUser__test(response.body.data)
        expect(response.body.success).toEqual(true);
      }
    });

    it('should return email must be existed', async () => {
      delete mockUser.email;
      mockUser.name = name;
      mockUser.password = password;

      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {

        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })

      }

      expect(response.body.success).toEqual(false);
    });

    it('should return this email used before', async () => {
      mockUser.email = email;
      mockUser.password = password;
      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })
      }

      expect(response.body.success).toEqual(false);
    });

    it('should return password must be existed', async () => {
      delete mockUser.password;
      mockUser.email = email;
      mockUser.name = name;

      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })

      }

      expect(response.body.success).toEqual(false);
    });

    it('should return name must be existed', async () => {
      delete mockUser.name;

      mockUser.email = email;
      mockUser.password = password;

      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })
      }

      expect(response.body.success).toEqual(false);
    });

    it('should return this email used befor', async () => {
      delete mockUser.name;

      mockUser.email = email;
      mockUser.password = password;

      const response = await request(BASE_URL)
        .post(endpoints.users.register)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.register
        })
      }

      expect(response.body.success).toEqual(false);
    });
  });

  describe('TEST~~E2E~~LOGIN', () => {
    let mockUser = mockUser__test

    it('should login ', async () => {
      const response = await request(BASE_URL)
        .post(endpoints.users.login)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success) {
        setUserTest(response.body.data);
        setUser__test(response.body.data)
        token = response.body.data.token;
      }

      if (response.body.success != true) {

        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.login
        })

      }

      expect(response.body.success).toEqual(true);
    });

    it('should return password not correct', async () => {
      mockUser.email = 'm.mhde96@hotmail.com';
      mockUser.password = '112233zxczxc';
      const response = await request(BASE_URL)
        .post(endpoints.users.login)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.login
        })
      }

      expect(response.body.success).toEqual(false);
    });

    it('should return email not found', async () => {
      mockUser.email = 'm.mhde96@hottmail.com';
      mockUser.password = '11223344';
      const response = await request(BASE_URL)
      .post(endpoints.users.login)
        .set('Accept', 'application/json')
        .send(mockUser);

      if (response.body.success != false) {
        testMessage({
          mockData: mockUser,
          response,
          url: endpoints.users.login
        })
      }

      expect(response.body.success).toEqual(false);
    });
  });

  describe('TEST~~E2E~~REFRESH~~TOKEN', () => {

    it('should return correct user data', async () => {
      const response = await request(BASE_URL)
        .post(endpoints.users.refreshToken)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${token}`);

      if (response.body.success) {
        token = response.body.token;
      }

      if (response.body.success != true) {
        testMessage({
          mockData: null,
          response,
          url: endpoints.users.refreshToken
        })

      }

      expect(response.body.success).toEqual(true);
    });

    it('should return token is invalid', async () => {
      token = 'asdx';

      const response = await request(BASE_URL)
        .post(endpoints.users.refreshToken)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${token}`);

      if (response.body.success != false) {
        testMessage({
          mockData: null,
          response,
          url: endpoints.users.refreshToken
        })
      }

      expect(response.body.success).toEqual(false);
    });
  });

  describe('TEST~~E2E~~TEST~~Forgot~~Password', () => {
    let url = endpoints.users.forgot_password + '/';

    // it('should sent email to user', async () => {
    //   let newUrl = url + 'm.mhde96@gmail.com';

    //   const response = await request(BASE_URL)
    //     .get(newUrl)
    //     .set('Accept', 'application/json');

    //   if (response.body.success != true) {
    //     console.log('url => ', url);
    //     console.log(response.error);
    //   }

    //   expect(response.body.success).toEqual(true);
    // });

    it('should return this user not exist', async () => {
      url = url + 'abcd@abcd.com';
      const response = await request(BASE_URL)
        .get(url)
        .set('Accept', 'application/json');

      if (response.body.success != false) {
        console.log('url => ', url);
        console.log(response.error);
      }

      expect(response.body.success).toEqual(false);
    });
  });


}