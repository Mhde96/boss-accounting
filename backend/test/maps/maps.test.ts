import * as request from 'supertest';
import {
  BASE_URL_TEST,
  testMessage,
} from '../data';
import { endpoints } from '../../src/common/constants/endpoints'
import { loginUser__test, user__test, } from '../utility/usre__test__utility';
import { mockMap } from '../mock/maps__mock'
import { firstMap__test, lastMap__test, setMaps__test } from '../utility/maps__test__utility';
const BASE_TEST = BASE_URL_TEST
export const testMaps = () => {

  describe('TEST~~E2E~~MAPS', () => {
    beforeAll(async () => {
      await loginUser__test()
    })


    it('should create new map ', async () => {
      const response = await request(BASE_TEST)
        .post(endpoints.maps.create)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .send({ ...mockMap, user: user__test, });

      if (response.body.success != true)
        testMessage({
          response,
          url: endpoints.maps.create,
          mockData: mockMap,
        });
      expect(response.body.data.name).toEqual(mockMap.name);
    });

    it('should fetch all maps', async () => {
      const response = await request(BASE_TEST)
        .post(endpoints.maps.fetchAll)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`);

      if (response.body.success != true)
        testMessage({
          response,
          url: endpoints.maps.fetchAll,
          mockData: null,
        });
      if (response.body.success) setMaps__test(response.body.data);

      expect(response.body.success).toEqual(true);
      expect(response.body.data.length).toBeGreaterThan(0);
    });

    it('should update map ', async () => {
      const updatedName = 'mohamd edit';
      mockMap.name = updatedName;
      mockMap.description = 'update description';
      mockMap['id'] = firstMap__test().id;
      const response = await request(BASE_TEST)
        .post(endpoints.maps.update)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .send(mockMap);

      if (response.body.success != true)
        testMessage({
          response,
          url: endpoints.maps.create,
          mockData: mockMap,
        });

      expect(response.body.data.name).toEqual(updatedName);
    });



    it('Should delete map we had added before ( Happy Scenario )', async () => {

      const response = await request(BASE_TEST)
        .post(endpoints.maps.delete)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)

        .send(lastMap__test());

      if (response.body.success != true)
        testMessage({
          response,
          url: endpoints.maps.delete,
          mockData: lastMap__test(),
        });

      expect(response.body.success).toEqual(true);

      const responseGetAllFinancialcycle = await request(BASE_TEST)
        .post(endpoints.financialcycles.fetch)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", lastMap__test().id)


      expect(responseGetAllFinancialcycle.body.data.length).toEqual(0)
    });
  });
};
