import { BASE_URL_TEST, testMessage } from "../data";
import { InsertDummyAccounts__test } from "../utility/accounts__test__utility";
import { firstMap__test, getUserMaps__test } from "../utility/maps__test__utility";
import { loginUser__test, user__test } from "../utility/usre__test__utility";
import * as request from 'supertest';
import { endpoints } from "../../src/common/constants/endpoints";
import { financialcycleMock } from "../mock/financialcycles__mock";
import { getFinancialcycle__test, lastFinancialcycle__test } from "../utility/financialcycles__test__utility";

export const testFinancialCycle = () => {

    describe('TEST~~E2E~~FINANCIAL~~CYCLE', () => {
        beforeAll(async () => {
            await loginUser__test()
            await getUserMaps__test()
            await InsertDummyAccounts__test()
        });


        it('Should create new financial cycle ( Happy Scenario )', async () => {
            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(financialcycleMock())

            if (response.body.success != true) {
                testMessage({
                    mockData: financialcycleMock(),
                    response,
                    url: endpoints.financialcycles.add
                })
            }


            expect(response.body.success).toEqual(true);
        })

        it('Should return you must send start_at date ', async () => {
            let mock = financialcycleMock()
            delete mock.start_at

            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(mock)

            if (response.body.success == true) {
                testMessage({
                    mockData: financialcycleMock(),
                    response,
                    url: endpoints.financialcycles.add
                })
            }


            expect(response.body.success).toEqual(false);
        })

        it('Should return you must send end_at date ', async () => {

            let mock = financialcycleMock()
            delete mock.end_at

            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(mock)

            if (response.body.success == true) {
                testMessage({
                    mockData: financialcycleMock(),
                    response,
                    url: endpoints.financialcycles.add
                })
            }


            expect(response.body.success).toEqual(false);
        })

        it('Should return you must send map_id date ', async () => {


            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .send(financialcycleMock())

            if (response.body.success == true) {
                testMessage({
                    mockData: financialcycleMock(),
                    response,
                    url: endpoints.financialcycles.add
                })
            }


            expect(response.body.success).toEqual(false);

        })

        it('should return you must send id of financial cycle ', async () => {
            await getFinancialcycle__test()

            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.delete)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(financialcycleMock())

                if (response.body.success == true) {
                    testMessage({
                        mockData: financialcycleMock(),
                        response,
                        url: endpoints.financialcycles.delete
                    })
                }

                expect(response.body.success).toEqual(false);
        })

        it('Should delete financial cycle ', async () => {

            await getFinancialcycle__test()

            const response = await request(BASE_URL_TEST)
                .post(endpoints.financialcycles.delete)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(lastFinancialcycle__test())


            if (response.body.success != true) {
                testMessage({
                    mockData: financialcycleMock(),
                    response,
                    url: endpoints.financialcycles.delete
                })
            }


            expect(response.body.success).toEqual(true);

        })

    })
}