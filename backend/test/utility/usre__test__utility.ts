import { endpoints } from '../../src/common/constants/endpoints';
import * as request from 'supertest';
import { BASE_URL_TEST } from '../data';

import { mockUser__test } from "../mock/users__mock";

export let user__test: any = {}
export const getUser__test = () => user__test
export let setUser__test = (user) => user__test = user

export const loginUser__test = async () => {
    const response = await request(BASE_URL_TEST)
        .post(endpoints.users.login)

        .set('Accept', 'application/json')
        .send(mockUser__test);

    if (response.body.success) {
        setUser__test(response.body.data)
    }


    return user__test
}
