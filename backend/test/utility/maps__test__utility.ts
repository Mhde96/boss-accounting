import * as request from 'supertest';

import { endpoints } from '../../src/common/constants/endpoints';
import { BASE_URL_TEST, testMessage } from '../data';
import { loginUser__test, user__test } from './usre__test__utility';
import { setFinancialcycles__test } from './financialcycles__test__utility';

export let maps__test: any = [];
export const setMaps__test = (data) => (maps__test = data);

export const firstMap__test = () => {
    return maps__test[0]
}
export const lastMap__test = () => {
    const length = maps__test.length - 1
    return maps__test[length]
}

export const getUserMaps__test = async () => {
    
    const response = await request(BASE_URL_TEST)
        .post(endpoints.maps.fetchAll)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)


    if (response.body.success) {
        setMaps__test(response.body.data)
        setFinancialcycles__test(firstMap__test().financialcycles)
    }



    else {
        testMessage({
            mockData: null,
            response: response,
            url: endpoints.maps.fetchAll
        })
    }


    return maps__test
}