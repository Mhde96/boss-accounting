import { BASE_URL_TEST } from "../data";
import { endpoints } from "../../src/common/constants/endpoints";
import { accountsMock } from "../mock/accounts__mock";

import * as request from 'supertest';
import { user__test } from "./usre__test__utility";
import { firstMap__test } from "./maps__test__utility";


export let accounts__test = [];
export const getAccounts__test = () => accounts__test;
export let setAccount__test = (data) => (accounts__test = data);


export const InsertDummyAccounts__test = async () => {
    let INSERT = () => {
        return accountsMock.map(async (item) => {
            const response = await request(BASE_URL_TEST)
                .post(endpoints.accounts.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(item);
            return response.body.data;
        });
    }
    const promisData = await Promise.all(INSERT());
    setAccount__test(promisData);
}


export const fetchAllAccounts__test = async () => {
    let response = await request(BASE_URL_TEST).post(endpoints.accounts.fetch)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)

        setAccount__test(response.body.data);
}