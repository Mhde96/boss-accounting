import * as request from 'supertest';

import { endpoints } from "../../src/common/constants/endpoints";
import { BASE_URL_TEST, testMessage } from "../data";
import { user__test } from "./usre__test__utility";
import { firstMap__test } from './maps__test__utility';


export let financialcycles__test = []
export const firstFinancialcycle__test = () => financialcycles__test[0]
export const setFinancialcycles__test = (data) => financialcycles__test = data
export const lastFinancialcycle__test = () => {
    const length = financialcycles__test.length
    return financialcycles__test[length - 1]
}

export const getFinancialcycle__test = async () => {
    const response = await request(BASE_URL_TEST)
        .post(endpoints.financialcycles.fetch)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)



    setFinancialcycles__test(response.body.data)


}