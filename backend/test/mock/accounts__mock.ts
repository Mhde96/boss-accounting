export let accountsMock = [
  {
    name: 'Box1',
    financial_statement: 0,
    user: undefined,
  },
  {
    name: 'Box2',
    financial_statement: 0,
    user: undefined,
  },
  {
    name: 'Box3',
    financial_statement: 1,
    user: undefined,
  },
  {
    name: 'Box4',
    financial_statement: 1,
    user: undefined,
  },
  {
    name: 'Box5',
    financial_statement: 0,
    user: undefined,
  },
];
