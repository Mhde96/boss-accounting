import { CraeteJournalDto } from "../../src/modules/journals/dto/create-journal-dto";
import { accounts__test, getAccounts__test } from "../utility/accounts__test__utility";
import { user__test } from "../utility/usre__test__utility";

export const JournalMock = (): CraeteJournalDto => {
    return {
        user: user__test,
        date: new Date(),
        description: 'test',
        user_id: user__test.id,
        entries: [
            {
                account: getAccounts__test()[2],
                credit: 0,
                debit: 2000,
                description: 'test',
                user_id: user__test.id,
                journal_id: undefined,
            },
            {
                account: getAccounts__test()[3],
                credit: 2000,
                debit: 0,
                description: 'test',
                user_id: user__test.id,
                journal_id: undefined,
            },
        ],
    }
};