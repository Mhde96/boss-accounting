import { firstMap__test } from "../utility/maps__test__utility"

export const financialcycleMock = () => {
    return {
        map_id: firstMap__test().id,
        start_at: "2024-11-14",
        end_at: "2024-11-17"
    }
}