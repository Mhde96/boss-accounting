import * as request from 'supertest';
import { BASE_URL_TEST, setJournal, testMessage } from '../data';
import { loginUser__test, user__test } from '../utility/usre__test__utility';
import { endpoints } from '../../src/common/constants/endpoints';
import { InsertDummyAccounts__test, accounts__test, fetchAllAccounts__test, getAccounts__test } from '../utility/accounts__test__utility';
import { firstMap__test, getUserMaps__test } from '../utility/maps__test__utility';
import { firstFinancialcycle__test } from '../utility/financialcycles__test__utility';
import { JournalMock } from '../mock/journals__mock';

export const testJournal = () => {
  let BASE = BASE_URL_TEST;

  describe('TEST~~E2E~~JOURNALS', () => {
    beforeAll(async () => {
      await loginUser__test()
      await getUserMaps__test()
      await InsertDummyAccounts__test()
      await fetchAllAccounts__test()
    });

    it('Should create new journal ( Happy Scenario )', async () => {
      const response = await request(BASE_URL_TEST)
        .post(endpoints.journals.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)
        .send(JournalMock());

      if (response.body.success != true) {
        testMessage({
          mockData: JournalMock(),
          response,
          url: endpoints.journals.add
        })
      }

      expect(response.body.success).toEqual(true);
    });

    it('Should return map_id must be existed', async () => {

      const response = await request(BASE)
        .post(endpoints.journals.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("financialcycle_id", firstFinancialcycle__test().id)
        .send(JournalMock());

      if (response.body.success != false) {
        testMessage({
          mockData: JournalMock(),
          response: response.body,
          url: endpoints.journals.add
        })
      }

      expect(response.body.success).toEqual(false);
    });

    it('Should return financialcycle_id must be existed', async () => {

      const response = await request(BASE)
        .post(endpoints.journals.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(JournalMock());

      if (response.body.success != false) {
        testMessage({
          mockData: JournalMock(),
          response: response.body,
          url: endpoints.journals.add
        })
      }

      expect(response.body.success).toEqual(false);
    });

    it('Should return total debit not equale total credit ', async () => {

      let journalMock = JournalMock()
      journalMock.entries[0].credit = 333
      const response = await request(BASE)
        .post(endpoints.journals.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)
        .send(journalMock);

      if (response.body.success != false) {
        testMessage({
          mockData: JournalMock(),
          response,
          url: endpoints.journals.add
        })
      }


      expect(response.body.success).toEqual(false);
    })




    it('Should update journal details ', async () => {
      const fetchJournals = await request(BASE_URL_TEST)
        .post(endpoints.journals.fetchAll)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test['token']}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)


      let journal = fetchJournals.body.data[0]

      journal.description = 'new test';
      journal['user'] = user__test;

      const response = await request(BASE_URL_TEST)
        .post(endpoints.journals.update)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test['token']}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)
        .send(journal);

      if (response.body.success != true) {
        testMessage({
          mockData: journal,
          response,
          url: endpoints.journals.update
        })
      }

      if (response.body.success) {
        setJournal(response.body.data);
      }
      expect(response.body.success).toEqual(true);
    });

    it('Should delete journal ', async () => {

      const fetchJournals = await request(BASE_URL_TEST)
        .post(endpoints.journals.fetchAll)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test['token']}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)


      let journal = fetchJournals.body.data[0]

      const response = await request(BASE)
        .post(endpoints.journals.delete)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test['token']}`)
        .set("map_id", firstMap__test().id)
        .set("financialcycle_id", firstFinancialcycle__test().id)
        .send(journal);

      if (response.body.success != true) {
        testMessage({
          mockData: journal,
          response,
          url: endpoints.journals.delete
        })
      }


      expect(response.body.success).toEqual(true);
    });
  });
};