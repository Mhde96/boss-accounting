import * as request from 'supertest';
import { BASE_URL_TEST, testMessage } from '../data';
import { getUser__test, loginUser__test, user__test } from '../utility/usre__test__utility';
import { endpoints } from '../../src/common/constants/endpoints';
import { firstMap__test, getUserMaps__test } from '../utility/maps__test__utility';

let BASE_URL = BASE_URL_TEST;

let account = {
  id: undefined,
  name:'asdasda'
};
let setAccount = (data) => {
  account = data;
};

export const testAccount = async () => {
  describe('Test~~E2E~~ACCOUNTS', () => {

    beforeAll(async () => {
      await loginUser__test()
      await getUserMaps__test()
    });

    let mockAccount = {
      name: 'Box',
      financial_statement: 0,
      user: getUser__test(),
      map: firstMap__test()
    };

    test('should return create new account ( Happy Senario )', async () => {

      const response = await request(BASE_URL)
        .post(endpoints.accounts.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(mockAccount);

      if (response.body.success != true) {
        testMessage({
          mockData: mockAccount,
          response,
          url: endpoints.accounts.add
        })
      }
      
      if (response.body.success) setAccount(response.body.data);

      expect(response.body.success).toEqual(true);

      const checkAccountIsExistedInDatabase = await request(BASE_URL_TEST)
        .post(endpoints.accounts.find_account_by_id)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(account)

      expect(checkAccountIsExistedInDatabase.body?.data?.id).toEqual(account.id);
    });

    it('should return this account alrady exist for this user', async () => {

      mockAccount.user = user__test;
      const response = await request(BASE_URL)
        .post(endpoints.accounts.add)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(mockAccount);

      if (response.body.success != false) {
        testMessage({
          mockData: mockAccount,
          response,
          url: endpoints.accounts.add
        })
      }
      expect(response.body.success).toEqual(false);
    });

    it('should update account we added before ', async () => {
      let update_account = 'update_account';
      account = { ...account, name: 'box2' };
      const response = await request(BASE_URL)
        .post(update_account)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(account);

      if (response.body.success != true) {

        testMessage({
          mockData: account,
          response,
          url: endpoints.accounts.add
        })
      }
      expect(response.body.success).toEqual(true);
    });

    it('should delete account we updated before', async () => {
      const response = await request(BASE_URL_TEST)
        .post(endpoints.accounts.delete)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(account);

      if (response.body.success != true) {
        testMessage({
          mockData: account,
          response,
          url: endpoints.accounts.delete
        })
      }
      expect(response.body.success).toEqual(true);


      const checkAccountIsExistedInDatabase = await request(BASE_URL_TEST)
        .post(endpoints.accounts.find_account_by_id)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${user__test.token}`)
        .set("map_id", firstMap__test().id)
        .send(account)


      expect(checkAccountIsExistedInDatabase.body?.data?.id).not.toEqual(account.id);



    });
  });

}