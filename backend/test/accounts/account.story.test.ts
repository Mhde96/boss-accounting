import * as request from 'supertest';
import { BASE_URL_TEST, testMessage } from '../data';
import { endpoints } from '../../src/common/constants/endpoints';

import { firstMap__test, getUserMaps__test } from '../utility/maps__test__utility';
import { getUser__test, loginUser__test, user__test } from '../utility/usre__test__utility';
import { firstFinancialcycle__test } from '../utility/financialcycles__test__utility';
import { JournalMock } from '../mock/journals__mock';
import { accounts__test } from '../utility/accounts__test__utility';

export const ACCOUNT_STORY = async () => {
    describe("TEST~~E2E~~ACCOUNT~~STORY", () => {
        beforeAll(async () => {
            await loginUser__test()
            await getUserMaps__test()
        })

        it(`Should create account and then add journal 
            include this account then try to delete this account
            and this must not deleted `, async () => {

            const mockData = {
                name: 'Ahmad123',
                user_id: user__test.id,
                financial_statement: 0,
                user: user__test,
                map: firstMap__test()
            }

            const createAccountRespone = await request(BASE_URL_TEST)
                .post(endpoints.accounts.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(mockData)

            expect(createAccountRespone.body.success).toEqual(true);


            let journalMock = JournalMock()
            journalMock.entries[0].account = createAccountRespone.body.data


            const createJournalResponse = await request(BASE_URL_TEST)
                .post(endpoints.journals.add)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .set("financialcycle_id", firstFinancialcycle__test().id)
                .send(journalMock)


            expect(createJournalResponse.body.success).toEqual(true);

            const deleteAccountUsedBefore = await request(BASE_URL_TEST)
                .post(endpoints.accounts.delete)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(createAccountRespone.body.data)


            expect(deleteAccountUsedBefore.body.success).toEqual(false);


            const checkAccountIsExistedInDatabase = await request(BASE_URL_TEST)
                .post(endpoints.accounts.find_account_by_id)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${user__test.token}`)
                .set("map_id", firstMap__test().id)
                .send(createAccountRespone.body.data)


            expect(checkAccountIsExistedInDatabase.body.data.id).toEqual(createAccountRespone.body.data.id);


        })


        it('should try get account from another user by using map_id and financial_id not to the authenticated user', async () => {
            let NewUserMock = {
                id: undefined,
                name: 'Ahmad_test',
                email: 'Ahmad_test@hotmail.com',
                password: '11223344',
                countryCode: "+963",
                phone: "932220737",
                token: undefined,
            }

            const response = await request(BASE_URL_TEST)
                .post(endpoints.users.register)
                .set('Accept', 'application/json')
                .send(NewUserMock);


            if (response.body.success != true) {
                testMessage({
                    mockData: NewUserMock,
                    response,
                    url: endpoints.users.register
                })
            }


            else if (response.body.success) {
                NewUserMock = response.body.data


            }


            const loginResponse = await request(BASE_URL_TEST)
                .post(endpoints.users.login)
                .set('Accept', 'application/json')
                .send({
                    email: NewUserMock.email,
                    password: "11223344"
                });




            // const user__test__account__response = await request(BASE_URL_TEST)
            //     .post(endpoints.accounts.find_account_by_id)
            //     .set('Accept', 'application/json')
            //     .set('Authorization', `Bearer ${user__test.token}`)
            //     .set("map_id", firstMap__test().id)
            //     .send(accounts__test[4])

            // const account1 = user__test__account__response.body.data

            // expect(user__test__account__response.body.success).toEqual(true)


            // console.log(NewUserMock)
            const new_user_account_try_get_data_from_another_user__response = await request(BASE_URL_TEST)
                .post(endpoints.accounts.find_account_by_id)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${loginResponse.body.data.token}`)
                .set("map_id", "3")
                .send(accounts__test[4])

            console.log('new : ', new_user_account_try_get_data_from_another_user__response.body)


            // const account2 = new__user__account__response.body.data


            expect(new_user_account_try_get_data_from_another_user__response.body.success).toEqual(false);


            // console.log(user__test__account__response.body)
            // expect(account2?.id).not.toEqual(account1?.id)

        })

    })
}



