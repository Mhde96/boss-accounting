import { user__test } from "./utility/usre__test__utility";

export const BASE_URL_TEST = 'localhost:5000/';
export const endPointesTest = {

  maps: {
    fetchAll: 'fetch_maps',
    create: 'create_map',
    update: 'update_map',
    delete: 'delete_map',
    change_current_map: 'change_current_map',
  },
  users: {
    refreshToken: 'refreshToken',
  },
  accounts: {

    create: 'add_account',
    fetch: 'fetch_accounts',
    dummy: 'generate_dummy_data',
  }
};

export const testMessage = ({ response, url, mockData }) => {
  let message = {
    url,
    message: response.body?.message,
    success: response.body?.success,
    error: response.error,
    mockData: mockData,
    user: user__test,
    data: response?.body?.data,
  };
  console.log(JSON.stringify(message));
};



export let userTest: any = {};
export let setUserTest = (data) => (userTest = data);

export let journal = {};
export let setJournal = (data) => (journal = data);

export let mapsTest: any = {};
export let setMapsTest = (data) => (mapsTest = data);
