import { testAccount } from './accounts/accounts.test';
import { ACCOUNT_STORY } from './accounts/account.story.test';
import { testFinancialCycle } from './financialcycle/financialcycle.test';
import { testJournal } from './journals/journals.test';
import { testMaps } from './maps/maps.test';
// import { TestAccountSenarioWithMaps } from './senarios/test_account_senario_with_maps';
import { testUser } from './users/users.test';
describe('sequentially run tests', () => {

  testUser();

  testAccount();

  testMaps();

  testFinancialCycle()

  testJournal();

  ACCOUNT_STORY()

});

 