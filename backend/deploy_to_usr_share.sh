# before run this script you should give permition
# chmod +x deploy_to_usr_share.sh
pm2 stop boss

git pull

rm -rf node_modules
rm pnpm-lock.yaml

# remove package-lock.json
if [[ -f "package-lock.json" ]]; then
    rm package-lock.json
    echo "package-lock.json removed successfully."
else
    echo "package-lock.json does not exist."
fi

# remove  yarn.lock 
if [[ -f "yarn.lock" ]]; then
    rm  yarn.lock 
    echo "yarn.lock removed successfully."
else
    echo "yarn.lock does not exist."
fi


npm install 

npm run build 

cp -r env dist 

npm run start:production
# pm2 start dist/main.js --name boss