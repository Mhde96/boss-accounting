'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">nest-boss-accounting documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccountsModule.html" data-type="entity-link" >AccountsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' : 'data-bs-target="#xs-controllers-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' :
                                            'id="xs-controllers-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' }>
                                            <li class="link">
                                                <a href="controllers/AccountController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccountController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' : 'data-bs-target="#xs-injectables-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' :
                                        'id="xs-injectables-links-module-AccountsModule-3675f6cfdaf22bab259bd77ba108e5f89fcd13c2bcc1bdb5cd679aaf2023f350a625474a52d05fb460c6b3ac43c650571fd5bfe74ff46f0459535d4113e54910"' }>
                                        <li class="link">
                                            <a href="injectables/AccountService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccountService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' : 'data-bs-target="#xs-controllers-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' :
                                            'id="xs-controllers-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' : 'data-bs-target="#xs-injectables-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' :
                                        'id="xs-injectables-links-module-AppModule-6ded02c45451e913ceea7f259d3252f4cef9f7e3f8bc0b01e84713ead736c393f292b39f56e3be7ae1f78ce417c878b7a283c0d1e19a44c8bdb81a916c57c03c"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/FinancialCyclesModule.html" data-type="entity-link" >FinancialCyclesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' : 'data-bs-target="#xs-controllers-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' :
                                            'id="xs-controllers-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' }>
                                            <li class="link">
                                                <a href="controllers/FinancialCycleController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FinancialCycleController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' : 'data-bs-target="#xs-injectables-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' :
                                        'id="xs-injectables-links-module-FinancialCyclesModule-3bd722d2c303180a61a5da35da54a7007256648e917b2ddbd134e3d1dfc813f9d33b37234ebf9c2a578d7ff72d207c927763c763d990db80f752aa6fa8ed6a35"' }>
                                        <li class="link">
                                            <a href="injectables/EntryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FinancialCycleService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FinancialCycleService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JournalService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JournalService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/JournalsModule.html" data-type="entity-link" >JournalsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' : 'data-bs-target="#xs-controllers-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' :
                                            'id="xs-controllers-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' }>
                                            <li class="link">
                                                <a href="controllers/JournalController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JournalController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' : 'data-bs-target="#xs-injectables-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' :
                                        'id="xs-injectables-links-module-JournalsModule-58b7521f49e8e622364da5aeb9f1e322e70577d48f0fc22eed9a943ab732d802070191867d899635d84a54faaaf34958e03aca5e9cba4b944f0d0dd6e0972bfc"' }>
                                        <li class="link">
                                            <a href="injectables/EntryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JournalService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JournalService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MailModule.html" data-type="entity-link" >MailModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-MailModule-86a7e74ca843059f19c510eb008e015e8dd1b202117641c9cdac8344efcc15cfd358240daf7e7d693dbeab8d22c7e5e5324bc5f9ef08df206a0b82acf378314b"' : 'data-bs-target="#xs-injectables-links-module-MailModule-86a7e74ca843059f19c510eb008e015e8dd1b202117641c9cdac8344efcc15cfd358240daf7e7d693dbeab8d22c7e5e5324bc5f9ef08df206a0b82acf378314b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MailModule-86a7e74ca843059f19c510eb008e015e8dd1b202117641c9cdac8344efcc15cfd358240daf7e7d693dbeab8d22c7e5e5324bc5f9ef08df206a0b82acf378314b"' :
                                        'id="xs-injectables-links-module-MailModule-86a7e74ca843059f19c510eb008e015e8dd1b202117641c9cdac8344efcc15cfd358240daf7e7d693dbeab8d22c7e5e5324bc5f9ef08df206a0b82acf378314b"' }>
                                        <li class="link">
                                            <a href="injectables/MailService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MailService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapsModule.html" data-type="entity-link" >MapsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' : 'data-bs-target="#xs-controllers-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' :
                                            'id="xs-controllers-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' }>
                                            <li class="link">
                                                <a href="controllers/MapController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' : 'data-bs-target="#xs-injectables-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' :
                                        'id="xs-injectables-links-module-MapsModule-b6a50c432df616df40b83ff645b95249d46a6219a64b44b937cfea8d48e2c65569303e9500f20e5d090ac5b1f0550ecc9e501b1785eaeae865a80e388af79e36"' }>
                                        <li class="link">
                                            <a href="injectables/AccountService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccountService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/EntryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FinancialCycleService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FinancialCycleService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JournalService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JournalService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MapService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersAdminModule.html" data-type="entity-link" >UsersAdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' : 'data-bs-target="#xs-controllers-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' :
                                            'id="xs-controllers-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' : 'data-bs-target="#xs-injectables-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' :
                                        'id="xs-injectables-links-module-UsersAdminModule-55597bdb49daf4bbe90c4e913c448e9c2ca60ec29da5ba6f99b8dcc795868644d29d8e561afddb9ad6f34e33eeeca28851492598f114f38ad8593d09d570d751"' }>
                                        <li class="link">
                                            <a href="injectables/UserAdminService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserAdminService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' : 'data-bs-target="#xs-controllers-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' :
                                            'id="xs-controllers-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' : 'data-bs-target="#xs-injectables-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' :
                                        'id="xs-injectables-links-module-UsersModule-9cde148735c67d19e92ba4725fad02545c2a5745ba5a9dd20bd9f3835d79e330f8a39ea622d6aeae3a5fc8a5602377661f15efddd0ae5c48840fd066a4b68ccb"' }>
                                        <li class="link">
                                            <a href="injectables/EntryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/FinancialCycleService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FinancialCycleService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JournalService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JournalService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MailService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MailService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MapService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserExistsRule.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserExistsRule</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#controllers-links"' :
                                'data-bs-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AccountController.html" data-type="entity-link" >AccountController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/FinancialCycleController.html" data-type="entity-link" >FinancialCycleController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/JournalController.html" data-type="entity-link" >JournalController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/MailController.html" data-type="entity-link" >MailController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/MapController.html" data-type="entity-link" >MapController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController-1.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#entities-links"' :
                                'data-bs-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Account.html" data-type="entity-link" >Account</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Entry.html" data-type="entity-link" >Entry</a>
                                </li>
                                <li class="link">
                                    <a href="entities/FinancialCycle.html" data-type="entity-link" >FinancialCycle</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Journal.html" data-type="entity-link" >Journal</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Map.html" data-type="entity-link" >Map</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/CraeteAccountDto.html" data-type="entity-link" >CraeteAccountDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CraeteEntryDto.html" data-type="entity-link" >CraeteEntryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CraeteJournalDto.html" data-type="entity-link" >CraeteJournalDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateFinancialCycleDto.html" data-type="entity-link" >CreateFinancialCycleDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateMapDto.html" data-type="entity-link" >CreateMapDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserDto.html" data-type="entity-link" >CreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/DatabaseConfig.html" data-type="entity-link" >DatabaseConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/EmailConfig.html" data-type="entity-link" >EmailConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/JournalDto.html" data-type="entity-link" >JournalDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateAccountDto.html" data-type="entity-link" >UpdateAccountDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateEntryDto.html" data-type="entity-link" >UpdateEntryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateFinancialCycleDto.html" data-type="entity-link" >UpdateFinancialCycleDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateJournalDto.html" data-type="entity-link" >UpdateJournalDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateMapDto.html" data-type="entity-link" >UpdateMapDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserDto.html" data-type="entity-link" >UpdateUserDto</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AccountService.html" data-type="entity-link" >AccountService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntryService.html" data-type="entity-link" >EntryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FinancialCycleService.html" data-type="entity-link" >FinancialCycleService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JournalService.html" data-type="entity-link" >JournalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MailService.html" data-type="entity-link" >MailService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MapService.html" data-type="entity-link" >MapService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserAdminService.html" data-type="entity-link" >UserAdminService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserExistsRule.html" data-type="entity-link" >UserExistsRule</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#guards-links"' :
                            'data-bs-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ExceptionType.html" data-type="entity-link" >ExceptionType</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ToApiType.html" data-type="entity-link" >ToApiType</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});