# boss-accounting

## 🛠️ Build Your Project In your Server

all this commands tested on ubuntu 22.04

So To make sure every thing will works fine makesure you are using same ubuntu

First install NodeJs

```bash
curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Then Install pnpm using npm

```bash
npm install -g pnpm
```

Then [install MariaDb ](https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-22-04)

connect to database using root user and create new database

```bash
sudo mariadb -u root -p
CREATE DATABASE boss;
```

Then install [pm2](https://pm2.keymetrics.io/)

```bash
npm install pm2 -g
```

go to your root project and give permesion to build.sh

```bash
cd /var/www/boss-accounting
chmod +x build.sh
./build.sh
```

Delete default website in nginx

Then Add your project to nginx file

```bash
cd /var/www/boss-accounting/artifacts
chmod +x populate.sh
./populate.sh
```

## Add Valid certificate using [Let's Encrypt ](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04)

```bash
sudo apt install certbot python3-certbot-nginx
sudo systemctl reload nginx

sudo ufw allow 'Nginx Full'
sudo ufw delete allow 'Nginx HTTP'

sudo certbot --nginx -d example.com -d www.example.com
```

### Now you can try to access to your project by your IP

# 🚀 run your Project in Development Enviroment

you should have

- nodejs
- mariadb
- pnpm

Connect to database using root user and create new database

```bash
sudo mariadb -u root -p
CREATE DATABASE boss;
```

Run frontend project and backend Individually

```bash
cd frontend
pnpm run dev

cd backend
pnpm run dev
```
