# before run this script you should give permition
# chmod +x build.sh

#################################
# BACKEND
#################################

pm2 stop boss

git reset --hard
git pull

cd ./backend


rm -rf node_modules
rm pnpm-lock.yaml

# remove package-lock.json
if [[ -f "package-lock.json" ]]; then
    rm package-lock.json
    echo "package-lock.json removed successfully."
else
    echo "package-lock.json does not exist."
fi

# remove  yarn.lock 
if [[ -f "yarn.lock" ]]; then
    rm  yarn.lock 
    echo "yarn.lock removed successfully."
else
    echo "yarn.lock does not exist."
fi


pnpm install 

pnpm run build 

cp -r env dist 

cd /var/www/boss-accounting/backend/dist
pm2 start main.js --name boss
# pnpm run start:production


cd ../../frontend-desktop

###############################
# FRONTEND
###############################

# cd ./frontend


rm -rf build
rm -rf node_modules
rm pnpm-lock.yaml

# remove package-lock.json
if [[ -f "package-lock.json" ]]; then
    rm package-lock.json
    echo "package-lock.json removed successfully."
else
    echo "package-lock.json does not exist."
fi

# ############################################
# # Increase The Ram
# ############################################
sudo fallocate -l 8G /swapfile
ls -lh /swapfile

sudo chmod 600 /swapfile
ls -lh /swapfile

sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
free -h


###########################################
# Build the project 
###########################################

npm cache clean --force
pnpm store prune

pnpm install

echo "export NODE_OPTIONS=--max-old-space-size=8192"
export NODE_OPTIONS=--max-old-space-size=8192

pnpm run build

